﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Mapiranja
{
    public class GeografskiObjekatMapiranje : ClassMap<GeografskiObjekat>
    {
        public GeografskiObjekatMapiranje()
        {
            UseUnionSubclassForInheritanceMapping();
            Id(x => x.Id).Column("ID").GeneratedBy.TriggerIdentity();
            Map(x => x.Naziv).Column("NAZIV");
        }
    }
}
