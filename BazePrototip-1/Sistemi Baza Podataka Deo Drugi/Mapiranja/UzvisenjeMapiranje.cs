﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Mapiranja
{
    public class UzvisenjeMapiranje : ClassMap<Uzvisenje>
    {
        UzvisenjeMapiranje()
        {
            Table("POVRSINSKI_OBJEKAT");
            Id(x => x.Id).Column("ID").GeneratedBy.TriggerIdentity();
            Map(x => x.NadmorskaVisina).Column("NADMORSKA_VISINA_UZVISENJA");
            Map(x => x.Naziv).Column("NAZIV");
            Map(x => x.TipPovrsinskogObjekta).Column("TIP_POVRSINSKOG_OBJEKTA");
            HasMany(x => x.Vrhovi).KeyColumn("ID_UZVISENJA").LazyLoad().Cascade.All().Inverse();
            HasManyToMany(x => x.Objekti).Table("EGZISTIRA_NA")
                .ParentKeyColumn("ID_UZVISENJA")
                .ChildKeyColumn("ID_GEOGRAFSKOG_OBJEKTA")
                .Cascade.All();

            HasManyToMany(x => x.Linijski).Table("OMEDJAVA")
                .ParentKeyColumn("ID_POVRSINSKOG_OBJEKTA")
                .ChildKeyColumn("ID_LINIJSKOG_OBJEKTA")
                .Cascade.All();
            Where("TIP_POVRSINSKOG_OBJEKTA = 'uzvisenje'");
        }
    }
}
