﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Mapiranja
{
    public class NalaziSeNaMapiranje : ClassMap<NalaziSeNa>
    {
        NalaziSeNaMapiranje()
        {
            Table("NALAZI_SE_NA");

            Id(x => x.Id).Column("ID").GeneratedBy.TriggerIdentity();
            References(x => x.Linijski).Column("ID_LINIJSKOG_OBJEKTA");
            References(x => x.Tackasti).Column("ID_TACKASTOG_OBJEKTA");
            Map(x => x.RedniBroj).Column("REDNI_BROJ");
            Map(x => x.RastojanjeNaredni).Column("RASTOJANJE_NAREDNI");
        }
    }
}
