﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Mapiranja
{
    public class PutMapiranje : ClassMap<Put>
    {
        PutMapiranje()
        {
            Table("LINIJSKI_OBJEKAT");
            Id(x => x.Id).Column("ID").GeneratedBy.TriggerIdentity();
            Map(x => x.Duzina).Column("DUZINA");
            Map(x => x.Naziv).Column("NAZIV");
            Map(x => x.KlasaPuta).Column("KLASA_PUTA");
            Map(x => x.TipLinijskogObjekta).Column("TIP_LINIJSKOG_OBJEKTA");

            HasMany(x => x.Koordinate).KeyColumn("ID_LINIJSKOG_OBJEKTA").LazyLoad().Cascade.All().Inverse();

            HasMany(x => x.NalaziSeNa).KeyColumn("ID_LINIJSKOG_OBJEKTA").LazyLoad();
            HasManyToMany(x => x.Povrsinski).Table("OMEDJAVA")
                .ParentKeyColumn("ID_LINIJSKOG_OBJEKTA")
                .ChildKeyColumn("ID_POVRSINSKOG_OBJEKTA")
                .Cascade.All();

            Where("TIP_LINIJSKOG_OBJEKTA = 'put'");
        }
    }
}
