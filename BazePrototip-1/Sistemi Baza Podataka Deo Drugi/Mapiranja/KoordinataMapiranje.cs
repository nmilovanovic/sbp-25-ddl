﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Mapiranja
{
    public class KoordinataMapiranje : ClassMap<Koordinata>
    {
        KoordinataMapiranje()
        {
            Table("KOORDINATA");
            Id(x => x.Id).Column("ID").GeneratedBy.TriggerIdentity();
            Map(x => x.GeografskaDuzina).Column("GEOGRAFSKA_DUZINA");
            Map(x => x.GeografskaSirina).Column("GEOGRAFSKA_SIRINA");
            Map(x => x.Indeks).Column("INDEKS");
            References(x => x.MyLinijskiObjekat).Column("ID_LINIJSKOG_OBJEKTA");
        }
    }
}
