﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Mapiranja
{
    public class VodenaPovrsinaMapiranje : ClassMap<VodenaPovrsina>
    {
        VodenaPovrsinaMapiranje()
        {
            Table("POVRSINSKI_OBJEKAT");
            Id(x => x.Id).Column("ID").GeneratedBy.TriggerIdentity();
            Map(x => x.TipPovrsinskogObjekta).Column("TIP_POVRSINSKOG_OBJEKTA");
            Map(x => x.Naziv).Column("NAZIV");
            Map(x => x.TipVodenePovrsine).Column("TIP_VODENE_POVRSINE");


            HasManyToMany(x => x.Linijski).Table("OMEDJAVA")
                .ParentKeyColumn("ID_POVRSINSKOG_OBJEKTA")
                .ChildKeyColumn("ID_LINIJSKOG_OBJEKTA")
                .Cascade.All();
            Where("TIP_POVRSINSKOG_OBJEKTA = 'vodena povrsina'");
        }
    }
}
