﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Mapiranja
{
    public class PovrsinskiObjekatMapiranje : SubclassMap<PovrsinskiObjekat>
    {
        public PovrsinskiObjekatMapiranje()
        {
            Table("POVRSINSKI_OBJEKAT");



            KeyColumn("ID");
            Abstract();
            Map(x => x.TipPovrsinskogObjekta).Column("TIP_POVRSINSKOG_OBJEKTA");
            HasManyToMany(x => x.Linijski).Table("OMEDJAVA")
                .ParentKeyColumn("ID_POVRSINSKOG_OBJEKTA")
                .ChildKeyColumn("ID_LINIJSKOG_OBJEKTA")
                .Cascade.All();
        }
    }
}
