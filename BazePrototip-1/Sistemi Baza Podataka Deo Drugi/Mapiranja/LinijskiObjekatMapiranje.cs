﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Mapiranja
{
    public class LinijskiObjekatMapiranje : SubclassMap<LinijskiObjekat>
    {
        public LinijskiObjekatMapiranje()
        {
            Table("LINIJSKI_OBJEKAT");
            KeyColumn("ID");
            Abstract();
            Map(x => x.Duzina).Column("DUZINA");
            Map(x => x.TipLinijskogObjekta).Column("TIP_LINIJSKOG_OBJEKTA");
            HasMany(x => x.Koordinate).KeyColumn("ID_LINIJSKOG_OBJEKTA").LazyLoad();
            HasMany(x => x.NalaziSeNa).KeyColumn("ID_LINIJSKOG_OBJEKTA").LazyLoad();
            HasManyToMany(x => x.Povrsinski).Table("OMEDJAVA")
                .ParentKeyColumn("ID_LINIJSKOG_OBJEKTA")
                .ChildKeyColumn("ID_POVRSINSKOG_OBJEKTA")
                .Cascade.All();
        }
    }
}
