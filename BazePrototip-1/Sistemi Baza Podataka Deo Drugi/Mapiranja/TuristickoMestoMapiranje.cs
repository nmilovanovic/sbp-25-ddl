﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Mapiranja
{
    public class TuristickoMestoMapiranje : ClassMap<TuristickoMesto> 
    {                                                                 
        TuristickoMestoMapiranje()                                    
        {
            Table("TACKASTI_OBJEKAT");

            Join("NASELJENO_MESTO", xx =>
            {
                xx.Fetch.Select();
                xx.KeyColumn("ID");
                xx.Map(x1 => x1.BrojStanovnika).Column("BROJ_STANOVNIKA");
                xx.Map(x1 => x1.Opstina).Column("OPSTINA");
                xx.Map(x1 => x1.DatumOsnivanja).Column("DATUM_OSNIVANJA");
                xx.Map(x1 => x1.TuristickoMestoFlag).Column("TURISTICKO_MESTO_FLAG");
                xx.HasMany(x1 => x1.Znamenitosti).KeyColumn("ID_NASELJENOG_MESTA").LazyLoad().Cascade.All().Inverse();
                xx.HasMany(x1 => x1.NalaziSeNa).KeyColumn("ID_TACKASTOG_OBJEKTA").LazyLoad().Cascade.All().Inverse();
                xx.Map(x1 => x1.DatumPocetkaTurizma).Column("DATUM_POCETKA_TURIZMA");
                xx.Map(x1 => x1.TipTurizma).Column("TIP_TURIZMA");
                
            });
            Id(x1 => x1.Id).Column("ID").GeneratedBy.TriggerIdentity();
            
            var x = this;
            x.Map(x1 => x1.NadmorskaVisina).Column("NADMORSKA_VISINA");
            x.Map(x1 => x1.Naziv).Column("NAZIV");
            x.Map(x1 => x1.GeografskaSirina).Column("GEOGRAFSKA_SIRINA");
            x.Map(x1 => x1.GeografskaDuzina).Column("GEOGRAFSKA_DUZINA");
            x.Map(x1 => x1.DatumPocetkaEvidencije).Column("DATUM_POCETKA_EVIDENCIJE");
            x.Map(x1 => x1.NaseljenoMestoFlag).Column("NASELJENO_MESTO_FLAG");
            Where("turisticko0_1_.TURISTICKO_MESTO_FLAG = 'T'");
        }
    }
}
