﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Mapiranja
{
    public class TackastiObjekatMapiranje : SubclassMap<TackastiObjekat>
    {
        TackastiObjekatMapiranje()
        {
            Table("TACKASTI_OBJEKAT");
            
            KeyColumn("ID");
            Abstract();
            Map(x => x.NadmorskaVisina).Column("NADMORSKA_VISINA");
            Map(x => x.GeografskaSirina).Column("GEOGRAFSKA_SIRINA");
            Map(x => x.GeografskaDuzina).Column("GEOGRAFSKA_DUZINA");
            Map(x => x.DatumPocetkaEvidencije).Column("DATUM_POCETKA_EVIDENCIJE");
            Map(x1 => x1.NaseljenoMestoFlag).Column("NASELJENO_MESTO_FLAG");

            HasMany(x => x.NalaziSeNa).KeyColumn("ID_TACKASTOG_OBJEKTA").LazyLoad();
        }
    }
}
