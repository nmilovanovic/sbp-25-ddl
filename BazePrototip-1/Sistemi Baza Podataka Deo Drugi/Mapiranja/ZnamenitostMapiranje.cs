﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Mapiranja
{
    public class ZnamenitostMapiranje : ClassMap<Znamenitost>
    {
        public ZnamenitostMapiranje()
        {
            Table("ZNAMENITOSTI");

            Id(x => x.Id).Column("ID").GeneratedBy.TriggerIdentity();
            Map(x => x.Ime).Column("IME");
            Map(x => x.Opis).Column("OPIS");
            References(x => x.MyNaseljenoMesto).Column("ID_NASELJENOG_MESTA");
        }
    }
}
