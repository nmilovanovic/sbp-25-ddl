﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Mapiranja
{
    public class VrhMapiranje : ClassMap<Vrh>
    {
        VrhMapiranje()
        {
            Table("VRH");

            Id(x => x.Id).GeneratedBy.TriggerIdentity();
            Map(x => x.NadmorskaVisina).Column("NADMORSKA_VISINA");
            Map(x => x.Naziv).Column("NAZIV");
            References(x => x.MyUzvisenje).Column("ID_UZVISENJA");
        }
    }
}
