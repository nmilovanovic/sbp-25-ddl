﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Sistemi_Baza_Podataka_Deo_Drugi.Mapiranja;

namespace Sistemi_Baza_Podataka_Deo_Drugi
{
    public class DataLayer
    {
        private static ISessionFactory _factory;
        private static object objLock = new object();

        public static ISession GetSession()
        {
            if(_factory==null)
            {
                lock(objLock)
                {
                    if (_factory == null)
                        _factory = CreateSessionFactory();
                }
            }
            
            return _factory.OpenSession();
        }

        private static ISessionFactory CreateSessionFactory()
        {
            try
            {
                var cfg = OracleManagedDataClientConfiguration.Oracle10
                    .ConnectionString(c => c.Is("Data Source=gislab-oracle.elfak.ni.ac.rs:1521/SBP_PDB;User Id=S15543;Password=lugalkien")).ShowSql();

                return Fluently.Configure().Database(cfg)
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<Mapiranja.TackastiObjekatMapiranje>()).
                    BuildSessionFactory();
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return null;
            }
        }

        public static ISessionFactory Factory { get => _factory; set => _factory = value; }
    }
}
