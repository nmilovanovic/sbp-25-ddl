﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NHibernate;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;

namespace Sistemi_Baza_Podataka_Deo_Drugi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                //Entiteti.Znamenitost t = s.Load<Znamenitost>(1);
                //string ss = t.Ime;

                Entiteti.Uzvisenje z = s.Load<Uzvisenje>(53);
                foreach(Vrh n in z.Vrhovi)
                {
                    MessageBox.Show(n.Naziv);
                }
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                

                Entiteti.Uzvisenje z = s.Load<Uzvisenje>(53);

                Vrh v = new Vrh();
                v.NadmorskaVisina = 10000;
                v.Naziv = "Jovanov test vrh";
                v.MyUzvisenje = z;
                s.Save(v);
                z.Vrhovi.Add(v);
                s.Save(z);
                


            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Entiteti.Uzvisenje z = s.Load<Uzvisenje>(53);

                Vrh v=null;

                for (int i = 0; i < z.Vrhovi.Count; i++)
                {
                    if (z.Vrhovi[i].Naziv.CompareTo("Jovanov test vrh") == 0)
                    {
                        v = z.Vrhovi[i];
                        z.Vrhovi.Remove(z.Vrhovi[i]);
                    }
                }
                s.Delete(v);
                s.SaveOrUpdate(z);
                s.Flush();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Entiteti.Uzvisenje z = s.Load<Uzvisenje>(53);

                foreach(GeografskiObjekat g in z.Objekti)
                {
                    MessageBox.Show(g.Naziv);
                }
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Entiteti.NaseljenoMesto z = s.Load<NaseljenoMesto>(69);

                z.Naziv = "Nesto novo3";

                s.SaveOrUpdate(z);
                s.Flush();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Entiteti.Uzvisenje z = s.Load<Uzvisenje>(54); //sa 54 sto je jezero nece raditi ni na nivou upita!!!
                                                              //pa je naredni if bespotreban!!!
                if (z.TipPovrsinskogObjekta.CompareTo("uzvisenje") == 0)
                {
                    MessageBox.Show(z.Naziv);
                    s.SaveOrUpdate(z);
                    s.Flush();
                }
                else
                    MessageBox.Show("Niste uzeli uzvisenje!");
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Entiteti.LinijskiObjekat z = s.Load<LinijskiObjekat>(1);

                MessageBox.Show(z.Naziv);
                s.Flush();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Entiteti.Put z = s.Load<Put>(24);

                MessageBox.Show(z.Naziv);
               
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Entiteti.NaseljenoMesto z = s.Load<NaseljenoMesto>(1);

                MessageBox.Show(z.Naziv);

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Entiteti.NaseljenoMesto z = s.Load<NaseljenoMesto>(3);
                Entiteti.LinijskiObjekat l = s.Load<LinijskiObjekat>(36);
                NalaziSeNa na = new NalaziSeNa();
                MessageBox.Show(l.Naziv);
                MessageBox.Show(z.Naziv);
                na.Linijski = l;
                na.RedniBroj = 0;
                na.Tackasti = z;
                na.RastojanjeNaredni = 15.123;
                s.Save(na);
                z.NalaziSeNa.Add(na);
                l.NalaziSeNa.Add(na);
                s.SaveOrUpdate(z);
                s.SaveOrUpdate(l);
                s.Flush();
                s.Close();

                MessageBox.Show(na.Id.ToString());

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Entiteti.NaseljenoMesto z = s.Load<NaseljenoMesto>(3);

                MessageBox.Show(z.Znamenitosti[0].Ime);
                z.Znamenitosti[0].Ime = "Evo jos jedan test da vidim dal radi";
                s.SaveOrUpdate(z.Znamenitosti[0]);
                s.SaveOrUpdate(z);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            

            try
            {
                ISession s = DataLayer.GetSession();


                Entiteti.NaseljenoMesto z = s.Load<NaseljenoMesto>(3);
                Entiteti.LinijskiObjekat l = s.Load<LinijskiObjekat>(36);

                NalaziSeNa na = z.NalaziSeNa[0];

                z.NalaziSeNa.RemoveAt(0);
                l.NalaziSeNa.RemoveAt(0);

                MessageBox.Show("Obrisana veza izmedju" + na.Linijski.Naziv +
                    " i " + na.Tackasti.Naziv);
                s.Delete(na);
                s.SaveOrUpdate(z);
                s.SaveOrUpdate(l);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Entiteti.Uzvisenje z = s.Load<Uzvisenje>(53);

                LinijskiObjekat l = new LinijskiObjekat();

                l.Naziv = "Jovanova reka";
                l.TipLinijskogObjekta = "reka";
                l.Duzina = 150000;

                s.SaveOrUpdate(l);
                z.Objekti.Add(l);
                s.SaveOrUpdate(z);
                s.Flush();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Entiteti.Uzvisenje z = s.Load<Uzvisenje>(53);

                for (int i = 0; i < z.Objekti.Count; i++)
                    if (z.Objekti[i].Naziv == "Jovanova reka")
                        z.Objekti.RemoveAt(i);
                s.SaveOrUpdate(z);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            try
            {
                /*ISession s = DataLayer.GetSession();
                TackastiObjekat t = new TackastiObjekat();
                t.DatumPocetkaEvidencije = DateTime.Now;
                t.GeografskaDuzina = 15;
                t.GeografskaSirina = 20;
                t.NadmorskaVisina = 1254;
                t.NaseljenoMestoFlag = "F";
                t.Naziv = "Jovanova tacka";
                s.SaveOrUpdate(t);
                s.Flush();
                s.Close();*/

                /*ISession s = DataLayer.GetSession();
                TackastiObjekat t = s.Load<TackastiObjekat>(126);
                s.Delete(t);
                s.Flush();
                s.Close();*/

                MySession.Open();
                TackastiObjekat t = MySession.GetTackastiObjekat(126);
                MySession.Delete(t);
                MySession.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                //NaseljenoMesto t = s.Load<NaseljenoMesto>(3);
                NaseljenoMesto t = new NaseljenoMesto();

                t.DatumPocetkaEvidencije = DateTime.Now;

                t.GeografskaDuzina = 15;
                t.GeografskaSirina = 20;
                t.NadmorskaVisina = 1254;
                //t.NaseljenoMestoFlag = true;
                t.Naziv = "Jovanov Grad";
                t.Opstina = "JOV";
                t.TuristickoMestoFlag = "F";
                t.NaseljenoMestoFlag = "T";
                t.BrojStanovnika = 150000;
                t.DatumOsnivanja = DateTime.Now;
                s.SaveOrUpdate(t);
                s.Flush();
                s.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                TuristickoMesto t = new TuristickoMesto();
                t.DatumPocetkaEvidencije = DateTime.Now;
                t.GeografskaDuzina = 15;
                t.GeografskaSirina = 20;
                t.NadmorskaVisina = 1254;
                t.NaseljenoMestoFlag = "T";
                t.Naziv = "Jovanova Banja";
                t.Opstina = "JOV";
                t.TuristickoMestoFlag = "T";
                t.TipTurizma = "banjski";
                t.DatumPocetkaTurizma = DateTime.Now;
                s.SaveOrUpdate(t);
                s.Flush();
                s.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button25_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Znamenitost z = new Znamenitost();
                NaseljenoMesto n = s.Load<NaseljenoMesto>(111);
                z.Ime = "Jovanova Crkva";
                z.MyNaseljenoMesto = n;
                z.Opis = "Divna crkva na kraju grada.";
                n.Znamenitosti.Add(z);
                s.SaveOrUpdate(z);
                s.SaveOrUpdate(n);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button21_Click(object sender, EventArgs e)
        {
            
        }

        private void button22_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Put l = new Put();
                l.Duzina = 10000;
                l.Naziv = "Jovanov put!";
                l.TipLinijskogObjekta = "put";
                l.KlasaPuta = "1.";
                s.SaveOrUpdate(l);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button23_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                LinijskiObjekat l = new LinijskiObjekat();
                l.Duzina = 10000;
                l.Naziv = "Jovanova granica!";
                l.TipLinijskogObjekta = "granicna linija";
                s.SaveOrUpdate(l);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button26_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Reka l = new Reka();
                l.Duzina = 10000;
                l.Naziv = "Jovanova reka!";
                l.TipLinijskogObjekta = "reka";
                s.SaveOrUpdate(l);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button27_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Reka l = s.Load<Reka>(120);
                TackastiObjekat t = s.Load<TackastiObjekat>(111);
                NalaziSeNa na = new NalaziSeNa();
                MessageBox.Show(l.Naziv);
                MessageBox.Show(t.Naziv);
                na.Linijski = l;
                na.RastojanjeNaredni = 15;
                na.RedniBroj = 1;
                na.Tackasti = t;
                
                s.SaveOrUpdate(na);
                t.NalaziSeNa.Add(na);
                l.NalaziSeNa.Add(na);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                VodenaPovrsina v = new VodenaPovrsina();
                v.Naziv = "Jovanovo jezero";
                v.TipPovrsinskogObjekta = "vodena povrsina";
                v.TipVodenePovrsine = "jezero";

                s.SaveOrUpdate(v);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Uzvisenje v = new Uzvisenje();
                v.Naziv = "Jovanova visoravan";
                v.TipPovrsinskogObjekta = "uzvisenje";
                v.NadmorskaVisina = 15000;

                s.SaveOrUpdate(v);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button24_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Uzvisenje u = s.Load<Uzvisenje>(123);
                Vrh v = new Vrh();
                v.MyUzvisenje = u;
                v.NadmorskaVisina = 15001;
                v.Naziv = "Jovanov vrh";
                u.Vrhovi.Add(v);

                s.SaveOrUpdate(v);
                s.SaveOrUpdate(u);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Reka l = s.Load<Reka>(120);
                Uzvisenje u = s.Load<Uzvisenje>(123);

                u.Objekti.Add(l);

                s.SaveOrUpdate(u);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }

        }

        private void button28_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Reka l = s.Load<Reka>(120);
                Uzvisenje u = s.Load<Uzvisenje>(123);

                u.Linijski.Add(l); 

                s.SaveOrUpdate(u);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button42_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                TackastiObjekat t = s.Load<TackastiObjekat>(110);

                s.Delete(t);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button39_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                NaseljenoMesto t = s.Load<NaseljenoMesto>(107);

                s.Delete(t);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button38_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                TuristickoMesto t = s.Load<TuristickoMesto>(109);

                s.Delete(t);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button30_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Reka r = s.Load<Reka>(120);
                TackastiObjekat t = s.Load<TackastiObjekat>(111);

                r.NalaziSeNa.RemoveAt(0);
                t.NalaziSeNa.RemoveAt(0);

                s.SaveOrUpdate(r);
                s.SaveOrUpdate(t);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Uzvisenje r = s.Load<Uzvisenje>(53);
                foreach (Vrh v in r.Vrhovi)
                    MessageBox.Show(v.Naziv);
                foreach (GeografskiObjekat go in r.Objekti)
                    MessageBox.Show(go.Naziv);
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                NaseljenoMesto r = s.Load<NaseljenoMesto>(4);
                if (r.NaseljenoMestoFlag.CompareTo("T") == 0)
                    r = s.Load<TuristickoMesto>(4);
                foreach (Znamenitost z in r.Znamenitosti)
                    MessageBox.Show(z.Ime);
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                NaseljenoMesto r = s.Load<NaseljenoMesto>(4);
                foreach (Znamenitost z in r.Znamenitosti)
                    MessageBox.Show(z.Ime);
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                NaseljenoMesto r = s.Load<NaseljenoMesto>(4);
                MessageBox.Show(r.BrojStanovnika.ToString());
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                TackastiObjekat r = s.Load<TackastiObjekat>(4);
                MessageBox.Show(r.Naziv);
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button7_Click_1(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                TuristickoMesto r = s.Load<TuristickoMesto>(4);
                MessageBox.Show(r.TipTurizma);
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button8_Click_1(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Reka r = s.Load<Reka>(22);
                MessageBox.Show(r.Duzina.ToString());
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button9_Click_1(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Put r = s.Load<Put>(33);
                MessageBox.Show(r.KlasaPuta.ToString());
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button10_Click_1(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                GranicnaLinija r = s.Load<GranicnaLinija>(25);
                MessageBox.Show(r.ImeDrzave);
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button11_Click_1(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                VodenaPovrsina r = s.Load<VodenaPovrsina>(50);
                MessageBox.Show(r.TipVodenePovrsine);
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button12_Click_1(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Uzvisenje r = s.Load<Uzvisenje>(53);
                MessageBox.Show(r.Vrhovi[0].Naziv);
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button13_Click_1(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                LinijskiObjekat l = s.Load<LinijskiObjekat>(33);
                foreach (NalaziSeNa na in l.NalaziSeNa)
                    MessageBox.Show(na.Tackasti.Naziv);
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button14_Click_1(object sender, EventArgs e)
        {
            try
            {
                MySession.Open();
                PovrsinskiObjekat l = MySession.GetPovrsinskiObjekat(53);
                foreach (LinijskiObjekat na in l.Linijski)
                    MessageBox.Show(na.Naziv);
                MySession.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            try
            {
                MySession.Open();
                TackastiObjekat t = MySession.GetTackastiObjekat(4);
                MessageBox.Show(t.Naziv);
                Uzvisenje u = MySession.GetUzvisenje(53);
                MessageBox.Show(u.Naziv);
                /*LinijskiObjekat l = MySession.GetLinijskiObjekat(49);
                MessageBox.Show(l.Naziv);
                MySession.Delete(l);*/
                TuristickoMesto t2 = MySession.GetTuristickoMesto(125);
                MySession.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button21_Click_1(object sender, EventArgs e)
        {
            MySession.Open();
            TackastiObjekat na = MySession.GetTackastiObjekat(111);
            MySession.Delete(na.NalaziSeNa[0]);
            na.NalaziSeNa.RemoveAt(0);
            MySession.Close();
        }
    }
}
