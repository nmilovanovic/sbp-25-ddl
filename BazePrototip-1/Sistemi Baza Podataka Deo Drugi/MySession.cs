﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;

using System.Windows.Forms;

namespace Sistemi_Baza_Podataka_Deo_Drugi
{
    public class MySession
    {
        public static ISession mySession;
        public static void Open()
        {
            mySession = DataLayer.GetSession();
        }
        public static void Close()
        {
            mySession.Flush();
            mySession.Close();
        }

        public static void Update(object obj)
        {
            mySession.Update(obj);
        }

        public static void SaveOrUpdate(object obj)
        {
            mySession.SaveOrUpdate(obj);
        }

        public static object Merge(object obj)
        {
            return mySession.Merge(obj);
        }

        public static void Flush()
        {
            mySession.Flush();
        }

        public static TuristickoMesto GetTuristickoMesto(int id)
        {
            TuristickoMesto t =  mySession.Load<TuristickoMesto>(id);
            if (t.TuristickoMestoFlag.CompareTo("T") != 0)
                throw new Exception("Turisticko mesto sa tim id-jem ne postoji!");
            return t;
        }

        public static Uzvisenje GetUzvisenje(int id)
        {
            Uzvisenje u = mySession.Load<Uzvisenje>(id);
            for (int i = 0; i < u.Objekti.Count; i++)
                u.Objekti[i] = MySession.GetGeografskiObjekat(u.Objekti[i].Id);
            for (int i = 0; i < u.Linijski.Count; i++)
                u.Linijski[i] = MySession.GetLinijskiObjekat(u.Linijski[i].Id);
            return u;
        }

        public static VodenaPovrsina GetVodenaPovrsina(int id)
        {
            return mySession.Load<VodenaPovrsina>(id);
        }

        public static Reka GetReka(int id)
        {
            return mySession.Load<Reka>(id);
        }

        public static GranicnaLinija GetGranicnaLinija(int id)
        {
            return mySession.Load<GranicnaLinija>(id);
        }

        public static Put GetPut(int id)
        {
            return mySession.Load<Put>(id);
        }

        public static NaseljenoMesto GetNaseljenoMesto(int id)
        {
            NaseljenoMesto m = mySession.Load<NaseljenoMesto>(id);
            if (m.TuristickoMestoFlag.CompareTo("T") == 0)
                m = mySession.Load<TuristickoMesto>(id);
            return m;
        }

        public static TackastiObjekat GetTackastiObjekat(int id)
        {
            TackastiObjekat t = mySession.Load<TackastiObjekat>(id);
            if (t.NaseljenoMestoFlag.CompareTo("T") == 0)
                t = MySession.GetNaseljenoMesto(id);
            return t;
        }

        public static LinijskiObjekat GetLinijskiObjekat(int id)
        {
            IList<LinijskiObjekat> lista = (from p in mySession.Query<LinijskiObjekat>()
                                            where p.Id == id
                                            select p).ToList();
            if (lista.Count == 1)
            {
                return lista[0];
            }
            else
            {
                /*LinijskiObjekat loTatko = lista.First(x => x.GetType() == typeof(LinijskiObjekat));
                LinijskiObjekat loDete = lista.First(x => x.GetType() != typeof(LinijskiObjekat));
                loDete.NalaziSeNa = loTatko.NalaziSeNa;
                loDete.Koordinate = loTatko.Koordinate;
                loDete.Povrsinski = loTatko.Povrsinski;
                return loDete;*/
                return lista.First(x => x.GetType() != typeof(LinijskiObjekat));
            }
        }

        public static PovrsinskiObjekat GetPovrsinskiObjekat(int id)
        {
            /*PovrsinskiObjekat p = mySession.Load<PovrsinskiObjekat>(id);
            if (p.TipPovrsinskogObjekta.CompareTo("vodena povrsina") == 0)
                p = MySession.GetVodenaPovrsina(id);
            else if (p.TipPovrsinskogObjekta.CompareTo("uzvisenje") == 0)
                p = MySession.GetUzvisenje(id);
            return p;*/
            IList<PovrsinskiObjekat> lista = (from p in mySession.Query<PovrsinskiObjekat>()
                                              where p.Id == id
                                              select p).ToList();
            if (lista.Count == 1)
            {
                return lista[0];
            }
            else
            {
                return lista.First(x => x.GetType() != typeof(PovrsinskiObjekat));
            }
        }

        public static GeografskiObjekat GetGeografskiObjekat(int id)
        {

            GeografskiObjekat g = null;
            try
            {
                g = GetTackastiObjekat(id);
                return g;
            }catch(Exception e)
            {

            }

            try
            {
                g = GetLinijskiObjekat(id);
                return g;
            }
            catch (Exception e)
            {

            }

            try
            {
                g = GetPovrsinskiObjekat(id);
                return g;
            }
            catch (Exception e)
            {

            }
            return g;
        }

        public static Vrh GetVrh(int id)
        {
            return mySession.Load<Vrh>(id);
        }

        public static Znamenitost GetZnamenitost(int id)
        {
            return mySession.Load<Znamenitost>(id);
        }

        public static Koordinata GetKoordinata(int id)
        {
            return mySession.Load<Koordinata>(id);
        }

        public static void Save(object o)
        {
            mySession.SaveOrUpdate(o);
        }

        public static void Delete(object o)
        {
            mySession.Delete(o);
        }

        public static IList<Uzvisenje> GetAllUzvisenje()
        {
            return (from p in mySession.Query<Uzvisenje>()
                    select p).ToList();
        }

        public static IList<VodenaPovrsina> GetAllVodenaPovrsina()
        {
            return (from p in mySession.Query<VodenaPovrsina>()
                    select p).ToList();
        }

        public static IList<PovrsinskiObjekat> GetAllPovrsinskiObjekat()
        {
            IList<PovrsinskiObjekat> lista = (from p in mySession.Query<PovrsinskiObjekat>()
                                              select p).ToList();

            return lista.Where(x => x.GetType() == typeof(PovrsinskiObjekat)).ToList();
        }

        public static IList<Reka> GetAllReka()
        {
            return (from p in mySession.Query<Reka>()
                    select p).ToList();
        }

        public static IList<Put> GetAllPut()
        {
            return (from p in mySession.Query<Put>()
                    select p).ToList();
        }

        public static IList<GranicnaLinija> GetAllGranicnaLinija()
        {
            return (from p in mySession.Query<GranicnaLinija>()
                    select p).ToList();
        }

        public static IList<LinijskiObjekat> GetAllLinijskiObjekat()
        {
            IList<LinijskiObjekat> lista = (from p in mySession.Query<LinijskiObjekat>()
                                            select p).ToList();

            return lista.Where(x => x.GetType() == typeof(LinijskiObjekat)).ToList();
        }

        public static IList<TuristickoMesto> GetAllTuristickoMesto()
        {
            return (from p in mySession.Query<TuristickoMesto>()
                    select p).ToList();
        }

        public static IList<NaseljenoMesto> GetAllNaseljenoMesto()
        {
            IList<NaseljenoMesto> lista = (from p in mySession.Query<NaseljenoMesto>()
                                           select p).ToList();

            return lista.Where(x => x.GetType() == typeof(NaseljenoMesto)).ToList();
        }

        public static IList<TackastiObjekat> GetAllTackastiObjekat()
        {
            IList<TackastiObjekat> lista = (from p in mySession.Query<TackastiObjekat>()
                                            select p).ToList();

            return lista.Where(x => x.GetType() == typeof(TackastiObjekat)).ToList();
        }

        public static IList<GeografskiObjekat> GetAllGeografskiObjekat()
        {
            IList<GeografskiObjekat> lista = (from p in mySession.Query<GeografskiObjekat>()
                                              select p).ToList();

            return lista.Where(x => x.GetType() == typeof(TackastiObjekat) ||
                                    x.GetType() == typeof(LinijskiObjekat) ||
                                    x.GetType() == typeof(PovrsinskiObjekat)).ToList();
        }
    }
}
