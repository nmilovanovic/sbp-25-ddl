﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Entiteti
{
    public abstract class GeografskiObjekat
    {
        public virtual int Id { get;  set; }
        public virtual string Naziv { get;  set; }
    }
}