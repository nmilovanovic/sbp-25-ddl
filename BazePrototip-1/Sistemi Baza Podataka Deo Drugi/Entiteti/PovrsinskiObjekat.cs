﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Entiteti
{
    public class PovrsinskiObjekat : GeografskiObjekat
    {
        public virtual string TipPovrsinskogObjekta { get;  set; }
        public virtual IList<LinijskiObjekat> Linijski { get;  set; }

        public PovrsinskiObjekat() { Linijski = new List<LinijskiObjekat>(); }
    }
}
