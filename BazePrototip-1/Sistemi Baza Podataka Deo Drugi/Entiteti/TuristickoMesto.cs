﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Entiteti
{
    public class TuristickoMesto : NaseljenoMesto
    {
        public virtual string TipTurizma { get;  set; }
        public virtual DateTime DatumPocetkaTurizma { get;  set; }
    }
}
