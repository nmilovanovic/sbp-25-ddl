﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Entiteti
{
    public class Koordinata
    {
        public virtual int Id { get;  set; }
        public virtual int Indeks { get;  set; }
        public virtual double GeografskaSirina { get;  set; }
        public virtual double GeografskaDuzina { get;  set; }
        public virtual LinijskiObjekat MyLinijskiObjekat { get; set; }
    }
}