﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Entiteti
{
    public class NaseljenoMesto : TackastiObjekat
    {
        public virtual int BrojStanovnika { get;  set; }
        public virtual string Opstina { get;  set; }
        public virtual DateTime DatumOsnivanja { get;  set; }
        public virtual string TuristickoMestoFlag { get;  set; }
        public virtual IList<Znamenitost> Znamenitosti { get;  set; }

        public NaseljenoMesto()
        {
            Znamenitosti = new List<Znamenitost>();
        }
    }
}
