﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Entiteti
{
    public class Znamenitost
    {
        public virtual int Id { get;  set; }
        public virtual string Ime { get; set; }
        public virtual string Opis { get; set; }
        public virtual NaseljenoMesto MyNaseljenoMesto { get; set; }
    }
}
