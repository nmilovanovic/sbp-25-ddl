﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Entiteti
{
    public class Vrh
    {
        public virtual int Id { get; protected set; }
        public virtual string Naziv { get;  set; }
        public virtual double NadmorskaVisina { get;  set; }
        public virtual Uzvisenje MyUzvisenje { get; set; }
    }
}
