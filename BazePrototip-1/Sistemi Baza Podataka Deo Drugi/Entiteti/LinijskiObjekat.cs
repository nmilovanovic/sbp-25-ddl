﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Entiteti
{
    public class LinijskiObjekat : GeografskiObjekat
    {
        public virtual string TipLinijskogObjekta { get;  set; }
        public virtual double Duzina { get;  set; }
        public virtual IList<Koordinata> Koordinate { get;  set; }
        public virtual IList<NalaziSeNa> NalaziSeNa { get;  set; }
        public virtual IList<PovrsinskiObjekat> Povrsinski { get;  set; }

        public LinijskiObjekat()
        {
            Koordinate = new List<Koordinata>();
            NalaziSeNa = new List<NalaziSeNa>();
        }
    }
}
