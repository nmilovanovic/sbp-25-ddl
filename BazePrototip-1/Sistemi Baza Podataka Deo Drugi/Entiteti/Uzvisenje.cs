﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Entiteti
{
    public class Uzvisenje : PovrsinskiObjekat
    {
        public virtual int NadmorskaVisina { get;  set; }
        public virtual IList<Vrh> Vrhovi { get;  set; }
        public virtual IList<GeografskiObjekat> Objekti { get;  set; }
        
        public Uzvisenje() {}
    }
}
