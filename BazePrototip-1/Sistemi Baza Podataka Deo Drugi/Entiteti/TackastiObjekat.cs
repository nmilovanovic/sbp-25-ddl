﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Entiteti
{
    public class TackastiObjekat : GeografskiObjekat
    {
        public virtual double NadmorskaVisina { get;  set; }
        public virtual double GeografskaSirina { get;  set; }
        public virtual double GeografskaDuzina { get;  set; }
        public virtual DateTime DatumPocetkaEvidencije { get;  set; }
        public virtual string NaseljenoMestoFlag { get;  set; }
        public virtual IList<NalaziSeNa> NalaziSeNa { get;  set; }

        public TackastiObjekat() { NalaziSeNa = new List<NalaziSeNa>(); }
    }
}
