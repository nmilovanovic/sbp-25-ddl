﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistemi_Baza_Podataka_Deo_Drugi.Entiteti
{
    public class NalaziSeNa
    {
        public virtual int RedniBroj { get;  set; }
        public virtual int Id { get;  set; }
        public virtual TackastiObjekat Tackasti { get;  set; }
        public virtual LinijskiObjekat Linijski { get;  set; }
        public virtual double RastojanjeNaredni { get;  set; }
    }
}
