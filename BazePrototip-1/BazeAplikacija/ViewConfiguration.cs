﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BazeAplikacija
{
    public delegate void ViewConfigurationSelect(MetroFramework.Controls.MetroGrid dgv);

    public static class ViewConfiguration
    {
        public static void GeografskiObjekat(MetroFramework.Controls.MetroGrid dgv)
        {
            dgv.Columns["Id"].Visible = false;

            dgv.Columns["Naziv"].Visible = true;
            dgv.Columns["Naziv"].DisplayIndex = 0;
        }

        public static void TackastiObjekat(MetroFramework.Controls.MetroGrid dgv)
        {
            dgv.Columns["Id"].Visible = false;
            /*dgv.Columns["NalaziSeNa"].Visible = false;
            dgv.Columns["NaseljenoMestoFlag"].Visible = false;*/

            dgv.Columns["Naziv"].Visible = true;
            dgv.Columns["Naziv"].DisplayIndex = 0;
            dgv.Columns["NadmorskaVisina"].Visible = true;
            dgv.Columns["NadmorskaVisina"].DisplayIndex = 1;
            dgv.Columns["NadmorskaVisina"].HeaderText = "Nadmorska visina";
            dgv.Columns["GeografskaSirina"].Visible = true;
            dgv.Columns["GeografskaSirina"].DisplayIndex = 2;
            dgv.Columns["GeografskaSirina"].HeaderText = "Geografska širina";
            dgv.Columns["GeografskaDuzina"].Visible = true;
            dgv.Columns["GeografskaDuzina"].DisplayIndex = 3;
            dgv.Columns["GeografskaDuzina"].HeaderText = "Geografska dužina";
            dgv.Columns["DatumPocetkaEvidencije"].Visible = true;
            dgv.Columns["DatumPocetkaEvidencije"].DisplayIndex = 4;
            dgv.Columns["DatumPocetkaEvidencije"].HeaderText = "Datum početka evidencije";
        }

        public static void NaseljenoMesto(MetroFramework.Controls.MetroGrid dgv)
        {
            dgv.Columns["Id"].Visible = false;
            /*dgv.Columns["NalaziSeNa"].Visible = false;
            dgv.Columns["NaseljenoMestoFlag"].Visible = false;
            dgv.Columns["TuristickoMestoFlag"].Visible = false;
            dgv.Columns["Znamenitosti"].Visible = false;*/

            dgv.Columns["Naziv"].Visible = true;
            dgv.Columns["Naziv"].DisplayIndex = 0;
            dgv.Columns["NadmorskaVisina"].Visible = true;
            dgv.Columns["NadmorskaVisina"].DisplayIndex = 1;
            dgv.Columns["NadmorskaVisina"].HeaderText = "Nadmorska visina";
            dgv.Columns["GeografskaSirina"].Visible = true;
            dgv.Columns["GeografskaSirina"].DisplayIndex = 2;
            dgv.Columns["GeografskaSirina"].HeaderText = "Geografska širina";
            dgv.Columns["GeografskaDuzina"].Visible = true;
            dgv.Columns["GeografskaDuzina"].DisplayIndex = 3;
            dgv.Columns["GeografskaDuzina"].HeaderText = "Geografska dužina";
            dgv.Columns["DatumPocetkaEvidencije"].Visible = true;
            dgv.Columns["DatumPocetkaEvidencije"].DisplayIndex = 4;
            dgv.Columns["DatumPocetkaEvidencije"].HeaderText = "Datum početka evidencije";
            dgv.Columns["BrojStanovnika"].Visible = true;
            dgv.Columns["BrojStanovnika"].DisplayIndex = 5;
            dgv.Columns["BrojStanovnika"].HeaderText = "Broj stanovnika";
            dgv.Columns["Opstina"].Visible = true;
            dgv.Columns["Opstina"].DisplayIndex = 6;
            dgv.Columns["Opstina"].HeaderText = "Opština";
            dgv.Columns["DatumOsnivanja"].Visible = true;
            dgv.Columns["DatumOsnivanja"].DisplayIndex = 7;
            dgv.Columns["DatumOsnivanja"].HeaderText = "Datum osnivanja";
        }

        public static void TuristickoMesto(MetroFramework.Controls.MetroGrid dgv)
        {
            dgv.Columns["Id"].Visible = false;
            /*dgv.Columns["NalaziSeNa"].Visible = false;
            dgv.Columns["NaseljenoMestoFlag"].Visible = false;
            dgv.Columns["TuristickoMestoFlag"].Visible = false;
            dgv.Columns["Znamenitosti"].Visible = false;*/

            dgv.Columns["Naziv"].Visible = true;
            dgv.Columns["Naziv"].DisplayIndex = 0;
            dgv.Columns["NadmorskaVisina"].Visible = true;
            dgv.Columns["NadmorskaVisina"].DisplayIndex = 1;
            dgv.Columns["NadmorskaVisina"].HeaderText = "Nadmorska visina";
            dgv.Columns["GeografskaSirina"].Visible = true;
            dgv.Columns["GeografskaSirina"].DisplayIndex = 2;
            dgv.Columns["GeografskaSirina"].HeaderText = "Geografska širina";
            dgv.Columns["GeografskaDuzina"].Visible = true;
            dgv.Columns["GeografskaDuzina"].DisplayIndex = 3;
            dgv.Columns["GeografskaDuzina"].HeaderText = "Geografska dužina";
            dgv.Columns["DatumPocetkaEvidencije"].Visible = true;
            dgv.Columns["DatumPocetkaEvidencije"].DisplayIndex = 4;
            dgv.Columns["DatumPocetkaEvidencije"].HeaderText = "Datum početka evidencije";
            dgv.Columns["BrojStanovnika"].Visible = true;
            dgv.Columns["BrojStanovnika"].DisplayIndex = 5;
            dgv.Columns["BrojStanovnika"].HeaderText = "Broj stanovnika";
            dgv.Columns["Opstina"].Visible = true;
            dgv.Columns["Opstina"].DisplayIndex = 6;
            dgv.Columns["Opstina"].HeaderText = "Opština";
            dgv.Columns["DatumOsnivanja"].Visible = true;
            dgv.Columns["DatumOsnivanja"].DisplayIndex = 7;
            dgv.Columns["DatumOsnivanja"].HeaderText = "Datum osnivanja";
            dgv.Columns["TipTurizma"].Visible = true;
            dgv.Columns["TipTurizma"].DisplayIndex = 8;
            dgv.Columns["TipTurizma"].HeaderText = "Tip turizma";
            dgv.Columns["DatumPocetkaTurizma"].Visible = true;
            dgv.Columns["DatumPocetkaTurizma"].DisplayIndex = 9;
            dgv.Columns["DatumPocetkaTurizma"].HeaderText = "Datum početka turizma";
        }

        public static void LinijskiObjekat(MetroFramework.Controls.MetroGrid dgv)
        {
            dgv.Columns["Id"].Visible = false;
            /*dgv.Columns["Koordinate"].Visible = false;
            dgv.Columns["NalaziSeNa"].Visible = false;
            dgv.Columns["Povrsinski"].Visible = false;*/

            dgv.Columns["Naziv"].Visible = true;
            dgv.Columns["Naziv"].DisplayIndex = 0;
            dgv.Columns["Duzina"].Visible = true;
            dgv.Columns["Duzina"].DisplayIndex = 1;
            dgv.Columns["Duzina"].HeaderText = "Dužina";
            dgv.Columns["TipLinijskogObjekta"].Visible = true;
            dgv.Columns["TipLinijskogObjekta"].DisplayIndex = 2;
            dgv.Columns["TipLinijskogObjekta"].HeaderText = "Tip linijskog objekta";
        }

        public static void Reka(MetroFramework.Controls.MetroGrid dgv)
        {
            dgv.Columns["Id"].Visible = false;
            /*dgv.Columns["TipLinijskogObjekta"].Visible = false;
            dgv.Columns["Koordinate"].Visible = false;
            dgv.Columns["NalaziSeNa"].Visible = false;
            dgv.Columns["Povrsinski"].Visible = false;*/

            dgv.Columns["Naziv"].Visible = true;
            dgv.Columns["Naziv"].DisplayIndex = 0;
            dgv.Columns["Duzina"].Visible = true;
            dgv.Columns["Duzina"].DisplayIndex = 1;
            dgv.Columns["Duzina"].HeaderText = "Dužina";
        }

        public static void Put(MetroFramework.Controls.MetroGrid dgv)
        {
            dgv.Columns["Id"].Visible = false;
            /*dgv.Columns["TipLinijskogObjekta"].Visible = false;
            dgv.Columns["Koordinate"].Visible = false;
            dgv.Columns["NalaziSeNa"].Visible = false;
            dgv.Columns["Povrsinski"].Visible = false;*/

            dgv.Columns["Naziv"].Visible = true;
            dgv.Columns["Naziv"].DisplayIndex = 0;
            dgv.Columns["Duzina"].Visible = true;
            dgv.Columns["Duzina"].DisplayIndex = 1;
            dgv.Columns["Duzina"].HeaderText = "Dužina";
            dgv.Columns["KlasaPuta"].Visible = true;
            dgv.Columns["KlasaPuta"].DisplayIndex = 2;
            dgv.Columns["KlasaPuta"].HeaderText = "Klasa puta";
        }

        public static void GranicnaLinija(MetroFramework.Controls.MetroGrid dgv)
        {
            dgv.Columns["Id"].Visible = false;
            /*dgv.Columns["TipLinijskogObjekta"].Visible = false;
            dgv.Columns["Koordinate"].Visible = false;
            dgv.Columns["NalaziSeNa"].Visible = false;
            dgv.Columns["Povrsinski"].Visible = false;*/

            dgv.Columns["Naziv"].Visible = true;
            dgv.Columns["Naziv"].DisplayIndex = 0;
            dgv.Columns["Duzina"].Visible = true;
            dgv.Columns["Duzina"].DisplayIndex = 1;
            dgv.Columns["Duzina"].HeaderText = "Dužina";
            dgv.Columns["ImeDrzave"].Visible = true;
            dgv.Columns["ImeDrzave"].DisplayIndex = 2;
            dgv.Columns["ImeDrzave"].HeaderText = "Ime države";
        }

        public static void PovrsinskiObjekat(MetroFramework.Controls.MetroGrid dgv)
        {
            dgv.Columns["Id"].Visible = false;
            //dgv.Columns["Linijski"].Visible = false;

            dgv.Columns["Naziv"].Visible = true;
            dgv.Columns["Naziv"].DisplayIndex = 0;
            dgv.Columns["TipPovrsinskogObjekta"].Visible = true;
            dgv.Columns["TipPovrsinskogObjekta"].DisplayIndex = 1;
            dgv.Columns["TipPovrsinskogObjekta"].HeaderText = "Tip površinskog objekta";
        }

        public static void VodenaPovrsina(MetroFramework.Controls.MetroGrid dgv)
        {
            dgv.Columns["Id"].Visible = false;
            /*dgv.Columns["TipPovrsinskogObjekta"].Visible = false;
            dgv.Columns["Linijski"].Visible = false;*/

            dgv.Columns["Naziv"].Visible = true;
            dgv.Columns["Naziv"].DisplayIndex = 0;
            dgv.Columns["TipVodenePovrsine"].Visible = true;
            dgv.Columns["TipVodenePovrsine"].DisplayIndex = 1;
            dgv.Columns["TipVodenePovrsine"].HeaderText = "Tip vodene površine";
        }

        public static void Uzvisenje(MetroFramework.Controls.MetroGrid dgv)
        {
            dgv.Columns["Id"].Visible = false;
            /*dgv.Columns["TipPovrsinskogObjekta"].Visible = false;
            dgv.Columns["Vrhovi"].Visible = false;
            dgv.Columns["Objekti"].Visible = false;
            dgv.Columns["Linijski"].Visible = false;*/

            dgv.Columns["Naziv"].Visible = true;
            dgv.Columns["Naziv"].DisplayIndex = 0;
            dgv.Columns["NadmorskaVisina"].Visible = true;
            dgv.Columns["NadmorskaVisina"].DisplayIndex = 1;
            dgv.Columns["NadmorskaVisina"].HeaderText = "Nadmorska visina";
        }

        public static void NalaziSeNaTackasti(MetroFramework.Controls.MetroGrid dgv)
        {
            dgv.Columns["RedniBroj"].HeaderText = "Redni broj";
            dgv.Columns["NadmorskaVisina"].HeaderText = "Nadmorska visina";
            dgv.Columns["GeografskaSirina"].HeaderText = "Geografska širina";
            dgv.Columns["GeografskaDuzina"].HeaderText = "Geografska dužina";
            dgv.Columns["DatumPocetkaEvidencije"].HeaderText = "Datum početka evidencije";
            dgv.Columns["RastojanjeNaredni"].HeaderText = "Rastojanje do narednog";
        }
        public static void NalaziSeNaLinijski(MetroFramework.Controls.MetroGrid dgv)
        {
            dgv.Columns["RedniBroj"].HeaderText = "Redni broj";
            dgv.Columns["Duzina"].HeaderText = "Dužina";
            dgv.Columns["TipLinijskogObjekta"].HeaderText = "Tip linijskog objekta";
            dgv.Columns["RastojanjeNaredni"].HeaderText = "Rastojanje do narednog";
        }

        public static void Znamenitost(MetroFramework.Controls.MetroGrid dgv)
        {

        }

        public static void Vrh(MetroFramework.Controls.MetroGrid dgv)
        {
            dgv.Columns["NadmorskaVisina"].HeaderText = "Nadmorska visina";
        }

        public static void Koordinata(MetroFramework.Controls.MetroGrid dgv)
        {
            //dgv.Columns["Id"].Visible = false;
            //dgv.Columns["MyLinijskiObjekat"].Visible = false;

            dgv.Columns["GeografskaSirina"].HeaderText = "Geografska širina";
            dgv.Columns["GeografskaDuzina"].HeaderText = "Geografska dužina";
        }
    }
}
