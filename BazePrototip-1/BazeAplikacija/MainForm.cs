﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using System.IO;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;
using Sistemi_Baza_Podataka_Deo_Drugi;
using NHibernate;
using BazeAplikacija.Karta;
using MetroFramework;

namespace BazeAplikacija
{
    public partial class MainForm : MetroForm
    {
        #region CRTANJE_po_KARTI
        public ChromiumWebBrowser browser;
        private static readonly string myPath = Application.StartupPath;
        private static readonly string pagesPath = Path.Combine(myPath, "pages");
        private Inject inj = new Inject();

        //objekat ove klase se koristi da JS otkrije tio objekta, a kad je u pitanju linijski objekat
        //sluzi i da se JS-u dostave njegove koordinate, kada su u pitanju linijski i povrsinski objekat,
        //njihove koordinate se dostavljaju preko fajla jer je bind-ovanje slozeno kada su u pitanju 
        //nizovi, klasicno bind-ovanje ne dozvoljava nizove.
        public class Inject
        {
            private string tipObjekta = "";
            private double geografskaSirina;
            private double geografskaDuzina;

            public string TipObjekta { get => tipObjekta; set => tipObjekta = value; }
            public double GeografskaSirina { get => geografskaSirina; set => geografskaSirina = value; }
            public double GeografskaDuzina { get => geografskaDuzina; set => geografskaDuzina = value; }

            public string GetTipObjekta()
            {
                return TipObjekta;
            }

            public double GetGeografskaSirina()
            {
                return GeografskaSirina;
            }

            public double GetGeografskaDuzina()
            {
                return GeografskaDuzina;
            }
        }

        private void NacrtajTackasti(TackastiObjekat to)
        {
            inj.GeografskaSirina = to.GeografskaSirina;
            inj.GeografskaDuzina = to.GeografskaDuzina;
            inj.TipObjekta = "tackasti";
        }

        private bool NacrtajLinijski(LinijskiObjekat lo)
        {
            if (lo.Koordinate.Count < 2) return false;
            IzveziLinijskeKoordinate(lo);
            inj.TipObjekta = "linijski";
            return true;
        }

        private void IzveziLinijskeKoordinate(LinijskiObjekat lo)
        {
            /*
           var flightPlanCoordinates = [
          { lat: 37.772, lng: -122.214 },
          { lat: 21.291, lng: -157.821 },
          { lat: -18.142, lng: 178.431 },
          { lat: -27.467, lng: 153.027 }
        ];*/

            StringBuilder sb = new StringBuilder();
            sb.Append("var flightPlanCoordinates = [");
            var koord = (from k in lo.Koordinate orderby k.Indeks ascending select k).ToList();
            foreach (Koordinata k in koord)
            {
                sb.Append("{lat: " + k.GeografskaSirina.ToString() + "," + " lng: " + k.GeografskaDuzina.ToString() + " },");
            }
            String s = sb.ToString();
            char[] charsToTrim = { ',' };
            s = s.TrimEnd(charsToTrim);
            s += "];";
            File.WriteAllText(GetPagePath("linijskikoordinate.js"), s);

        }

        private bool NacrtajPovrsinski(PovrsinskiObjekat po)
        {
            try
            {
                LinijskiObjekat medja = (from p in po.Linijski where p.Naziv.Contains(" međa") select p).First();
                if (medja.Koordinate.Count < 3) return false;
            }
            catch (Exception)
            {
                return false;
            }
            IzveziPovrsinskeKoordinate(po);
            inj.TipObjekta = "povrsinski";
            return true;
        }

        public void IzveziPovrsinskeKoordinate(PovrsinskiObjekat po)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("var flightPlanCoordinates = [");
            List<LinijskiObjekat> linije = (from lin in po.Linijski select lin).ToList();
            LinijskiObjekat medja = (from p in po.Linijski where p.Naziv.Contains(" međa") select p).First();

            var koord = (from k in medja.Koordinate orderby k.Indeks ascending select k).ToList();
            foreach (Koordinata k in koord)
            {
                sb.Append("{lat: " + k.GeografskaSirina.ToString() + "," + " lng: " + k.GeografskaDuzina.ToString() + " },");
            }

            String s = sb.ToString();
            char[] charsToTrim = { ',' };
            s = s.TrimEnd(charsToTrim);
            s += "];";
            File.WriteAllText(GetPagePath("linijskikoordinate.js"), s);
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            Int32? id = dgv.SelectedRows[0].Cells["Id"].Value as Int32?;
            if (id == null)
            {
                MessageBox.Show("Greska po indeksu.");
                return;
            }
            bool success = false;
            MySession.Open();
            try
            {
                TackastiObjekat g = MySession.GetTackastiObjekat((int)id);
                success = true;
                NacrtajTackasti(g);

            }
            catch (Exception e1)
            {

            }
            try
            {
                LinijskiObjekat g = MySession.GetLinijskiObjekat((int)id);
                success = true;
                if (!NacrtajLinijski(g))
                {
                    MetroMessageBox.Show(this, "Nema dovoljno informacija u vidu koordinata da bi se izabrani linijski objekat iscrtao.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information, 200);
                    return;

                }

            }
            catch (Exception e1)
            {

            }
            try
            {
                PovrsinskiObjekat g = MySession.GetPovrsinskiObjekat((int)id);
                success = true;
                if (!NacrtajPovrsinski(g))
                {
                    MetroMessageBox.Show(this, "Nema dovoljno informacija u vidu koordinata da bi se izabrani površinski objekat iscrtao.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
                    return;
                }
            }
            catch (Exception e1)
            {

            }
            MySession.Close();
            if (success) browser.Load(GetPagePath("osnovni1.html"));
            //browser.Load("www.erepublik.com");
        }

        public void InitializeBrowser()
        {
            CefSettings settings = new CefSettings();
            CefSharpSettings.LegacyJavascriptBindingEnabled = true;
            Cef.Initialize(settings);
            CefSharp.BrowserSettings browser_setting = new CefSharp.BrowserSettings();
            browser = new ChromiumWebBrowser(GetPagePath("osnovni1.html"));
            browser.RegisterJsObject("inj", inj);


            panela.Controls.Add(browser);
            browser.Dock = DockStyle.Fill;

        }

        private string GetPagePath(string pageName)
        {
            return Path.Combine(pagesPath, pageName);
        }

        #endregion

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            InitializeBrowser();

            cbTipObjekta.SelectedIndex = 0;
            cbPodtipObjekta.SelectedIndex = 0;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (cbPodtipObjekta.Text == "Svi objekti")
            {
                MySession.Open();
                dgv.DataSource = (from x in MySession.GetAllGeografskiObjekat().Where(x => x.Naziv.ToUpper().Contains(tbNaziv.Text.ToUpper()) && !x.Naziv.EndsWith(" međa")).OrderBy(x => x.Naziv).ToList()
                                  select new
                                  {
                                      Id = x.Id,
                                      Naziv = x.Naziv
                                  }).ToList();

                ViewConfiguration.GeografskiObjekat(dgv);

                MySession.Close();
            }

            else if (cbPodtipObjekta.Text == "Svi tačkasti objekti")
            {
                MySession.Open();
                dgv.DataSource = (from x in MySession.GetAllTackastiObjekat().Where(x => x.Naziv.ToUpper().Contains(tbNaziv.Text.ToUpper()) && !x.Naziv.EndsWith(" međa")).OrderBy(x => x.Naziv).ToList()
                                  select new
                                  {
                                      Id = x.Id,
                                      Naziv = x.Naziv,
                                      NadmorskaVisina = x.NadmorskaVisina,
                                      GeografskaSirina = x.GeografskaSirina,
                                      GeografskaDuzina = x.GeografskaDuzina,
                                      DatumPocetkaEvidencije = x.DatumPocetkaEvidencije
                                  }).ToList();

                ViewConfiguration.TackastiObjekat(dgv);

                MySession.Close();
            }
            if (cbPodtipObjekta.Text == "Naseljena mesta")
            {
                MySession.Open();
                dgv.DataSource = (from x in MySession.GetAllNaseljenoMesto().Where(x => x.Naziv.ToUpper().Contains(tbNaziv.Text.ToUpper()) && !x.Naziv.EndsWith(" međa")).OrderBy(x => x.Naziv).ToList()
                                  select new
                                  {
                                      Id = x.Id,
                                      Naziv = x.Naziv,
                                      NadmorskaVisina = x.NadmorskaVisina,
                                      GeografskaSirina = x.GeografskaSirina,
                                      GeografskaDuzina = x.GeografskaDuzina,
                                      DatumPocetkaEvidencije = x.DatumPocetkaEvidencije,
                                      BrojStanovnika = x.BrojStanovnika,
                                      Opstina = x.Opstina,
                                      DatumOsnivanja = x.DatumOsnivanja
                                  }).ToList();

                ViewConfiguration.NaseljenoMesto(dgv);

                MySession.Close();
            }
            if (cbPodtipObjekta.Text == "Turistička mesta")
            {
                MySession.Open();

                dgv.DataSource = (from x in MySession.GetAllTuristickoMesto().Where(x => x.Naziv.ToUpper().Contains(tbNaziv.Text.ToUpper()) && !x.Naziv.EndsWith(" međa")).OrderBy(x => x.Naziv).ToList()
                                 select new
                                 {
                                     Id = x.Id,
                                     Naziv = x.Naziv,
                                     NadmorskaVisina = x.NadmorskaVisina,
                                     GeografskaSirina = x.GeografskaSirina,
                                     GeografskaDuzina = x.GeografskaDuzina,
                                     DatumPocetkaEvidencije = x.DatumPocetkaEvidencije,
                                     BrojStanovnika = x.BrojStanovnika,
                                     Opstina = x.Opstina,
                                     DatumOsnivanja = x.DatumOsnivanja,
                                     TipTurizma = (x.TipTurizma == "banjski") ? "Banjski" :
                                        (x.TipTurizma == "letnji" ? "Letnji" : "Zimski"),
                                     DatumPocetkaTurizma = x.DatumPocetkaTurizma
                                 }).ToList();

                ViewConfiguration.TuristickoMesto(dgv);

                MySession.Close();
            }

            if (cbPodtipObjekta.Text == "Svi linijski objekti")
            {
                MySession.Open();
                dgv.DataSource = (from x in MySession.GetAllLinijskiObjekat().Where(x => x.Naziv.ToUpper().Contains(tbNaziv.Text.ToUpper()) && !x.Naziv.EndsWith(" međa")).OrderBy(x => x.Naziv).ToList()
                                  select new
                                  {
                                      Id = x.Id,
                                      Naziv = x.Naziv,
                                      Duzina = x.Duzina,
                                      TipLinijskogObjekta = (x.TipLinijskogObjekta == "reka") ? "Reka" :
                                         (x.TipLinijskogObjekta == "put" ? "Put" : "Granična linija")
                                  }).ToList();

                ViewConfiguration.LinijskiObjekat(dgv);

                MySession.Close();
            }
            if (cbPodtipObjekta.Text == "Putevi")
            {
                MySession.Open();
                dgv.DataSource = (from x in MySession.GetAllPut().Where(x => x.Naziv.ToUpper().Contains(tbNaziv.Text.ToUpper()) && !x.Naziv.EndsWith(" međa")).OrderBy(x => x.Naziv).ToList()
                                  select new
                                  {
                                      Id = x.Id,
                                      Naziv = x.Naziv,
                                      Duzina = x.Duzina,
                                      KlasaPuta = (x.KlasaPuta == "auto-put") ? "Auto-put" :
                                        (x.KlasaPuta == "1." ? "1. klasa" : (x.KlasaPuta == "2." ? "2. klasa" : "3. klasa"))
                                  }).ToList();

                ViewConfiguration.Put(dgv);

                MySession.Close();
            }
            if (cbPodtipObjekta.Text == "Reke")
            {
                MySession.Open();
                dgv.DataSource = (from x in MySession.GetAllReka().Where(x => x.Naziv.ToUpper().Contains(tbNaziv.Text.ToUpper()) && !x.Naziv.EndsWith(" međa")).OrderBy(x => x.Naziv).ToList()
                                  select new
                                  {
                                      Id = x.Id,
                                      Naziv = x.Naziv,
                                      Duzina = x.Duzina
                                  }).ToList();

                ViewConfiguration.Reka(dgv);

                MySession.Close();
            }
            if (cbPodtipObjekta.Text == "Granične linije")
            {
                MySession.Open();
                dgv.DataSource = (from x in MySession.GetAllGranicnaLinija().Where(x => x.Naziv.ToUpper().Contains(tbNaziv.Text.ToUpper()) && !x.Naziv.EndsWith(" međa")).OrderBy(x => x.Naziv).ToList()
                                  select new
                                  {
                                      Id = x.Id,
                                      Naziv = x.Naziv,
                                      Duzina = x.Duzina,
                                      ImeDrzave = x.ImeDrzave
                                  }).ToList();

                ViewConfiguration.GranicnaLinija(dgv);

                MySession.Close();
            }

            if (cbPodtipObjekta.Text == "Svi površinski objekti")
            {
                MySession.Open();
                dgv.DataSource = (from x in MySession.GetAllPovrsinskiObjekat().Where(x => x.Naziv.ToUpper().Contains(tbNaziv.Text.ToUpper()) && !x.Naziv.EndsWith(" međa")).OrderBy(x => x.Naziv).ToList()
                                  select new
                                  {
                                      Id = x.Id,
                                      Naziv = x.Naziv,
                                      TipPovrsinskogObjekta = (x.TipPovrsinskogObjekta == "uzvisenje") ? "Uzvišenje" : "Vodena površina"
                                  }).ToList();

                ViewConfiguration.PovrsinskiObjekat(dgv);

                MySession.Close();
            }
            if (cbPodtipObjekta.Text == "Vodene površine")
            {
                MySession.Open();
                dgv.DataSource = (from x in MySession.GetAllVodenaPovrsina().Where(x => x.Naziv.ToUpper().Contains(tbNaziv.Text.ToUpper()) && !x.Naziv.EndsWith(" međa")).OrderBy(x => x.Naziv).ToList()
                                  select new
                                  {
                                      Id = x.Id,
                                      Naziv = x.Naziv,
                                      TipVodenePovrsine = (x.TipVodenePovrsine == "more") ? "More" :
                                         (x.TipVodenePovrsine == "jezero" ? "Jezero" : "Bara")
                                  }).ToList();

                ViewConfiguration.VodenaPovrsina(dgv);

                MySession.Close();
            }
            if (cbPodtipObjekta.Text == "Uzvišenja")
            {
                MySession.Open();
                dgv.DataSource = (from x in MySession.GetAllUzvisenje().Where(x => x.Naziv.ToUpper().Contains(tbNaziv.Text.ToUpper()) && !x.Naziv.EndsWith(" međa")).OrderBy(x => x.Naziv).ToList()
                                  select new
                                  {
                                      Id = x.Id,
                                      Naziv = x.Naziv,
                                      NadmorskaVisina = x.NadmorskaVisina
                                  }).ToList();

                ViewConfiguration.Uzvisenje(dgv);

                MySession.Close();
            }
        }

        private void cbTip_TextChanged(object sender, EventArgs e)
        {
            if (cbTipObjekta.Text == "Svi objekti")
            {
                cbPodtipObjekta.Items.Clear();
                cbPodtipObjekta.Items.AddRange(new object[] {
                    "Svi objekti"});
                cbPodtipObjekta.SelectedIndex = 0;
                cbPodtipObjekta.Enabled = false;
            }
            else if (cbTipObjekta.Text == "Tačkasti objekti")
            {
                cbPodtipObjekta.Items.Clear();
                cbPodtipObjekta.Items.AddRange(new object[] {
                    "Svi tačkasti objekti",
                    "Naseljena mesta",
                    "Turistička mesta"});
                cbPodtipObjekta.SelectedIndex = 0;
                cbPodtipObjekta.Enabled = true;
            }
            else if (cbTipObjekta.Text == "Linijski objekti")
            {
                cbPodtipObjekta.Items.Clear();
                cbPodtipObjekta.Items.AddRange(new object[] {
                    "Svi linijski objekti",
                    "Putevi",
                    "Reke",
                    "Granične linije"});
                cbPodtipObjekta.SelectedIndex = 0;
                cbPodtipObjekta.Enabled = true;
            }
            else if (cbTipObjekta.Text == "Površinski objekti")
            {
                cbPodtipObjekta.Items.Clear();
                cbPodtipObjekta.Items.AddRange(new object[] {
                    "Svi površinski objekti",
                    "Vodene površine",
                    "Uzvišenja"});
                cbPodtipObjekta.SelectedIndex = 0;
                cbPodtipObjekta.Enabled = true;
            }
        }

        private void cbPodtipObjekta_TextChanged(object sender, EventArgs e)
        {
            if (cbPodtipObjekta.Text == "Svi objekti")
            {
                btnDugme1.Visible = false;
                btnDugme2.Visible = false;
                btnDugme3.Visible = false;
            }
            else if (cbPodtipObjekta.Text == "Svi tačkasti objekti")
            {
                btnDugme1.Text = "Spisak linijskih objekata";
                btnDugme1.Visible = true;
                btnDugme2.Visible = false;
                btnDugme3.Visible = false;
            }
            else if (cbPodtipObjekta.Text == "Naseljena mesta" || cbPodtipObjekta.Text == "Turistička mesta")
            {
                btnDugme1.Text = "Spisak linijskih objekata";
                btnDugme1.Visible = true;
                btnDugme2.Text = "Lista znamenitosti mesta";
                btnDugme2.Visible = true;
                btnDugme3.Visible = false;
            }
            else if (cbTipObjekta.Text == "Linijski objekti")
            {
                btnDugme1.Text = "Lista koordinata objekta";
                btnDugme1.Visible = true;
                btnDugme2.Text = "Tačkasti objekti na datom";
                btnDugme2.Visible = true;
                btnDugme3.Text = "Površinski objekti okolo";
                btnDugme3.Visible = true;
            }
            else if (cbPodtipObjekta.Text == "Svi površinski objekti" || cbPodtipObjekta.Text == "Vodene površine")
            {
                btnDugme1.Text = "Međa datog objekta";
                btnDugme1.Visible = true;
                btnDugme2.Visible = false;
                btnDugme3.Visible = false;
            }
            else if (cbPodtipObjekta.Text == "Uzvišenja")
            {
                btnDugme1.Text = "Međa datog objekta";
                btnDugme1.Visible = true;
                btnDugme2.Text = "Spisak vrhova";
                btnDugme2.Visible = true;
                btnDugme3.Text = "Objekti unutar uzvišenja";
                btnDugme3.Visible = true;
            }
            btnSearch.PerformClick();
        }

        private void btnDugme1_Click(object sender, EventArgs e)
        {
            if (cbTipObjekta.Text == "Površinski objekti")
            {
                if (dgv.SelectedRows.Count == 0)
                {
                    return;
                }
                Int32? id = dgv.SelectedRows[0].Cells["Id"].Value as Int32?;
                if (id == null)
                {
                    return;
                }
                MySession.Open();
                PovrsinskiObjekat pob = MySession.GetPovrsinskiObjekat(id.Value);
                var rezultat = (from x in pob.Linijski.Where(x => !x.Naziv.EndsWith(" međa")).OrderBy(x => x.Naziv)
                                select new
                                {
                                    Id = x.Id,
                                    Naziv = x.Naziv,
                                    Duzina = x.Duzina,
                                    TipLinijskogObjekta = (x.TipLinijskogObjekta == "reka") ? "Reka" :
                                         (x.TipLinijskogObjekta == "put" ? "Put" : "Granična linija")
                                }).ToList();
                MySession.Close();
                DodatniPrikaz dp = new DodatniPrikaz("Međa datog objekta",
                    new ViewConfigurationSelect(ViewConfiguration.LinijskiObjekat), rezultat);
                dp.ShowDialog();
            }
            else if (cbTipObjekta.Text == "Linijski objekti")
            {
                if (dgv.SelectedRows.Count == 0)
                {
                    return;
                }
                Int32? id = dgv.SelectedRows[0].Cells["Id"].Value as Int32?;
                if (id == null)
                {
                    return;
                }
                MySession.Open();
                LinijskiObjekat lob = MySession.GetLinijskiObjekat(id.Value);
                var rezultat = (from x in lob.Koordinate.OrderBy(x => x.Indeks)
                                select new
                                {
                                    Indeks = x.Indeks,
                                    GeografskaSirina = x.GeografskaSirina,
                                    GeografskaDuzina = x.GeografskaDuzina
                                }).ToList();
                MySession.Close();
                DodatniPrikaz dp = new DodatniPrikaz("Lista koordinata objekta",
                    new ViewConfigurationSelect(ViewConfiguration.Koordinata), rezultat);
                dp.ShowDialog();
            }
            else if (cbTipObjekta.Text == "Tačkasti objekti")
            {
                if (dgv.SelectedRows.Count == 0)
                {
                    return;
                }
                Int32? id = dgv.SelectedRows[0].Cells["Id"].Value as Int32?;
                if (id == null)
                {
                    return;
                }
                MySession.Open();
                TackastiObjekat tob = MySession.GetTackastiObjekat(id.Value);
                var lista = (from x in tob.NalaziSeNa
                             orderby x.Linijski.Naziv
                             select new
                             {
                                 RedniBroj = x.RedniBroj,
                                 Naziv = x.Linijski.Naziv,
                                 Duzina = x.Linijski.Duzina,
                                 TipLinijskogObjekta = (x.Linijski.TipLinijskogObjekta == "reka") ? "Reka" :
                                         (x.Linijski.TipLinijskogObjekta == "put" ? "Put" : "Granična linija"),
                                 RastojanjeNaredni = x.RastojanjeNaredni
                             }).ToList();
                MySession.Close();
                DodatniPrikaz dp = new DodatniPrikaz("Spisak linijskih objekata",
                    new ViewConfigurationSelect(ViewConfiguration.NalaziSeNaLinijski), lista);
                dp.ShowDialog();
            }
        }

        private void btnDugme2_Click(object sender, EventArgs e)
        {
            if (cbTipObjekta.Text == "Linijski objekti")
            {
                if (dgv.SelectedRows.Count == 0)
                {
                    return;
                }
                Int32? id = dgv.SelectedRows[0].Cells["Id"].Value as Int32?;
                if (id == null)
                {
                    return;
                }
                MySession.Open();
                LinijskiObjekat lob = MySession.GetLinijskiObjekat(id.Value);
                var lista = (from x in lob.NalaziSeNa
                             orderby x.RedniBroj ascending
                             select new
                             {
                                 RedniBroj = x.RedniBroj,
                                 Naziv = x.Tackasti.Naziv,
                                 NadmorskaVisina = x.Tackasti.NadmorskaVisina,
                                 GeografskaSirina = x.Tackasti.GeografskaSirina,
                                 GeografskaDuzina = x.Tackasti.GeografskaDuzina,
                                 DatumPocetkaEvidencije = x.Tackasti.DatumPocetkaEvidencije,
                                 RastojanjeNaredni = x.RastojanjeNaredni
                             }).ToList();
                MySession.Close();
                DodatniPrikaz dp = new DodatniPrikaz("Tačkasti objekti na datom",
                    new ViewConfigurationSelect(ViewConfiguration.NalaziSeNaTackasti), lista);
                dp.ShowDialog();
            }
            else if (cbPodtipObjekta.Text == "Naseljena mesta" || cbPodtipObjekta.Text == "Turistička mesta")
            {
                if (dgv.SelectedRows.Count == 0)
                {
                    return;
                }
                Int32? id = dgv.SelectedRows[0].Cells["Id"].Value as Int32?;
                if (id == null)
                {
                    return;
                }
                MySession.Open();
                NaseljenoMesto nm = MySession.GetNaseljenoMesto(id.Value);
                var rezultat = (from x in nm.Znamenitosti
                                orderby x.Ime
                                select new
                                {
                                    Ime = x.Ime,
                                    Opis = x.Opis
                                }).ToList();
                MySession.Close();
                DodatniPrikaz dp = new DodatniPrikaz("Lista znamenitosti mesta",
                    new ViewConfigurationSelect(ViewConfiguration.Znamenitost), rezultat);
                dp.ShowDialog();
            }
            else if (cbPodtipObjekta.Text == "Uzvišenja")
            {
                if (dgv.SelectedRows.Count == 0)
                {
                    return;
                }
                Int32? id = dgv.SelectedRows[0].Cells["Id"].Value as Int32?;
                if (id == null)
                {
                    return;
                }
                MySession.Open();
                Uzvisenje uzvisenje = MySession.GetUzvisenje(id.Value);
                var rezultat = (from x in uzvisenje.Vrhovi.OrderBy(y => y.Naziv)
                                select new
                                {
                                    Naziv = x.Naziv,
                                    NadmorskaVisina = x.NadmorskaVisina
                                }).ToList();
                MySession.Close();
                DodatniPrikaz dp = new DodatniPrikaz("Spisak vrhova",
                    new ViewConfigurationSelect(ViewConfiguration.Vrh), rezultat);
                dp.ShowDialog();
            }
        }

        private void btnDugme3_Click(object sender, EventArgs e)
        {
            if (cbPodtipObjekta.Text == "Uzvišenja")
            {
                if (dgv.SelectedRows.Count == 0)
                {
                    return;
                }
                Int32? id = dgv.SelectedRows[0].Cells["Id"].Value as Int32?;
                if (id == null)
                {
                    return;
                }
                MySession.Open();
                Uzvisenje uzvisenje = MySession.GetUzvisenje(id.Value);
                var rezultat = (from x in uzvisenje.Objekti.OrderBy(y => y.Naziv)
                                select new
                                {
                                    Id = x.Id,
                                    Naziv = x.Naziv
                                }).ToList();
                MySession.Close();
                DodatniPrikaz dp = new DodatniPrikaz("Objekti unutar uzvišenja",
                    new ViewConfigurationSelect(ViewConfiguration.GeografskiObjekat), rezultat);
                dp.ShowDialog();
            }
            else if (cbTipObjekta.Text == "Linijski objekti")
            {
                if (dgv.SelectedRows.Count == 0)
                {
                    return;
                }
                Int32? id = dgv.SelectedRows[0].Cells["Id"].Value as Int32?;
                if (id == null)
                {
                    return;
                }
                MySession.Open();
                LinijskiObjekat lob = MySession.GetLinijskiObjekat(id.Value);
                var rezultat = (from x in lob.Povrsinski.OrderBy(y => y.Naziv)
                                select new
                                {
                                    Id = x.Id,
                                    Naziv = x.Naziv,
                                    TipPovrsinskogObjekta = (x.TipPovrsinskogObjekta == "uzvisenje") ? "Uzvišenje" : "Vodena površina"
                                }).ToList();
                MySession.Close();
                DodatniPrikaz dp = new DodatniPrikaz("Površinski objekti okolo",
                    new ViewConfigurationSelect(ViewConfiguration.PovrsinskiObjekat), rezultat);
                dp.ShowDialog();
            }
        }

        private void btnDodajNovi_Click(object sender, EventArgs e)
        {
            CreateIzbor izbor = new CreateIzbor();
            var r = izbor.ShowDialog();
            if (r == DialogResult.OK)
            {
                //panel1.Controls.Remove(browser);
                var f = new CreateTackastiForm();
                f.ShowDialog();
                //panel1.Controls.Add(browser);
            }
            else if (r == DialogResult.Yes)
            {
                //yes vraca ako je izabran linijski
                var f = new CreateLinijskiForm();
                f.ShowDialog();
            }
            else if (r == DialogResult.Retry)
            {
                var f = new CreatePovrsinskiForm(Mode.Create);
                f.ShowDialog();
            }
            btnSearch.PerformClick();
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count == 0)
            {
                return;
            }
            Int32? id = dgv.SelectedRows[0].Cells["Id"].Value as Int32?;
            if (id == null)
            {
                return;
            }

            MySession.Open();
            GeografskiObjekat go = MySession.GetGeografskiObjekat(id.Value);

            NaseljenoMesto naseljenoMesto = go as NaseljenoMesto;
            if (naseljenoMesto != null)
            {
                if (naseljenoMesto.NalaziSeNa.Count > 0)
                {
                    MySession.Close();
                    MessageBox.Show("Nesto ne valja, brate");
                    return;
                }
                foreach (Znamenitost znam in naseljenoMesto.Znamenitosti)
                {
                    MySession.Delete(znam);
                }
                foreach (Uzvisenje uzvis in MySession.GetAllUzvisenje())
                {
                    GeografskiObjekat result = null;
                    foreach (GeografskiObjekat gob in uzvis.Objekti)
                    {
                        if (gob.Id == id.Value)
                        {
                            result = gob;
                            break;
                        }
                    }
                    if (result != null)
                    {
                        uzvis.Objekti.Remove(result);
                        MySession.Update(uzvis);
                    }
                }
                MySession.Delete(naseljenoMesto);
                MySession.Close();
                btnSearch.PerformClick();
                return;
            }
            TackastiObjekat tackastiObjekat = go as TackastiObjekat;
            if (tackastiObjekat != null)
            {
                if (tackastiObjekat.NalaziSeNa.Count > 0)
                {
                    MySession.Close();
                    MessageBox.Show("Nesto ne valja, brate");
                    return;
                }
                foreach (Uzvisenje uzvis in MySession.GetAllUzvisenje())
                {
                    GeografskiObjekat result = null;
                    foreach (GeografskiObjekat gob in uzvis.Objekti)
                    {
                        if (gob.Id == id.Value)
                        {
                            result = gob;
                            break;
                        }
                    }
                    if (result != null)
                    {
                        uzvis.Objekti.Remove(result);
                        MySession.Update(uzvis);
                    }
                }
                MySession.Delete(tackastiObjekat);
                MySession.Close();
                btnSearch.PerformClick();
                return;
            }

            LinijskiObjekat linijskiObjekat = go as LinijskiObjekat;
            if (linijskiObjekat != null)
            {
                if (linijskiObjekat.Povrsinski.Count > 0)
                {
                    MySession.Close();
                    MessageBox.Show("Nesto ne valja, brate");
                    return;
                }
                foreach (Koordinata koor in linijskiObjekat.Koordinate)
                {
                    MySession.Delete(koor);
                }
                linijskiObjekat.Koordinate = new List<Koordinata>();
                MySession.Update(linijskiObjekat);
                MySession.Flush();
                foreach (NalaziSeNa nsn in linijskiObjekat.NalaziSeNa)
                {
                    TackastiObjekat tob = nsn.Tackasti;
                    MySession.Delete(nsn);
                    MySession.Update(tob);
                    MySession.Flush();
                }
                linijskiObjekat.NalaziSeNa = new List<NalaziSeNa>();
                MySession.Update(linijskiObjekat);
                MySession.Flush();

                foreach (Uzvisenje uzvis in MySession.GetAllUzvisenje())
                {
                    GeografskiObjekat result = null;
                    foreach (GeografskiObjekat gob in uzvis.Objekti)
                    {
                        if (gob.Id == id.Value)
                        {
                            result = gob;
                            break;
                        }
                    }
                    if (result != null)
                    {
                        uzvis.Objekti.Remove(result);
                        MySession.Update(uzvis);
                    }
                }
                MySession.Delete(linijskiObjekat);
                MySession.Close();
                btnSearch.PerformClick();
                return;
            }

            Uzvisenje uzvisenje = go as Uzvisenje;
            if (uzvisenje != null)
            {
                LinijskiObjekat medja = null;

                foreach (LinijskiObjekat lob in uzvisenje.Linijski)
                {
                    if (lob.Naziv == uzvisenje.Naziv + " međa")
                    {
                        medja = lob;
                    }
                    lob.Povrsinski.Remove(uzvisenje);
                    MySession.Update(lob);
                }
                uzvisenje.Linijski = new List<LinijskiObjekat>();
                MySession.Update(uzvisenje);
                MySession.Flush();

                foreach(Koordinata koor in medja.Koordinate)
                {
                    MySession.Delete(koor);
                    MySession.Flush();
                }
                medja.Povrsinski = new List<PovrsinskiObjekat>();
                MySession.Delete(medja);
                MySession.Flush();

                foreach (Uzvisenje uzvis in MySession.GetAllUzvisenje())
                {
                    GeografskiObjekat result = null;
                    foreach (GeografskiObjekat gob in uzvis.Objekti)
                    {
                        if (gob.Id == id.Value)
                        {
                            result = gob;
                            break;
                        }
                    }
                    if (result != null)
                    {
                        uzvis.Objekti.Remove(result);
                        MySession.Update(uzvis);
                    }
                }
         
                foreach (Vrh vrh in uzvisenje.Vrhovi)
                {
                    MySession.Delete(vrh);
                }
                uzvisenje.Vrhovi = new List<Vrh>();
                uzvisenje.Objekti = new List<GeografskiObjekat>();
                MySession.Update(uzvisenje);

                MySession.Flush();

                MySession.Delete(uzvisenje);
                MySession.Close();
                btnSearch.PerformClick();
                return;
            }

            PovrsinskiObjekat povrsinskiObjekat = go as PovrsinskiObjekat;
            if (povrsinskiObjekat != null)
            {
                foreach (LinijskiObjekat lob in povrsinskiObjekat.Linijski)
                {
                    lob.Povrsinski.Remove(povrsinskiObjekat);
                    MySession.Update(lob);
                }
                povrsinskiObjekat.Linijski = new List<LinijskiObjekat>();
                MySession.Update(povrsinskiObjekat);
                MySession.Flush();

                foreach (Uzvisenje uzvis in MySession.GetAllUzvisenje())
                {
                    GeografskiObjekat result = null;
                    foreach (GeografskiObjekat gob in uzvis.Objekti)
                    {
                        if (gob.Id == id.Value)
                        {
                            result = gob;
                            break;
                        }
                    }
                    if (result != null)
                    {
                        uzvis.Objekti.Remove(result);
                        MySession.Update(uzvis);
                    }
                }
                MySession.Delete(povrsinskiObjekat);
                MySession.Close();
                btnSearch.PerformClick();
            }
        }

        private void btnAzuriraj_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count == 0)
            {
                return;
            }
            Int32? id = dgv.SelectedRows[0].Cells["Id"].Value as Int32?;
            if (id == null)
            {
                return;
            }

            MySession.Open();
            GeografskiObjekat go = MySession.GetGeografskiObjekat(id.Value);

            TackastiObjekat tackastiObjekat = go as TackastiObjekat;
            if (tackastiObjekat != null)
            {
                CreateTackastiForm ctf = new CreateTackastiForm(tackastiObjekat);
                ctf.ShowDialog();
                btnSearch.PerformClick();
                return;
            }

            LinijskiObjekat linijskiObjekat = go as LinijskiObjekat;
            if (linijskiObjekat != null)
            {
                CreateLinijskiForm clf = new CreateLinijskiForm(linijskiObjekat);
                clf.ShowDialog();
                btnSearch.PerformClick();
                return;
            }


            PovrsinskiObjekat povrsinskiObjekat = go as PovrsinskiObjekat;
            if (povrsinskiObjekat != null)
            {
                CreatePovrsinskiForm cpf = new CreatePovrsinskiForm(Mode.Update);
                cpf.PovrsinskiZaIzmenu = povrsinskiObjekat;
                cpf.ShowDialog();
                btnSearch.PerformClick();
                return;
            }
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            browser.ShowDevTools();
        }
    }
}
