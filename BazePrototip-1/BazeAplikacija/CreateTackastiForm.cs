﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using System.IO;
using Sistemi_Baza_Podataka_Deo_Drugi;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;
using NHibernate;

namespace BazeAplikacija
{
    public partial class CreateTackastiForm : MetroForm
    {
        private IList<Uzvisenje> izabranaUzvisenja = null;

        private TackastiObjekat workingObject;

        public CreateTackastiForm()
        {
            workingObject = null;
            InitializeComponent();
            InitializeBrowser(43.331281, 21.892535);
            //LatInChanged += LatInChangedHandler;
            //LngInChanged += LngInChangedHandler;
            //AltInChanged += AltInChangedHandler;
            dtpNaseljeno.Format = DateTimePickerFormat.Custom;
            dtpNaseljeno.CustomFormat = "dd.MM.yyyy.";
            cbTipTurizma.SelectedIndex = 0;
            dtpTuristicko.Format = DateTimePickerFormat.Custom;
            dtpTuristicko.CustomFormat = "dd.MM.yyyy.";
        }

        public CreateTackastiForm(TackastiObjekat to)
        {
            InitializeComponent();
            InitializeBrowser(to.GeografskaSirina, to.GeografskaDuzina);

            workingObject = to;
            this.Text = "Ažuriraj tačkasti objekat";
            this.metroButton4.Text = "Аžuriraj";
            tbNaziv.Text = to.Naziv;
            tbProbaLat.Text = to.GeografskaSirina.ToString();
            tbProbaLen.Text = to.GeografskaDuzina.ToString();
            tbAlt.Text = to.NadmorskaVisina.ToString();

            izabranaUzvisenja = new List<Uzvisenje>();
            foreach (Uzvisenje uzv in MySession.GetAllUzvisenje())
            {
                bool unutra = false;
                foreach (GeografskiObjekat go in uzv.Objekti)
                {
                    if (go.Id == to.Id)
                    {
                        unutra = true;
                        break;
                    }
                }
                if (unutra)
                {
                    izabranaUzvisenja.Add(uzv);
                }
            }
            cbNaseljeno.Enabled = false;
            cbTuristicko.Enabled = false;
            if (to.NaseljenoMestoFlag == "F")
            {
                cbNaseljeno.Checked = false;
            }
            else
            {
                cbNaseljeno.Checked = true;
                NaseljenoMesto nm = to as NaseljenoMesto;
                tbBrojStanovnika.Text = nm.BrojStanovnika.ToString();
                tbOpstina.Text = nm.Opstina;

                if (nm.DatumOsnivanja >= dtpNaseljeno.MinDate && nm.DatumOsnivanja <= dtpNaseljeno.MaxDate)
                {
                    dtpNaseljeno.Value = nm.DatumOsnivanja;
                }
                else
                {
                    dtpNaseljeno.Value = DateTime.Now;
                }
                foreach(Znamenitost znam in nm.Znamenitosti)
                {
                    DataGridViewRow row = (DataGridViewRow)lvZnamenitosti.Rows[0].Clone();
                    row.Cells[0].Value = znam.Ime;
                    row.Cells[1].Value = znam.Opis;
                    lvZnamenitosti.Rows.Add(row);
                }
                if (nm.TuristickoMestoFlag == "F")
                {
                    cbTuristicko.Checked = false;
                }
                else
                {
                    cbTuristicko.Checked = true;
                    TuristickoMesto tm = nm as TuristickoMesto;
                    cbTipTurizma.SelectedItem = cbTipTurizma.Items.OfType<String>().First(x => x.ToLower() == tm.TipTurizma);

                    if (tm.DatumPocetkaTurizma >= dtpTuristicko.MinDate && tm.DatumPocetkaTurizma <= dtpTuristicko.MaxDate)
                    {
                        dtpTuristicko.Value = tm.DatumPocetkaTurizma;
                    }
                    else
                    {
                        dtpTuristicko.Value = DateTime.Now;
                    }
                }
            }

            MySession.Close();

            //LatInChanged += LatInChangedHandler;
            //LngInChanged += LngInChangedHandler;
            //AltInChanged += AltInChangedHandler;
            dtpNaseljeno.Format = DateTimePickerFormat.Custom;
            dtpNaseljeno.CustomFormat = "dd.MM.yyyy.";
            if (cbTipTurizma.SelectedIndex == -1)
            {
                cbTipTurizma.SelectedIndex = 0;
            }
            dtpTuristicko.Format = DateTimePickerFormat.Custom;
            dtpTuristicko.CustomFormat = "dd.MM.yyyy.";
        }

        public ChromiumWebBrowser browser;
        private static readonly string myPath = Application.StartupPath;
        private static readonly string pagesPath = Path.Combine(myPath, "pages");
        private Inject inj;

        public void InitializeBrowser(double gs, double gd)
        {
            browser = new ChromiumWebBrowser(GetPagePath("osnovni1.html"));
            inj = new Inject(this, gs, gd);
            browser.RegisterJsObject("inj", inj); 
            panel1.Controls.Add(browser);
            browser.Dock = DockStyle.Fill;
        }

        private string GetPagePath(string pageName)
        {
            return Path.Combine(pagesPath, pageName);
        }

        //ovde se Inject koristi samo za vracanje kliknutih koordinata
        public class Inject
        {
            private CreateTackastiForm myForm;
            private string tipObjekta = "unostackastog";
            //startne koordinate
            private double geografskaSirina;
            private double geografskaDuzina;

            private double latIn;
            private double lngIn;
            private double altIn;

            /*public Inject(CreateTackastiForm f)
            {
                myForm = f;
            }*/

            public Inject(CreateTackastiForm f, double gs, double gd)
            {
                myForm = f;
                geografskaSirina = gs;
                geografskaDuzina = gd;
            }

            public string TipObjekta { get => tipObjekta; set => tipObjekta = value; }

            public string GetTipObjekta()
            {
                return TipObjekta;
            }

            public double GetGeografskaSirina()
            {
                return geografskaSirina;
            }

            public double GetGeografskaDuzina()
            {
                return geografskaDuzina;
            }

            public void SetLatLngAltIn(double x, double y, double z)
            {
                latIn = x;
                lngIn = y;
                altIn = z;
                LatLngAltChanged d = myForm.LatLngAltInChangedHandler;
                myForm.Invoke(d, new object[] { x,y,z });
            }
        }

        private delegate void LatLngAltChanged(double newLat,double newLng, double newAlt);

        private void LatLngAltInChangedHandler(double newLat, double newLng, double newAlt)
        {
            tbProbaLat.Text = newLat.ToString();
            tbProbaLen.Text = newLng.ToString();
            tbAlt.Text = newAlt.ToString();
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            browser.ShowDevTools();
        }

        private void cbNaseljeno_CheckedChanged(object sender, EventArgs e)
        {
            if (cbNaseljeno.Checked)
            {
                tbBrojStanovnika.Enabled = true;
                tbOpstina.Enabled = true;
                dtpNaseljeno.Enabled = true;
                tbImeZnamenitosti.Enabled = true;
                tbOpisZnamenitosti.Enabled = true;
                lvZnamenitosti.Enabled = true;
            }
            else
            {
                tbBrojStanovnika.Enabled = false;
                tbOpstina.Enabled = false;
                dtpNaseljeno.Enabled = false;
                tbImeZnamenitosti.Enabled = false;
                tbOpisZnamenitosti.Enabled = false;
                lvZnamenitosti.Enabled = false;
            }
        }

        private void cbTuristicko_CheckedChanged(object sender, EventArgs e)
        {
            if (cbTuristicko.Checked)
            {
                cbTipTurizma.Enabled = true;
                dtpTuristicko.Enabled = true;
                cbNaseljeno.Checked = true;
            }
            else
            {
                cbTipTurizma.Enabled = false;
                dtpTuristicko.Enabled = false;
            }
        }

        public void RemoveText(object sender, EventArgs e)
        {
            if(tbImeZnamenitosti.Text == "Naziv znamenitosti") tbImeZnamenitosti.Text = "";
        }

        public void AddText(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbImeZnamenitosti.Text))
                tbImeZnamenitosti.Text = "Naziv znamenitosti";
        }

        public void RemoveText1(object sender, EventArgs e)
        {
            if(tbOpisZnamenitosti.Text == "Opis znamenitosti") tbOpisZnamenitosti.Text = "";
        }

        public void AddText1(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbOpisZnamenitosti.Text))
                tbOpisZnamenitosti.Text = "Opis znamenitosti";
        }

        private void btnUnesiZnamenitost_Click(object sender, EventArgs e)
        {
            if (tbImeZnamenitosti.Text == "" || tbOpisZnamenitosti.Text == "")
            {
                return;
            }
            lvZnamenitosti.Rows.Add(tbImeZnamenitosti.Text, tbOpisZnamenitosti.Text);
        }

        private void metroButton3_Click(object sender, EventArgs e)
        {
            var iu = new IzborUzvisenja();
            if (iu.ShowDialog() == DialogResult.OK)
            {
                izabranaUzvisenja = iu.ListaIzabranihUzvisenja;
            }
        }

        private bool Validacija()
        {
            if (tbNaziv.Text == "" || tbNaziv.Text.EndsWith(" međa"))
            {
                return false;
            }
            if (tbProbaLat.Text == "" || tbProbaLen.Text == "" || tbAlt.Text == "")
            {
                return false;
            }
            return true;
        }

        private void metroButton4_Click(object sender, EventArgs e)
        {
            if (!Validacija())
            {
                MessageBox.Show("Ne moze brate");
                return;
            }

            if (workingObject != null)
            {
                if (cbNaseljeno.Checked == false && cbTuristicko.Checked == false)
                {
                    MySession.Open();

                    TackastiObjekat to = MySession.GetTackastiObjekat(workingObject.Id);
                    to.Naziv = tbNaziv.Text;
                    to.GeografskaSirina = Double.Parse(tbProbaLat.Text);
                    to.GeografskaDuzina = Double.Parse(tbProbaLen.Text);
                    to.NadmorskaVisina = Double.Parse(tbAlt.Text);

                    MySession.Update(to);

                    foreach (Uzvisenje uz in MySession.GetAllUzvisenje())
                    {
                        GeografskiObjekat workingResult = null;
                        foreach (GeografskiObjekat gob in uz.Objekti)
                        {
                            if (gob.Id == workingObject.Id)
                            {
                                workingResult = gob;
                                break;
                            }
                        }
                        bool unutra = false;
                        foreach (Uzvisenje u in izabranaUzvisenja)
                        {
                            if (u.Id == uz.Id)
                            {
                                unutra = true;
                                break;
                            }
                        }
                        if (unutra && workingResult == null)
                        {
                            uz.Objekti.Add(to);
                            MySession.Update(uz);
                            MySession.Flush();
                        }
                        if (!unutra && workingResult != null)
                        {
                            uz.Objekti.Remove(workingResult);
                            MySession.Update(uz);
                            MySession.Flush();
                        }
                    }
                    MySession.Close();
                }

                if (cbNaseljeno.Checked == true && cbTuristicko.Checked == false)
                {
                    MySession.Open();

                    NaseljenoMesto no = MySession.GetNaseljenoMesto(workingObject.Id);
                    no.Naziv = tbNaziv.Text;
                    no.GeografskaSirina = Double.Parse(tbProbaLat.Text);
                    no.GeografskaDuzina = Double.Parse(tbProbaLen.Text);
                    no.NadmorskaVisina = Double.Parse(tbAlt.Text);
                    no.Opstina = tbOpstina.Text;
                    no.DatumOsnivanja = dtpNaseljeno.Value;

                    bool jelMoze;
                    int resul;
                    jelMoze = int.TryParse(tbBrojStanovnika.Text, out resul);
                    if (jelMoze)
                    {
                        no.BrojStanovnika = resul;
                    }
                    else
                    {
                        no.BrojStanovnika = 0;
                    }
                    if (no.BrojStanovnika < 0)
                    {
                        no.BrojStanovnika = 0;
                    }

                    foreach (Znamenitost znam in no.Znamenitosti)
                    {
                        MySession.Delete(znam);
                    }
                    if (lvZnamenitosti.Rows.Count != 0)
                    {
                        IList<Znamenitost> lz = new List<Znamenitost>();
                        foreach (DataGridViewRow a in lvZnamenitosti.Rows)
                        {
                            if (a.Cells[0].Value == null)
                            {
                                continue;
                            }
                            lz.Add(new Znamenitost()
                            {
                                Ime = (string)a.Cells[0].Value,
                                Opis = (string)a.Cells[1].Value,
                            });
                        }
                        no.Znamenitosti = lz;
                        foreach (Znamenitost znam in no.Znamenitosti)
                        {
                            znam.MyNaseljenoMesto = no;
                        }
                    }
                    MySession.Update(no);

                    foreach (Uzvisenje uz in MySession.GetAllUzvisenje())
                    {
                        GeografskiObjekat workingResult = null;
                        foreach (GeografskiObjekat gob in uz.Objekti)
                        {
                            if (gob.Id == workingObject.Id)
                            {
                                workingResult = gob;
                                break;
                            }
                        }
                        bool unutra = false;
                        foreach (Uzvisenje u in izabranaUzvisenja)
                        {
                            if (u.Id == uz.Id)
                            {
                                unutra = true;
                                break;
                            }
                        }
                        if (unutra && workingResult == null)
                        {
                            uz.Objekti.Add(no);
                            MySession.Update(uz);
                            MySession.Flush();
                        }
                        if (!unutra && workingResult != null)
                        {
                            uz.Objekti.Remove(workingResult);
                            MySession.Update(uz);
                            MySession.Flush();
                        }
                    }
                    MySession.Close();
                }
                else if (cbNaseljeno.Checked == true && cbTuristicko.Checked == true)
                {
                    MySession.Open();

                    TuristickoMesto to = MySession.GetTuristickoMesto(workingObject.Id);

                    to.Naziv = tbNaziv.Text;
                    to.GeografskaSirina = Double.Parse(tbProbaLat.Text);
                    to.GeografskaDuzina = Double.Parse(tbProbaLen.Text);
                    to.NadmorskaVisina = Double.Parse(tbAlt.Text);
                    to.Opstina = tbOpstina.Text;
                    to.DatumOsnivanja = dtpNaseljeno.Value;

                    bool jelMoze;
                    int resul;
                    jelMoze = int.TryParse(tbBrojStanovnika.Text, out resul);
                    if (jelMoze)
                    {
                        to.BrojStanovnika = resul;
                    }
                    else
                    {
                        to.BrojStanovnika = 0;
                    }
                    if (to.BrojStanovnika < 0)
                    {
                        to.BrojStanovnika = 0;
                    }

                    if (((string)cbTipTurizma.Text) == "Banjski")
                    {
                        to.TipTurizma = "banjski";
                    }
                    else if (((string)cbTipTurizma.Text) == "Letnji")
                    {
                        to.TipTurizma = "letnji";
                    }
                    else if (((string)cbTipTurizma.Text) == "Zimski")
                    {
                        to.TipTurizma = "zimski";
                    }
                    to.DatumPocetkaTurizma = dtpTuristicko.Value;

                    foreach (Znamenitost znam in to.Znamenitosti)
                    {
                        MySession.Delete(znam);
                    }
                    if (lvZnamenitosti.Rows.Count != 0)
                    {
                        IList<Znamenitost> lz = new List<Znamenitost>();
                        foreach (DataGridViewRow a in lvZnamenitosti.Rows)
                        {
                            if (a.Cells[0].Value == null)
                            {
                                continue;
                            }
                            lz.Add(new Znamenitost()
                            {
                                Ime = (string)a.Cells[0].Value,
                                Opis = (string)a.Cells[1].Value,
                            });
                        }
                        to.Znamenitosti = lz;
                        foreach (Znamenitost znam in to.Znamenitosti)
                        {
                            znam.MyNaseljenoMesto = to;
                        }
                    }
                    MySession.Update(to);

                    foreach (Uzvisenje uz in MySession.GetAllUzvisenje())
                    {
                        GeografskiObjekat workingResult = null;
                        foreach (GeografskiObjekat gob in uz.Objekti)
                        {
                            if (gob.Id == workingObject.Id)
                            {
                                workingResult = gob;
                                break;
                            }
                        }
                        bool unutra = false;
                        foreach (Uzvisenje u in izabranaUzvisenja)
                        {
                            if (u.Id == uz.Id)
                            {
                                unutra = true;
                                break;
                            }
                        }
                        if (unutra && workingResult == null)
                        {
                            uz.Objekti.Add(to);
                            MySession.Update(uz);
                            MySession.Flush();
                        }
                        if (!unutra && workingResult != null)
                        {
                            uz.Objekti.Remove(workingResult);
                            MySession.Update(uz);
                            MySession.Flush();
                        }
                    }
                    MySession.Close();
                }
                return;
            }

            if (cbNaseljeno.Checked == false && cbTuristicko.Checked == false)
            {
                TackastiObjekat to = new TackastiObjekat();
                to.Naziv = tbNaziv.Text;
                to.GeografskaSirina = Double.Parse(tbProbaLat.Text);
                to.GeografskaDuzina = Double.Parse(tbProbaLen.Text);
                to.NadmorskaVisina = Double.Parse(tbAlt.Text);
                to.DatumPocetkaEvidencije = DateTime.Now.Date;
                to.NaseljenoMestoFlag = "F";

                ISession s = DataLayer.GetSession();
                s.Save(to);

                if (izabranaUzvisenja != null && izabranaUzvisenja.Count != 0)
                {
                    foreach (Uzvisenje u in izabranaUzvisenja)
                    {
                        var managedUzvisenje = s.Merge(u);
                        managedUzvisenje.Objekti.Add(to);
                        s.Update(managedUzvisenje);
                    }
                }
                s.Flush();
                s.Close();
            }
            else if (cbNaseljeno.Checked == true && cbTuristicko.Checked == false)
            {
                NaseljenoMesto no = new NaseljenoMesto();
                no.Naziv = tbNaziv.Text;
                no.GeografskaSirina = Double.Parse(tbProbaLat.Text);
                no.GeografskaDuzina = Double.Parse(tbProbaLen.Text);
                no.NadmorskaVisina = Double.Parse(tbAlt.Text);
                no.DatumPocetkaEvidencije = DateTime.Now.Date;
                no.Opstina = tbOpstina.Text;
                no.DatumOsnivanja = dtpNaseljeno.Value;

                bool jelMoze;
                int resul;
                jelMoze = int.TryParse(tbBrojStanovnika.Text, out resul);
                if (jelMoze)
                {
                    no.BrojStanovnika = resul;
                }
                else
                {
                    no.BrojStanovnika = 0;
                }
                if (no.BrojStanovnika < 0)
                {
                    no.BrojStanovnika = 0;
                }

                no.NaseljenoMestoFlag = "T";
                no.TuristickoMestoFlag = "F";

                ISession s = DataLayer.GetSession();

                s.Save((TackastiObjekat)no);
                s.Save(no);

                if (lvZnamenitosti.Rows.Count != 0)
                {
                    IList<Znamenitost> lz = new List<Znamenitost>();
                    foreach (DataGridViewRow a in lvZnamenitosti.Rows)
                    {
                        if (a.Cells[0].Value == null)
                        {
                            continue;
                        }
                        lz.Add(new Znamenitost()
                        {
                            Ime = (string)a.Cells[0].Value,
                            Opis = (string)a.Cells[1].Value,
                        });
                    }
                    no.Znamenitosti = lz;
                    foreach (Znamenitost znam in no.Znamenitosti)
                    {
                        znam.MyNaseljenoMesto = no;
                    }
                }

                if (izabranaUzvisenja != null && izabranaUzvisenja.Count != 0)
                {
                    foreach (Uzvisenje u in izabranaUzvisenja)
                    {
                        var managedUzvisenje = s.Merge(u);
                        managedUzvisenje.Objekti.Add(no);
                        s.Update(managedUzvisenje);
                    }

                }
                s.Update(no);

                s.Flush();
                s.Close();
            }
            else if (cbNaseljeno.Checked == true && cbTuristicko.Checked == true)
            {
                TuristickoMesto to = new TuristickoMesto();
                to.Naziv = tbNaziv.Text;
                to.GeografskaSirina = Double.Parse(tbProbaLat.Text);
                to.GeografskaDuzina = Double.Parse(tbProbaLen.Text);
                to.NadmorskaVisina = Double.Parse(tbAlt.Text);
                to.DatumPocetkaEvidencije = DateTime.Now.Date;
                to.Opstina = tbOpstina.Text;
                to.DatumOsnivanja = dtpNaseljeno.Value;

                bool jelMoze;
                int resul;
                jelMoze = int.TryParse(tbBrojStanovnika.Text, out resul);
                if (jelMoze)
                {
                    to.BrojStanovnika = resul;
                }
                else
                {
                    to.BrojStanovnika = 0;
                }
                if (to.BrojStanovnika < 0)
                {
                    to.BrojStanovnika = 0;
                }

                to.NaseljenoMestoFlag = "T";
                to.TuristickoMestoFlag = "T";

                if (((string)cbTipTurizma.Text) == "Banjski")
                {
                    to.TipTurizma = "banjski";
                }
                else if (((string)cbTipTurizma.Text) == "Letnji")
                {
                    to.TipTurizma = "letnji";
                }
                else if (((string)cbTipTurizma.Text) == "Zimski")
                {
                    to.TipTurizma = "zimski";
                }
                to.DatumPocetkaTurizma = dtpTuristicko.Value;

                ISession s = DataLayer.GetSession();

                s.Save((TackastiObjekat)to);
                s.Save(to);

                if (lvZnamenitosti.Rows.Count != 0)
                {
                    IList<Znamenitost> lz = new List<Znamenitost>();
                    foreach (DataGridViewRow a in lvZnamenitosti.Rows)
                    {
                        if (a.Cells[0].Value == null)
                        {
                            continue;
                        }
                        lz.Add(new Znamenitost()
                        {
                            Ime = (string)a.Cells[0].Value,
                            Opis = (string)a.Cells[1].Value,
                        });
                    }
                    to.Znamenitosti = lz;
                    foreach (Znamenitost znam in to.Znamenitosti)
                    {
                        znam.MyNaseljenoMesto = to;
                    }
                }

                if (izabranaUzvisenja != null && izabranaUzvisenja.Count != 0)
                {
                    foreach (Uzvisenje u in izabranaUzvisenja)
                    {
                        var managedUzvisenje = s.Merge(u);
                        managedUzvisenje.Objekti.Add(to);
                        s.Update(managedUzvisenje);
                    }
                }
                s.Update(to);

                s.Flush();
                s.Close();
            }
        }
    }
}
