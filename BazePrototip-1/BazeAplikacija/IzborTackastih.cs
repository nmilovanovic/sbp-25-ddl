﻿using MetroFramework.Forms;
using Sistemi_Baza_Podataka_Deo_Drugi;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BazeAplikacija
{
    public partial class IzborTackastih : MetroForm
    {
        public TackastiObjekat IzabraniTackasti { get; set; }
        public IList<TackastiObjekat> ListaTackastih { get; set; }

        public IzborTackastih()
        {
            InitializeComponent();
        }

        private void IzborTackastih_Load(object sender, EventArgs e)
        {
            MySession.Open();
            ListaTackastih = (from p in MySession.GetAllTackastiObjekat() select p).ToList();
            //dgv.DataSource = (from u in ListaTackastih select new { Id = u.Id, Naziv = u.Naziv,u.GeografskaSirina,u.GeografskaDuzina, NadmorskaVisina = u.NadmorskaVisina }).ToList();
            foreach(TackastiObjekat t in ListaTackastih)
            {
                dgv.Rows.Add(t.Id, t.Naziv, t.GeografskaSirina, t.GeografskaDuzina, t.NadmorskaVisina);
            }
            MySession.Close();
            dgv.Columns[0].Visible = false;
            dgv.ClearSelection();
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count != 1)
            {
                DialogResult = DialogResult.None;
                Hide();
            }
            int id = Int32.Parse(dgv.SelectedRows[0].Cells[0].Value.ToString());
            IzabraniTackasti = (from t in ListaTackastih where t.Id == id select t).First();
            DialogResult = DialogResult.OK;
            Hide();
        }

        private void metroLabel1_Click(object sender, EventArgs e)
        {

        }
    }
}
