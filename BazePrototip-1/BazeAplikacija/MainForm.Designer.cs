﻿namespace BazeAplikacija
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panela = new MetroFramework.Controls.MetroPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dgv = new MetroFramework.Controls.MetroGrid();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.btnDodajNovi = new MetroFramework.Controls.MetroTile();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.btnDugme1 = new MetroFramework.Controls.MetroTile();
            this.btnDugme2 = new MetroFramework.Controls.MetroTile();
            this.btnDugme3 = new MetroFramework.Controls.MetroTile();
            this.btnObrisi = new MetroFramework.Controls.MetroTile();
            this.btnAzuriraj = new MetroFramework.Controls.MetroTile();
            this.btnPrikazi = new MetroFramework.Controls.MetroTile();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.cbPodtipObjekta = new MetroFramework.Controls.MetroComboBox();
            this.lblPodtipObjekta = new MetroFramework.Controls.MetroLabel();
            this.cbTipObjekta = new MetroFramework.Controls.MetroComboBox();
            this.lblTipObjekta = new MetroFramework.Controls.MetroLabel();
            this.tbNaziv = new MetroFramework.Controls.MetroTextBox();
            this.btnSearch = new MetroFramework.Controls.MetroButton();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panela
            // 
            this.panela.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panela.HorizontalScrollbarBarColor = true;
            this.panela.HorizontalScrollbarHighlightOnWheel = false;
            this.panela.HorizontalScrollbarSize = 10;
            this.panela.Location = new System.Drawing.Point(550, 3);
            this.panela.Name = "panela";
            this.tableLayoutPanel1.SetRowSpan(this.panela, 2);
            this.panela.Size = new System.Drawing.Size(700, 714);
            this.panela.TabIndex = 1;
            this.panela.VerticalScrollbarBarColor = true;
            this.panela.VerticalScrollbarHighlightOnWheel = false;
            this.panela.VerticalScrollbarSize = 10;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 547F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panela, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.dgv, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.metroPanel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(20, 60);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36.81882F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 63.18118F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1253, 720);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgv.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgv.Location = new System.Drawing.Point(3, 268);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(541, 449);
            this.dgv.TabIndex = 3;
            this.dgv.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.btnDodajNovi);
            this.metroPanel1.Controls.Add(this.checkBox1);
            this.metroPanel1.Controls.Add(this.btnDugme1);
            this.metroPanel1.Controls.Add(this.btnDugme2);
            this.metroPanel1.Controls.Add(this.btnDugme3);
            this.metroPanel1.Controls.Add(this.btnObrisi);
            this.metroPanel1.Controls.Add(this.btnAzuriraj);
            this.metroPanel1.Controls.Add(this.btnPrikazi);
            this.metroPanel1.Controls.Add(this.metroLabel3);
            this.metroPanel1.Controls.Add(this.cbPodtipObjekta);
            this.metroPanel1.Controls.Add(this.lblPodtipObjekta);
            this.metroPanel1.Controls.Add(this.cbTipObjekta);
            this.metroPanel1.Controls.Add(this.lblTipObjekta);
            this.metroPanel1.Controls.Add(this.tbNaziv);
            this.metroPanel1.Controls.Add(this.btnSearch);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(3, 3);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(541, 259);
            this.metroPanel1.TabIndex = 4;
            this.metroPanel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // btnDodajNovi
            // 
            this.btnDodajNovi.ActiveControl = null;
            this.btnDodajNovi.Location = new System.Drawing.Point(361, 10);
            this.btnDodajNovi.Name = "btnDodajNovi";
            this.btnDodajNovi.Size = new System.Drawing.Size(170, 49);
            this.btnDodajNovi.TabIndex = 18;
            this.btnDodajNovi.Text = "Dodaj novi objekat";
            this.btnDodajNovi.UseSelectable = true;
            this.btnDodajNovi.Click += new System.EventHandler(this.btnDodajNovi_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(302, 301);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(80, 17);
            this.checkBox1.TabIndex = 17;
            this.checkBox1.Text = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // btnDugme1
            // 
            this.btnDugme1.ActiveControl = null;
            this.btnDugme1.Location = new System.Drawing.Point(9, 134);
            this.btnDugme1.Name = "btnDugme1";
            this.btnDugme1.Size = new System.Drawing.Size(170, 49);
            this.btnDugme1.TabIndex = 16;
            this.btnDugme1.UseSelectable = true;
            this.btnDugme1.Click += new System.EventHandler(this.btnDugme1_Click);
            // 
            // btnDugme2
            // 
            this.btnDugme2.ActiveControl = null;
            this.btnDugme2.Location = new System.Drawing.Point(185, 134);
            this.btnDugme2.Name = "btnDugme2";
            this.btnDugme2.Size = new System.Drawing.Size(170, 49);
            this.btnDugme2.TabIndex = 15;
            this.btnDugme2.UseSelectable = true;
            this.btnDugme2.Click += new System.EventHandler(this.btnDugme2_Click);
            // 
            // btnDugme3
            // 
            this.btnDugme3.ActiveControl = null;
            this.btnDugme3.Location = new System.Drawing.Point(361, 134);
            this.btnDugme3.Name = "btnDugme3";
            this.btnDugme3.Size = new System.Drawing.Size(170, 49);
            this.btnDugme3.TabIndex = 14;
            this.btnDugme3.UseSelectable = true;
            this.btnDugme3.Click += new System.EventHandler(this.btnDugme3_Click);
            // 
            // btnObrisi
            // 
            this.btnObrisi.ActiveControl = null;
            this.btnObrisi.Location = new System.Drawing.Point(185, 189);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(170, 49);
            this.btnObrisi.TabIndex = 13;
            this.btnObrisi.Text = "Obriši objekat";
            this.btnObrisi.UseSelectable = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // btnAzuriraj
            // 
            this.btnAzuriraj.ActiveControl = null;
            this.btnAzuriraj.Location = new System.Drawing.Point(9, 189);
            this.btnAzuriraj.Name = "btnAzuriraj";
            this.btnAzuriraj.Size = new System.Drawing.Size(170, 49);
            this.btnAzuriraj.TabIndex = 12;
            this.btnAzuriraj.Text = "Ažuriraj objekat";
            this.btnAzuriraj.UseSelectable = true;
            this.btnAzuriraj.Click += new System.EventHandler(this.btnAzuriraj_Click);
            // 
            // btnPrikazi
            // 
            this.btnPrikazi.ActiveControl = null;
            this.btnPrikazi.Location = new System.Drawing.Point(361, 189);
            this.btnPrikazi.Name = "btnPrikazi";
            this.btnPrikazi.Size = new System.Drawing.Size(170, 49);
            this.btnPrikazi.TabIndex = 9;
            this.btnPrikazi.Text = "Prikaži na karti";
            this.btnPrikazi.UseSelectable = true;
            this.btnPrikazi.Click += new System.EventHandler(this.metroTile1_Click);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(3, 87);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(44, 19);
            this.metroLabel3.TabIndex = 8;
            this.metroLabel3.Text = "Naziv:";
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // cbPodtipObjekta
            // 
            this.cbPodtipObjekta.Enabled = false;
            this.cbPodtipObjekta.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbPodtipObjekta.FormattingEnabled = true;
            this.cbPodtipObjekta.ItemHeight = 19;
            this.cbPodtipObjekta.Items.AddRange(new object[] {
            "Svi objekti"});
            this.cbPodtipObjekta.Location = new System.Drawing.Point(87, 44);
            this.cbPodtipObjekta.Name = "cbPodtipObjekta";
            this.cbPodtipObjekta.Size = new System.Drawing.Size(233, 25);
            this.cbPodtipObjekta.TabIndex = 7;
            this.cbPodtipObjekta.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cbPodtipObjekta.UseSelectable = true;
            this.cbPodtipObjekta.TextChanged += new System.EventHandler(this.cbPodtipObjekta_TextChanged);
            // 
            // lblPodtipObjekta
            // 
            this.lblPodtipObjekta.AutoSize = true;
            this.lblPodtipObjekta.Location = new System.Drawing.Point(3, 47);
            this.lblPodtipObjekta.Name = "lblPodtipObjekta";
            this.lblPodtipObjekta.Size = new System.Drawing.Size(51, 19);
            this.lblPodtipObjekta.TabIndex = 6;
            this.lblPodtipObjekta.Text = "Podtip:";
            this.lblPodtipObjekta.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // cbTipObjekta
            // 
            this.cbTipObjekta.DisplayMember = "Svi tipovi";
            this.cbTipObjekta.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbTipObjekta.FormattingEnabled = true;
            this.cbTipObjekta.ItemHeight = 19;
            this.cbTipObjekta.Items.AddRange(new object[] {
            "Svi objekti",
            "Tačkasti objekti",
            "Linijski objekti",
            "Površinski objekti"});
            this.cbTipObjekta.Location = new System.Drawing.Point(87, 5);
            this.cbTipObjekta.Name = "cbTipObjekta";
            this.cbTipObjekta.Size = new System.Drawing.Size(233, 25);
            this.cbTipObjekta.TabIndex = 5;
            this.cbTipObjekta.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cbTipObjekta.UseSelectable = true;
            this.cbTipObjekta.TextChanged += new System.EventHandler(this.cbTip_TextChanged);
            // 
            // lblTipObjekta
            // 
            this.lblTipObjekta.AutoSize = true;
            this.lblTipObjekta.Location = new System.Drawing.Point(3, 8);
            this.lblTipObjekta.Name = "lblTipObjekta";
            this.lblTipObjekta.Size = new System.Drawing.Size(77, 19);
            this.lblTipObjekta.TabIndex = 4;
            this.lblTipObjekta.Text = "Tip objekta:";
            this.lblTipObjekta.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // tbNaziv
            // 
            // 
            // 
            // 
            this.tbNaziv.CustomButton.Image = null;
            this.tbNaziv.CustomButton.Location = new System.Drawing.Point(209, 1);
            this.tbNaziv.CustomButton.Name = "";
            this.tbNaziv.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.tbNaziv.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbNaziv.CustomButton.TabIndex = 1;
            this.tbNaziv.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbNaziv.CustomButton.UseSelectable = true;
            this.tbNaziv.CustomButton.Visible = false;
            this.tbNaziv.Lines = new string[0];
            this.tbNaziv.Location = new System.Drawing.Point(87, 84);
            this.tbNaziv.MaxLength = 50;
            this.tbNaziv.Name = "tbNaziv";
            this.tbNaziv.PasswordChar = '\0';
            this.tbNaziv.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbNaziv.SelectedText = "";
            this.tbNaziv.SelectionLength = 0;
            this.tbNaziv.SelectionStart = 0;
            this.tbNaziv.ShortcutsEnabled = true;
            this.tbNaziv.Size = new System.Drawing.Size(233, 25);
            this.tbNaziv.TabIndex = 3;
            this.tbNaziv.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbNaziv.UseSelectable = true;
            this.tbNaziv.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbNaziv.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnSearch
            // 
            this.btnSearch.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnSearch.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnSearch.Location = new System.Drawing.Point(361, 83);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(170, 26);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Pretraži";
            this.btnSearch.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnSearch.UseSelectable = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(302, 21);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(103, 23);
            this.metroButton1.TabIndex = 3;
            this.metroButton1.Text = "devtools";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1293, 800);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MainForm";
            this.Text = "Geografski objekti";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel panela;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroComboBox cbPodtipObjekta;
        private MetroFramework.Controls.MetroLabel lblPodtipObjekta;
        private MetroFramework.Controls.MetroComboBox cbTipObjekta;
        private MetroFramework.Controls.MetroLabel lblTipObjekta;
        private MetroFramework.Controls.MetroTextBox tbNaziv;
        private MetroFramework.Controls.MetroButton btnSearch;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTile btnPrikazi;
        private MetroFramework.Controls.MetroTile btnDugme1;
        private MetroFramework.Controls.MetroTile btnDugme2;
        private MetroFramework.Controls.MetroTile btnDugme3;
        private MetroFramework.Controls.MetroTile btnAzuriraj;
        private MetroFramework.Controls.MetroGrid dgv;
        private System.Windows.Forms.CheckBox checkBox1;
        private MetroFramework.Controls.MetroTile btnObrisi;
        private MetroFramework.Controls.MetroTile btnDodajNovi;
        private MetroFramework.Controls.MetroButton metroButton1;
    }
}

