﻿using MetroFramework.Forms;
using Sistemi_Baza_Podataka_Deo_Drugi;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BazeAplikacija
{
    public partial class IzborUzvisenja : MetroForm
    {
        public IList<Uzvisenje> ListaUzvisenja { get; set; }
        public IList<Uzvisenje> ListaIzabranihUzvisenja { get; set; }

        public IzborUzvisenja()
        {
            InitializeComponent();
            ListaIzabranihUzvisenja = new List<Uzvisenje>();
        }

        private void IzborUzvisenja_Load(object sender, EventArgs e)
        {
            MySession.Open();
            ListaUzvisenja = (from p in MySession.GetAllUzvisenje() orderby p.Naziv select p).ToList();
            dgv.DataSource = (from u in ListaUzvisenja select new { Id = u.Id, Naziv = u.Naziv, NadmorskaVisina = u.NadmorskaVisina }).ToList();
            MySession.Close();
            dgv.Columns[0].Visible = false;
            dgv.Columns["NadmorskaVisina"].HeaderText = "Nadmorska visina";
            dgv.ClearSelection();
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            ListaIzabranihUzvisenja.Clear();
            foreach(DataGridViewRow a in dgv.SelectedRows)
            {
                int id = Int32.Parse(a.Cells[0].Value.ToString());
                ListaIzabranihUzvisenja.Add((from u in ListaUzvisenja where u.Id==id select u).First());
            }
            Close();
        }

        private void metroButton3_Click(object sender, EventArgs e)
        {
            dgv.ClearSelection();
        }
    }
}
