﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using System.IO;
using Sistemi_Baza_Podataka_Deo_Drugi;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;
using NHibernate;
using MetroFramework;

namespace BazeAplikacija
{
    public partial class CreatePovrsinskiForm : MetroForm
    {
        IList<Uzvisenje> izabranaUzvisenja = null;
        IList<TackastiObjekat> listaIzabranihTackastih = new List<TackastiObjekat>();

        //moze i bez promenljive mode, tako sto se ispituje da li je povrsinskiZaIzmenu==null
        Mode mode;
        public PovrsinskiObjekat PovrsinskiZaIzmenu { get; set; }

        public CreatePovrsinskiForm(Mode mode)
        {
            this.mode = mode;
            inj = new Inject(this);
            InitializeComponent();
            if (mode == Mode.Update)
            {
                //inj.tipObjekta = "povrsinski";
                inj.SetZoom(-1);
                this.Text = "Izmena površinskog objekta";
            }
            InitializeBrowser();
        }

        private void CreateLinijskiForm_Load(object sender, EventArgs e)
        {
            IzveziLinijskeKoordinate();
            cbVodenaPovrsina.Checked = true;
            cbVrstaVodenePovrsine.Text = "Jezero";
            cbVrstaVodenePovrsine.SelectedIndex = 0;
            tbVisinaVrha.Text = "Visina vrha";
            tbNazivVrha.Text = "Naziv vrha";

            if (mode == Mode.Update)
            {
                //obaviti pocetna punjenja ovde
                tbNaziv.Text = PovrsinskiZaIzmenu.Naziv;

                //punjenje koordinata
                LinijskiObjekat medja = (from p in PovrsinskiZaIzmenu.Linijski where p.Naziv == (PovrsinskiZaIzmenu.Naziv + " međa") select p).First();
                int i = 1;
                foreach (Koordinata k in medja.Koordinate)
                {
                    dgvKoordinate.Rows.Add(i++, k.GeografskaSirina, k.GeografskaDuzina);
                }

                //punjenje linijskih
                IzabraniLinijskiObjekti = new List<LinijskiObjekat>();
                foreach (LinijskiObjekat l in PovrsinskiZaIzmenu.Linijski)
                {
                    if (l.Naziv == PovrsinskiZaIzmenu.Naziv + " međa") continue;
                    dgvLinijski.Rows.Add(l.Id, l.Naziv, l.TipLinijskogObjekta, l.Duzina);
                    IzabraniLinijskiObjekti.Add(l);
                }

                if (PovrsinskiZaIzmenu.GetType() == typeof(Uzvisenje) || PovrsinskiZaIzmenu.GetType().Name == "UzvisenjeProxy")
                {
                    cbUzvisenje.Checked = true;
                    cbVodenaPovrsina.Enabled = false;
                    tbNadmorskaVisina.Text = ((Uzvisenje)PovrsinskiZaIzmenu).NadmorskaVisina.ToString();
                    //punjenje geografskih
                    IzabranigeografskiObjekti = new List<GeografskiObjekat>();
                    foreach (GeografskiObjekat g in ((Uzvisenje)PovrsinskiZaIzmenu).Objekti)
                    {
                        dgvGeografski.Rows.Add(g.Id, g.Naziv);
                        IzabranigeografskiObjekti.Add(g);
                    }

                    //dodavanje vrhova
                    foreach (Vrh g in ((Uzvisenje)PovrsinskiZaIzmenu).Vrhovi)
                    {
                        dgvVrhovi.Rows.Add(dgvVrhovi.Rows.Count + 1, g.Naziv, g.NadmorskaVisina);
                    }
                }
                else if(PovrsinskiZaIzmenu.GetType() == typeof(VodenaPovrsina) || PovrsinskiZaIzmenu.GetType().Name == "VodenaPovrsinaProxy")
                {
                    //u pitanju je vodena povrsina
                    cbVodenaPovrsina.Checked = true;
                    cbUzvisenje.Enabled = false;

                    string tip = ((VodenaPovrsina)PovrsinskiZaIzmenu).TipVodenePovrsine;
                    if (tip == "more") {
                        cbVrstaVodenePovrsine.Text = "More";
                    }else if (tip == "bara")
                    {
                        cbVrstaVodenePovrsine.Text = "bara";
                    }else if (tip == "jezero")
                    {
                        cbVrstaVodenePovrsine.Text = "Jezero";
                    }
                }
                Crtaj();
                //inj.tipObjekta = "unospovrsinskog";
            }
        }


        #region KARTA
        public ChromiumWebBrowser browser;
        private static readonly string myPath = Application.StartupPath;
        private static readonly string pagesPath = Path.Combine(myPath, "pages");
        private Inject inj;

        public void InitializeBrowser()
        {
            browser = new ChromiumWebBrowser(GetPagePath("osnovni1.html"));
            //inj = new Inject(this);
            browser.RegisterJsObject("inj", inj);
            panel1.Controls.Add(browser);
            browser.Dock = DockStyle.Fill;
        }

        private string GetPagePath(string pageName)
        {
            return Path.Combine(pagesPath, pageName);
        }


        //ovde se Inject koristi samo za vracanje kliknutih koordinata
        public class Inject
        {

            private CreatePovrsinskiForm myForm;
            public string tipObjekta = "unospovrsinskog";
            //startne koordinate
            private double geografskaSirina = 43.331281;
            private double geografskaDuzina = 21.892535;
            private double zoom = 10;

            private double latIn;
            private double lngIn;

            public Inject(CreatePovrsinskiForm f)
            {
                myForm = f;
            }

            public string TipObjekta { get => tipObjekta; set => tipObjekta = value; }


            public string GetTipObjekta()
            {
                return TipObjekta;
            }

            public void SetZoom(double x)
            {
                zoom = x;
            }

            public double GetZoom()
            {
                return zoom;
            }

            public double GetGeografskaSirina()
            {
                return geografskaSirina;
            }

            public double GetGeografskaDuzina()
            {
                return geografskaDuzina;
            }

            public void SetGeografskaSirina(double x)
            {
                geografskaSirina = x;
            }

            public void SetGeografskaDuzina(double x)
            {
                geografskaDuzina = x;
            }

            public void SetLatLngIn(double x, double y)
            {
                latIn = x;
                lngIn = y;
                LatLngChanged d = myForm.LatLngInChangedHandler;
                myForm.Invoke(d, new object[] { x, y });
            }



        }

        private delegate void LatLngChanged(double newLat, double newLng);


        private void LatLngInChangedHandler(double newLat, double newLng)
        {

            dgvKoordinate.Rows.Add(dgvKoordinate.Rows.Count + 1, newLat, newLng);

        }


        private void metroButton1_Click(object sender, EventArgs e)
        {
            browser.ShowDevTools();
        }

        #endregion


        private void metroButton3_Click(object sender, EventArgs e)
        {
            var iu = new IzborUzvisenja();
            if (iu.ShowDialog() == DialogResult.OK)
            {
                izabranaUzvisenja = iu.ListaIzabranihUzvisenja;
            }
        }

        private bool Validacija()
        {
            if (tbNaziv.Text == "" || tbNaziv.Text.EndsWith(" međa"))
            {
                return false;
            }

            if (!cbUzvisenje.Checked && !cbVodenaPovrsina.Checked)
            {
                return false;
            }

            if (cbUzvisenje.Checked)
            {
                double resul;
                if (!double.TryParse(tbNadmorskaVisina.Text, out resul))
                {
                    return false;
                }
            }

            return true;
        }

        private void metroButton4_Click(object sender, EventArgs e)
        {
            if (!Validacija())
            {
                return;
            }

            if (mode == Mode.Create)
            {

                MySession.Open();
                LinijskiObjekat medja = new LinijskiObjekat();
                medja.TipLinijskogObjekta = "granicna linija";
                medja.Naziv = tbNaziv.Text + " međa";
                MySession.Save(medja);

                int i = 1;
                foreach (DataGridViewRow dgvr in dgvKoordinate.Rows)
                {
                    Koordinata k = new Koordinata();
                    k.MyLinijskiObjekat = medja;
                    k.GeografskaSirina = Double.Parse(dgvr.Cells[1].Value.ToString());
                    k.GeografskaDuzina = Double.Parse(dgvr.Cells[2].Value.ToString());
                    k.Indeks = i++;
                    medja.Koordinate.Add(k);
                    MySession.Save(k);
                }

                MySession.Save(medja);
                MySession.Flush();

                if (cbVodenaPovrsina.Checked)
                {
                    VodenaPovrsina v = new VodenaPovrsina();
                    v.TipPovrsinskogObjekta = "vodena povrsina";
                    v.Linijski = new List<LinijskiObjekat>();
                    v.Linijski.Add(medja);
                    v.Naziv = tbNaziv.Text;

                    foreach (LinijskiObjekat l in IzabraniLinijskiObjekti)
                    {
                        LinijskiObjekat managedLinijski = (LinijskiObjekat)MySession.Merge(l);

                        v.Linijski.Add(managedLinijski);
                        //s.Update(managedLinijski);
                    }

                    //v.TipPovrsinkogObjekta = "vodena povrsina";
                    v.TipVodenePovrsine = cbVrstaVodenePovrsine.Text.ToLower();

                    MySession.Save(v);
                    MySession.Close();
                }
                else
                {
                    Uzvisenje u = new Uzvisenje();
                    u.Naziv = tbNaziv.Text;
                    u.TipPovrsinskogObjekta = "uzvisenje";
                    u.Linijski = new List<LinijskiObjekat>();
                    u.Linijski.Add(medja);

                    foreach (LinijskiObjekat l in IzabraniLinijskiObjekti)
                    {
                        LinijskiObjekat managedLinijski = (LinijskiObjekat)MySession.Merge(l);

                        u.Linijski.Add(managedLinijski);
                        //s.Update(managedLinijski);
                    }

                    u.NadmorskaVisina = Int32.Parse(tbNadmorskaVisina.Text);

                    u.Objekti = new List<GeografskiObjekat>();
                    foreach (GeografskiObjekat l in IzabranigeografskiObjekti)
                    {
                        GeografskiObjekat managedGeografski = (GeografskiObjekat)MySession.Merge(l);
                        u.Objekti.Add(managedGeografski);
                        //s.Update(managedGeografski);
                    }

                    u.Vrhovi = new List<Vrh>();
                    foreach (DataGridViewRow a in dgvVrhovi.Rows)
                    {
                        if (a.Cells[0].Value == null)
                        {
                            continue;
                        }
                        u.Vrhovi.Add(new Vrh()
                        {
                            Naziv = (string)a.Cells[1].Value,
                            NadmorskaVisina = Double.Parse(a.Cells[2].Value.ToString()),
                            MyUzvisenje = u
                        });
                    }

                    MySession.Save(u);
                    MySession.Close();
                }

                MetroMessageBox.Show(this, "Površinski objekat je uspešno zapamćen u bazu.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
                Close();
            }
            else if (mode == Mode.Update)
            {
                ISession s = DataLayer.GetSession();

                if (PovrsinskiZaIzmenu.GetType() == typeof(Uzvisenje) || PovrsinskiZaIzmenu.GetType().Name == "UzvisenjeProxy")
                {
                    /*
                    Uzvisenje u = new Uzvisenje();
                    u.Id = PovrsinskiZaIzmenu.Id;
                    Uzvisenje managed = s.Merge<Uzvisenje>(u);
                    */

                    Uzvisenje managed = s.Merge<Uzvisenje>((Uzvisenje)PovrsinskiZaIzmenu);

                    managed.Naziv = tbNaziv.Text;

                    //posto je medja azurirana, u staroj medji brisemo sve koordinate a dodajemo nove
                    LinijskiObjekat medja = null;
                    try
                    {
                        medja = (from p in managed.Linijski where p.Naziv.Contains(" međa") select p).First();

                        foreach (var k in medja.Koordinate)
                        {
                            s.Delete(k);
                        }
                        s.Flush();
                    }
                    catch (InvalidOperationException e1)
                    {
                        //ako nema medju
                        medja = new LinijskiObjekat();
                        managed.Linijski.Add(medja);
                    }

                    int i = 1;
                    foreach (DataGridViewRow dgvr in dgvKoordinate.Rows)
                    {
                        Koordinata k = new Koordinata();
                        k.MyLinijskiObjekat = medja;
                        k.GeografskaSirina = Double.Parse(dgvr.Cells[1].Value.ToString());
                        k.GeografskaDuzina = Double.Parse(dgvr.Cells[2].Value.ToString());
                        k.Indeks = i++;
                        //jer nema cascade na koordinate
                        s.Save(k);
                        medja.Koordinate.Add(k);
                    }
                    medja.TipLinijskogObjekta = "granicna linija";
                    medja.Naziv = tbNaziv.Text + " međa";

                    //sada treba obrisati postojece linijske i geografske
                    var zaBrisanjeLinijski = (from p in managed.Linijski select p).ToList();

                    foreach (var lin in zaBrisanjeLinijski)
                    {
                        if (!lin.Naziv.Contains(" međa"))
                        {
                            LinijskiObjekat managedLinijski = s.Merge(lin);
                            //brisanje nece da radi drugacije
                            managed.Linijski.Remove(managedLinijski);
                        }
                    }
                    var zaBrisanjeGeografski = (from p in managed.Objekti select p).ToList();

                    foreach (var lin in zaBrisanjeGeografski)
                    {
                        GeografskiObjekat managedGeografski = s.Merge(lin);
                        //brisanje ne moze drugacije (pravi exception ili se ne obrise)
                        managed.Objekti.Remove(managedGeografski);
                    }

                    s.SaveOrUpdate(managed);
                    s.Flush();

                    //sada dodajemo nove linijske
                    foreach (LinijskiObjekat l in IzabraniLinijskiObjekti)
                    {
                        LinijskiObjekat managedLinijski = s.Merge(l);
                        managed.Linijski.Add(managedLinijski);
                    }

                    //za svaki slucaj
                    managed.TipPovrsinskogObjekta = "uzvisenje";
                    managed.NadmorskaVisina = Int32.Parse(tbNadmorskaVisina.Text);

                    foreach (GeografskiObjekat l in IzabranigeografskiObjekti)
                    {
                        GeografskiObjekat managedGeografski = s.Merge(l);
                        managed.Objekti.Add(managedGeografski);
                    }

                    s.Update(managed);

                    foreach (Vrh v in managed.Vrhovi)
                    {
                        s.Delete(v);
                    }
                    managed.Vrhovi = new List<Vrh>();
                    s.Update(managed);

                    foreach (DataGridViewRow a in dgvVrhovi.Rows)
                    {
                        if (a.Cells[0].Value == null)
                        {
                            continue;
                        }
                        managed.Vrhovi.Add(new Vrh()
                        {
                            Naziv = (string)a.Cells[1].Value,
                            NadmorskaVisina = Double.Parse(a.Cells[2].Value.ToString()),
                            MyUzvisenje = managed
                        });
                    }

                    s.Update(managed);

                }
                else if (PovrsinskiZaIzmenu.GetType() == typeof(VodenaPovrsina) || PovrsinskiZaIzmenu.GetType().Name == "VodenaPovrsinaProxy")
                {
                    VodenaPovrsina u = new VodenaPovrsina();
                    u.Id = PovrsinskiZaIzmenu.Id;

                    VodenaPovrsina managed = s.Merge<VodenaPovrsina>((VodenaPovrsina)PovrsinskiZaIzmenu);
                    managed.Naziv = tbNaziv.Text;

                    //posto je medja azurirana, u staroj medji brisemo sve koordinate a dodajemo nove
                    LinijskiObjekat medja = null;
                    try
                    {
                        medja = (from p in managed.Linijski where p.Naziv.Contains(" međa") select p).First();

                        foreach (var k in medja.Koordinate)
                        {
                            s.Delete(k);
                        }
                        s.Flush();
                    }
                    catch (InvalidOperationException e1)
                    {
                        //ako nema medju
                        medja = new LinijskiObjekat();
                        managed.Linijski.Add(medja);
                    }

                    int i = 1;
                    foreach (DataGridViewRow dgvr in dgvKoordinate.Rows)
                    {
                        Koordinata k = new Koordinata();
                        k.MyLinijskiObjekat = medja;
                        k.GeografskaSirina = Double.Parse(dgvr.Cells[1].Value.ToString());
                        k.GeografskaDuzina = Double.Parse(dgvr.Cells[2].Value.ToString());
                        k.Indeks = i++;
                        //jer nema cascade na koordinate
                        s.Save(k);
                        medja.Koordinate.Add(k);
                    }
                    medja.TipLinijskogObjekta = "granicna linija";
                    medja.Naziv = tbNaziv.Text + " međa";

                    //sada treba obrisati postojece linijske i geografske
                    var zaBrisanjeLinijski = (from p in managed.Linijski select p).ToList();

                    foreach (var lin in zaBrisanjeLinijski)
                    {
                        if (!lin.Naziv.Contains(" međa"))
                        {
                            LinijskiObjekat managedLinijski = s.Merge(lin);
                            //brisanje nece da radi drugacije
                            managed.Linijski.Remove(managedLinijski);
                        }
                    }

                    s.SaveOrUpdate(managed);
                    s.Flush();

                    //sada dodajemo nove linijske
                    foreach (LinijskiObjekat l in IzabraniLinijskiObjekti)
                    {
                        LinijskiObjekat managedLinijski = s.Merge(l);
                        managed.Linijski.Add(managedLinijski);
                    }

                    managed.TipVodenePovrsine = cbVrstaVodenePovrsine.Text.ToLower();
                    managed.TipPovrsinskogObjekta = "vodena povrsina";
                    s.Update(managed);

                }
                s.Flush();
                s.Close();

                MetroMessageBox.Show(this, "Površinski objekat je uspešno izmenjen.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
                Close();
            }
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            if (dgvKoordinate.Rows.Count != 0) dgvKoordinate.Rows.RemoveAt(dgvKoordinate.Rows.Count - 1);
        }

        private void dgvKoordinate_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (dgvKoordinate.Rows.Count > 1 && inj.GetZoom() != -1) Crtaj();
        }

        private void dgvKoordinate_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (inj.GetZoom() != -1) Crtaj();
        }

        private void IzveziLinijskeKoordinate()
        {
            /*
           var flightPlanCoordinates = [
          { lat: 37.772, lng: -122.214 },
          { lat: 21.291, lng: -157.821 },
          { lat: -18.142, lng: 178.431 },
          { lat: -27.467, lng: 153.027 }
        ];*/

            StringBuilder sb = new StringBuilder();
            sb.Append("var flightPlanCoordinates = [");

            foreach (DataGridViewRow r in dgvKoordinate.Rows)
            {
                sb.Append("{lat: " + r.Cells[1].Value.ToString() + "," + " lng: " + r.Cells[2].Value.ToString() + " },");
            }
            String s = sb.ToString();
            char[] charsToTrim = { ',' };
            s = s.TrimEnd(charsToTrim);
            s += "];";
            File.WriteAllText(GetPagePath("linijskikoordinate.js"), s);

        }

        private void Crtaj()
        {
            IzveziLinijskeKoordinate();
            browser.Load(GetPagePath("osnovni1.html"));
        }



        private void cbReka_CheckedChanged(object sender, EventArgs e)
        {
            if (cbVodenaPovrsina.Checked == true)
            {
                cbVrstaVodenePovrsine.Enabled = true;
                cbUzvisenje.Checked = false;
                dgvGeografski.Enabled = false;
                dgvVrhovi.Enabled = false;
                btnIzaberiGeografski.Enabled = false;
                btnUnesiVrh.Enabled = false;
                tbVisinaVrha.Enabled = false;
                tbNazivVrha.Enabled = false;
                dgvVrhovi.Enabled = false;
                tbNadmorskaVisina.Enabled = false;
            }
        }

        private void cbPut_CheckedChanged(object sender, EventArgs e)
        {
            if (cbUzvisenje.Checked == true)
            {
                tbNadmorskaVisina.Enabled = true;
                cbVodenaPovrsina.Checked = false;
                dgvGeografski.Enabled = true;
                dgvVrhovi.Enabled = true;
                btnIzaberiGeografski.Enabled = true;
                btnUnesiVrh.Enabled = true;
                tbVisinaVrha.Enabled = true;
                tbNazivVrha.Enabled = true;
                dgvVrhovi.Enabled = true;
                cbVrstaVodenePovrsine.Enabled = false;
            }
        }

        public void RemoveText(object sender, EventArgs e)
        {
            if (tbNazivVrha.Text == "Naziv vrha") tbNazivVrha.Text = "";
        }

        public void AddText(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbNazivVrha.Text))
                tbNazivVrha.Text = "Naziv vrha";
        }

        public void RemoveText1(object sender, EventArgs e)
        {
            if (tbVisinaVrha.Text == "Visina vrha") tbVisinaVrha.Text = "";
        }

        public void AddText1(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbVisinaVrha.Text))
                tbVisinaVrha.Text = "Visina vrha";
        }


        private List<LinijskiObjekat> IzabraniLinijskiObjekti = new List<LinijskiObjekat>();
        private List<LinijskiObjekat> LinijskiObjektiZaPrikaz = null;
        private void metroButton6_Click(object sender, EventArgs e)
        {
            //kad se radi update, samo se dgv napuni postojecim i lista izabranih se napuni postojecim
            IzborLinijskih ilf = new IzborLinijskih();
            if (LinijskiObjektiZaPrikaz == null)
            {
                MySession.Open();
                LinijskiObjektiZaPrikaz = (List<LinijskiObjekat>)MySession.GetAllLinijskiObjekat();
                MySession.Close();
                LinijskiObjektiZaPrikaz = (from p in LinijskiObjektiZaPrikaz where !p.Naziv.Contains(" međa") orderby p.Naziv select p).ToList();
            }

            //da se ne bi ucitavalo iz baze kad god se pokrene forma
            ilf.LinijskiObjektiZaPrikaz = LinijskiObjektiZaPrikaz;

            if (ilf.ShowDialog() == DialogResult.OK)
            {


                //da li je vec dodat?
                foreach (LinijskiObjekat t in IzabraniLinijskiObjekti)
                {
                    if (ilf.IzabraniLinijskiObjekat.Id == t.Id)
                    {
                        MetroMessageBox.Show(this, "Ovaj linijski objekat je već dodat.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
                        return;
                    }
                }

                IzabraniLinijskiObjekti.Add(ilf.IzabraniLinijskiObjekat);
                //add row to dgv
                dgvLinijski.Rows.Add(ilf.IzabraniLinijskiObjekat.Id, ilf.IzabraniLinijskiObjekat.Naziv, ilf.IzabraniLinijskiObjekat.TipLinijskogObjekta, ilf.IzabraniLinijskiObjekat.Duzina);
            }
        }




        private void metroLabel4_Click(object sender, EventArgs e)
        {

        }

        private void metroLabel7_Click(object sender, EventArgs e)
        {

        }

        private void dgvTackasti_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnUnesiVrh_Click(object sender, EventArgs e)
        {
            if (tbNazivVrha.Text != "" && tbVisinaVrha.Text != "" && tbVisinaVrha.Text != "Visina vrha" && tbVisinaVrha.Text != "Naziv vrha")
            {
                double resul;
                if (!double.TryParse(tbVisinaVrha.Text, out resul))
                {
                    return;
                }

                dgvVrhovi.Rows.Add(dgvVrhovi.Rows.Count + 1, tbNazivVrha.Text, tbVisinaVrha.Text);

                tbNazivVrha.Text = "Naziv vrha";
                tbVisinaVrha.Text = "Visina vrha";
            }
        }

        private void dgvLinijski_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            int deletedId = Int32.Parse(e.Row.Cells[0].Value.ToString());
            LinijskiObjekat deleted = (from p in IzabraniLinijskiObjekti where p.Id == deletedId select p).First();
            IzabraniLinijskiObjekti.Remove(deleted);

        }

        private List<GeografskiObjekat> IzabranigeografskiObjekti = new List<GeografskiObjekat>();
        private List<GeografskiObjekat> GeografskiObjektiZaPrikaz = null;
        private void btnIzaberiGeografski_Click(object sender, EventArgs e)
        {
            //kad se radi update, samo se dgv napuni postojecim i lista izabranih se napuni postojecim
            IzborGeografskog ilf = new IzborGeografskog();
            if (GeografskiObjektiZaPrikaz == null)
            {
                MySession.Open();
                GeografskiObjektiZaPrikaz = (List<GeografskiObjekat>)MySession.GetAllGeografskiObjekat().Where(x => !x.Naziv.EndsWith(" međa")).OrderBy(x => x.Naziv).ToList();
                MySession.Close();
            }

            //da se ne bi ucitavalo iz baze kad god se pokrene forma
            ilf.GeografskiObjektiZaPrikaz = GeografskiObjektiZaPrikaz;

            if (ilf.ShowDialog() == DialogResult.OK)
            {


                //da li je vec dodat?
                foreach (GeografskiObjekat t in IzabranigeografskiObjekti)
                {
                    if (ilf.IzabraniGeografskiObjekat.Id == t.Id)
                    {
                        MetroMessageBox.Show(this, "Ovaj geografski objekat je već dodat.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
                        return;
                    }
                }

                IzabranigeografskiObjekti.Add(ilf.IzabraniGeografskiObjekat);
                //add row to dgv
                dgvGeografski.Rows.Add(ilf.IzabraniGeografskiObjekat.Id, ilf.IzabraniGeografskiObjekat.Naziv);
            }
        }

        private void dgvGeografskiObjekti_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            int deletedId = Int32.Parse(e.Row.Cells[0].Value.ToString());
            GeografskiObjekat deleted = (from p in IzabranigeografskiObjekti where p.Id == deletedId select p).First();
            IzabranigeografskiObjekti.Remove(deleted);
        }
    }
}
