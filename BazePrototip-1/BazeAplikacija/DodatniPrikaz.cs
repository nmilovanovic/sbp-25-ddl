﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;

namespace BazeAplikacija
{
    public partial class DodatniPrikaz : MetroForm
    {
        public DodatniPrikaz(String ime, ViewConfigurationSelect selektor, object prikaz)
        {
            InitializeComponent();
            this.Text = ime;
            dgvPrikaz.DataSource = prikaz;
            selektor.Invoke(dgvPrikaz);
        }
    }
}
