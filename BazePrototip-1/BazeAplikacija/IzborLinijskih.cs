﻿using MetroFramework;
using MetroFramework.Forms;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BazeAplikacija
{
    public partial class IzborLinijskih : MetroForm
    {
        public List<LinijskiObjekat> LinijskiObjektiZaPrikaz { get; set; }
        public LinijskiObjekat IzabraniLinijskiObjekat { get; set; }

        public IzborLinijskih()
        {
            InitializeComponent();
            
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count != 1) {
                MetroMessageBox.Show(this, "Treba izabrati jedan linijski objekat.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information, 200);
                return;
            }
            IzabraniLinijskiObjekat = (from l in LinijskiObjektiZaPrikaz where l.Id == Int32.Parse(dgv.SelectedRows[0].Cells[0].Value.ToString()) select l).First();

            DialogResult = DialogResult.OK;
            Close();
        }

        private void IzborLinijskih_Load(object sender, EventArgs e)
        {
            

            foreach (LinijskiObjekat l in LinijskiObjektiZaPrikaz)
            {
                dgv.Rows.Add(l.Id, l.Naziv, (l.TipLinijskogObjekta == "reka") ? "Reka" :
                                         (l.TipLinijskogObjekta == "put" ? "Put" : "Granična linija"),  l.Duzina);
            }
        }
    }
}
