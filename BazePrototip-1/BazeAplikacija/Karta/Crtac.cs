﻿using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BazeAplikacija.Karta
{
    public class Crtac 
    {
        private static Crtac instance = new Crtac();
        private StringBuilder sb = new StringBuilder();
        
        public string TackastiHtml(Koordinata k)
        {
            sb.Clear();
            sb.AppendLine("<!DOCTYPE html>");
            sb.AppendLine("<html>");
            sb.AppendLine("<head>");
            sb.AppendLine("<title>Simple Map</title>");
            sb.AppendLine("<meta name='viewport' content='initial - scale = 1.0'>");
            sb.AppendLine("<meta charset='utf-8'>");
            sb.AppendLine("<style>");
            sb.AppendLine("#map {");
            sb.AppendLine("height: 100%;");
            sb.AppendLine("backgorund-color: blue;");
            sb.AppendLine("}");
            sb.AppendLine("html,body {");
            sb.AppendLine("height: 100%;");
            sb.AppendLine("margin: 0;");
            sb.AppendLine("padding: 0;");
            sb.AppendLine("}");
            sb.AppendLine("</style>");
            sb.AppendLine("</head>");
            sb.AppendLine("<body>");
            sb.AppendLine("<div id='map'></div>");
            sb.AppendLine("<script>");
            sb.AppendLine("var latitude = 43.313;");
            sb.AppendLine("var longitude = 21.873;");
            sb.AppendLine("function initMap() {");
            sb.AppendLine("var myLatLng = { lat: latitude, lng: longitude };");
            sb.AppendLine("var map = new google.maps.Map(document.getElementById('map'), {");
            sb.AppendLine("zoom: 11,");
            sb.AppendLine("center: myLatLng");
            sb.AppendLine("});");
            sb.AppendLine("var marker = new google.maps.Marker({");
            sb.AppendLine("position: myLatLng,");
            sb.AppendLine("map: map,");
            sb.AppendLine("title: 'Hello World!'");
            sb.AppendLine("});");
            sb.AppendLine("}");
            sb.AppendLine("</script>");
            sb.AppendLine("<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyB-EhvD5zJRJ6l9lNt6utkbwFrrY0-GBu4&callback=initMap' async defer ></ script > ");
            sb.AppendLine("</body>");

            return sb.ToString();
        }

        public string LinijskiHtml(double sirina, double duzina)
        {
            sb.Clear();
            sb.AppendLine("<!DOCTYPE html>");
            sb.AppendLine("<html>");
            sb.AppendLine("<head>");
            sb.AppendLine("<style>");
            sb.AppendLine("#map {");
            sb.AppendLine("height: 100%;");
            sb.AppendLine("background-color: blue;");
            sb.AppendLine("}");
            sb.AppendLine("html,body {");
            sb.AppendLine("height: 100%;");
            sb.AppendLine("margin: 0;");
            sb.AppendLine("padding: 0;");
            sb.AppendLine("}");
            sb.AppendLine("</style>");
            sb.AppendLine("</head>");
            sb.AppendLine("<body>");
            sb.AppendLine("<div id='map'></div>");
            sb.AppendLine("<script>");
            sb.AppendLine("function showAlert(){");
            sb.AppendLine("alert('I am an alert box!');");
            sb.AppendLine("}");
            sb.AppendLine("</script>");
            sb.AppendLine("</body>");
            
            
                
            
            return sb.ToString();
        }

        public string PovrsinskiHtml(double sirina, double duzina)
        {
            return null;
        }

        public string TackastiScript()
        {
            sb.Clear();
            sb.AppendLine("var latitude = 43.313;");
            sb.AppendLine("var longitude = 21.873;");
            sb.AppendLine("function initMap() {");
            sb.AppendLine("var myLatLng = { lat: latitude, lng: longitude };");
            sb.AppendLine("var map = new google.maps.Map(document.getElementById('map'), {");
            sb.AppendLine("zoom: 11,");
            sb.AppendLine("center: myLatLng");
            sb.AppendLine("});");
            sb.AppendLine("var marker = new google.maps.Marker({");
            sb.AppendLine("position: myLatLng,");
            sb.AppendLine("map: map,");
            sb.AppendLine("title: 'Hello World!'");
            sb.AppendLine("});");
            sb.AppendLine("}");

            return sb.ToString();
        }


        private Crtac()
        {

        }

        public static Crtac Instance { get => instance; private set => instance = value; }
    }
}
