﻿namespace BazeAplikacija
{
    partial class CreatePovrsinskiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.dgvLinijski = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroButton6 = new MetroFramework.Controls.MetroButton();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.dgvKoordinate = new MetroFramework.Controls.MetroGrid();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.tbNaziv = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroButton4 = new MetroFramework.Controls.MetroButton();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.tbNadmorskaVisina = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.dgvGeografski = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnIzaberiGeografski = new MetroFramework.Controls.MetroButton();
            this.btnUnesiVrh = new MetroFramework.Controls.MetroButton();
            this.tbVisinaVrha = new MetroFramework.Controls.MetroTextBox();
            this.tbNazivVrha = new MetroFramework.Controls.MetroTextBox();
            this.dgvVrhovi = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.cbVrstaVodenePovrsine = new MetroFramework.Controls.MetroComboBox();
            this.cbVodenaPovrsina = new MetroFramework.Controls.MetroCheckBox();
            this.cbUzvisenje = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel1.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLinijski)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKoordinate)).BeginInit();
            this.metroPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGeografski)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVrhovi)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.metroPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.metroPanel2, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(20, 60);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1238, 862);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.HorizontalScrollbarBarColor = true;
            this.panel1.HorizontalScrollbarHighlightOnWheel = false;
            this.panel1.HorizontalScrollbarSize = 10;
            this.panel1.Location = new System.Drawing.Point(703, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(532, 856);
            this.panel1.TabIndex = 0;
            this.panel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.panel1.VerticalScrollbarBarColor = true;
            this.panel1.VerticalScrollbarHighlightOnWheel = false;
            this.panel1.VerticalScrollbarSize = 10;
            // 
            // metroPanel1
            // 
            this.metroPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel1.Controls.Add(this.dgvLinijski);
            this.metroPanel1.Controls.Add(this.metroLabel7);
            this.metroPanel1.Controls.Add(this.metroButton6);
            this.metroPanel1.Controls.Add(this.metroButton2);
            this.metroPanel1.Controls.Add(this.dgvKoordinate);
            this.metroPanel1.Controls.Add(this.metroLabel4);
            this.metroPanel1.Controls.Add(this.tbNaziv);
            this.metroPanel1.Controls.Add(this.metroLabel12);
            this.metroPanel1.Controls.Add(this.metroButton4);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(3, 3);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(344, 856);
            this.metroPanel1.TabIndex = 1;
            this.metroPanel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // dgvLinijski
            // 
            this.dgvLinijski.AllowUserToAddRows = false;
            this.dgvLinijski.AllowUserToResizeRows = false;
            this.dgvLinijski.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvLinijski.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvLinijski.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvLinijski.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLinijski.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvLinijski.ColumnHeadersHeight = 28;
            this.dgvLinijski.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvLinijski.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn9});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLinijski.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvLinijski.EnableHeadersVisualStyles = false;
            this.dgvLinijski.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvLinijski.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvLinijski.Location = new System.Drawing.Point(18, 514);
            this.dgvLinijski.Name = "dgvLinijski";
            this.dgvLinijski.ReadOnly = true;
            this.dgvLinijski.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLinijski.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvLinijski.RowHeadersVisible = false;
            this.dgvLinijski.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvLinijski.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLinijski.Size = new System.Drawing.Size(311, 222);
            this.dgvLinijski.TabIndex = 48;
            this.dgvLinijski.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.dgvLinijski.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvLinijski_UserDeletedRow);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Naziv";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 130;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Tip";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 80;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "Dužina";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 75;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel7.Location = new System.Drawing.Point(18, 492);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(132, 19);
            this.metroLabel7.TabIndex = 47;
            this.metroLabel7.Text = "Ostali linijski objekti:";
            this.metroLabel7.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel7.Click += new System.EventHandler(this.metroLabel7_Click);
            // 
            // metroButton6
            // 
            this.metroButton6.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.metroButton6.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            this.metroButton6.Location = new System.Drawing.Point(174, 763);
            this.metroButton6.Name = "metroButton6";
            this.metroButton6.Size = new System.Drawing.Size(141, 23);
            this.metroButton6.TabIndex = 46;
            this.metroButton6.Text = "Dodaj linijski objekat";
            this.metroButton6.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroButton6.UseSelectable = true;
            this.metroButton6.Click += new System.EventHandler(this.metroButton6_Click);
            // 
            // metroButton2
            // 
            this.metroButton2.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.metroButton2.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            this.metroButton2.Location = new System.Drawing.Point(138, 395);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(177, 23);
            this.metroButton2.TabIndex = 37;
            this.metroButton2.Text = "Obriši poslednju koordinatu";
            this.metroButton2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.metroButton2_Click);
            // 
            // dgvKoordinate
            // 
            this.dgvKoordinate.AllowUserToAddRows = false;
            this.dgvKoordinate.AllowUserToDeleteRows = false;
            this.dgvKoordinate.AllowUserToResizeRows = false;
            this.dgvKoordinate.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvKoordinate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvKoordinate.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvKoordinate.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvKoordinate.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvKoordinate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKoordinate.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column1,
            this.Column2});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvKoordinate.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvKoordinate.EnableHeadersVisualStyles = false;
            this.dgvKoordinate.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvKoordinate.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvKoordinate.Location = new System.Drawing.Point(18, 166);
            this.dgvKoordinate.Name = "dgvKoordinate";
            this.dgvKoordinate.ReadOnly = true;
            this.dgvKoordinate.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvKoordinate.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvKoordinate.RowHeadersVisible = false;
            this.dgvKoordinate.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvKoordinate.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvKoordinate.Size = new System.Drawing.Size(311, 223);
            this.dgvKoordinate.TabIndex = 36;
            this.dgvKoordinate.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.dgvKoordinate.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvKoordinate_RowsAdded);
            this.dgvKoordinate.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvKoordinate_RowsRemoved);
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Redni broj";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 70;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Geografska širina";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 110;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Geografska dužina";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 110;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel4.Location = new System.Drawing.Point(15, 135);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(198, 19);
            this.metroLabel4.TabIndex = 35;
            this.metroLabel4.Text = "Koordinate koje definišu među:";
            this.metroLabel4.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel4.Click += new System.EventHandler(this.metroLabel4_Click);
            // 
            // tbNaziv
            // 
            // 
            // 
            // 
            this.tbNaziv.CustomButton.Image = null;
            this.tbNaziv.CustomButton.Location = new System.Drawing.Point(105, 1);
            this.tbNaziv.CustomButton.Name = "";
            this.tbNaziv.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbNaziv.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbNaziv.CustomButton.TabIndex = 1;
            this.tbNaziv.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbNaziv.CustomButton.UseSelectable = true;
            this.tbNaziv.CustomButton.Visible = false;
            this.tbNaziv.Lines = new string[0];
            this.tbNaziv.Location = new System.Drawing.Point(165, 32);
            this.tbNaziv.MaxLength = 32767;
            this.tbNaziv.Name = "tbNaziv";
            this.tbNaziv.PasswordChar = '\0';
            this.tbNaziv.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbNaziv.SelectedText = "";
            this.tbNaziv.SelectionLength = 0;
            this.tbNaziv.SelectionStart = 0;
            this.tbNaziv.ShortcutsEnabled = true;
            this.tbNaziv.Size = new System.Drawing.Size(127, 23);
            this.tbNaziv.TabIndex = 31;
            this.tbNaziv.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbNaziv.UseSelectable = true;
            this.tbNaziv.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbNaziv.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel12.Location = new System.Drawing.Point(15, 36);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(45, 19);
            this.metroLabel12.TabIndex = 30;
            this.metroLabel12.Text = "Naziv:";
            this.metroLabel12.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroButton4
            // 
            this.metroButton4.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.metroButton4.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            this.metroButton4.Location = new System.Drawing.Point(115, 92);
            this.metroButton4.Name = "metroButton4";
            this.metroButton4.Size = new System.Drawing.Size(177, 23);
            this.metroButton4.TabIndex = 29;
            this.metroButton4.Text = "Zapamti površinski objekat";
            this.metroButton4.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroButton4.UseSelectable = true;
            this.metroButton4.Click += new System.EventHandler(this.metroButton4_Click);
            // 
            // metroPanel2
            // 
            this.metroPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel2.Controls.Add(this.metroLabel9);
            this.metroPanel2.Controls.Add(this.tbNadmorskaVisina);
            this.metroPanel2.Controls.Add(this.metroLabel8);
            this.metroPanel2.Controls.Add(this.dgvGeografski);
            this.metroPanel2.Controls.Add(this.btnIzaberiGeografski);
            this.metroPanel2.Controls.Add(this.btnUnesiVrh);
            this.metroPanel2.Controls.Add(this.tbVisinaVrha);
            this.metroPanel2.Controls.Add(this.tbNazivVrha);
            this.metroPanel2.Controls.Add(this.dgvVrhovi);
            this.metroPanel2.Controls.Add(this.metroLabel6);
            this.metroPanel2.Controls.Add(this.metroLabel5);
            this.metroPanel2.Controls.Add(this.metroLabel3);
            this.metroPanel2.Controls.Add(this.cbVrstaVodenePovrsine);
            this.metroPanel2.Controls.Add(this.cbVodenaPovrsina);
            this.metroPanel2.Controls.Add(this.cbUzvisenje);
            this.metroPanel2.Controls.Add(this.metroLabel2);
            this.metroPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(353, 3);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(344, 856);
            this.metroPanel2.TabIndex = 2;
            this.metroPanel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel9.Location = new System.Drawing.Point(315, 183);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(21, 19);
            this.metroLabel9.TabIndex = 61;
            this.metroLabel9.Text = "m";
            this.metroLabel9.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // tbNadmorskaVisina
            // 
            // 
            // 
            // 
            this.tbNadmorskaVisina.CustomButton.Image = null;
            this.tbNadmorskaVisina.CustomButton.Location = new System.Drawing.Point(99, 1);
            this.tbNadmorskaVisina.CustomButton.Name = "";
            this.tbNadmorskaVisina.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbNadmorskaVisina.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbNadmorskaVisina.CustomButton.TabIndex = 1;
            this.tbNadmorskaVisina.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbNadmorskaVisina.CustomButton.UseSelectable = true;
            this.tbNadmorskaVisina.CustomButton.Visible = false;
            this.tbNadmorskaVisina.Lines = new string[0];
            this.tbNadmorskaVisina.Location = new System.Drawing.Point(188, 179);
            this.tbNadmorskaVisina.MaxLength = 32767;
            this.tbNadmorskaVisina.Name = "tbNadmorskaVisina";
            this.tbNadmorskaVisina.PasswordChar = '\0';
            this.tbNadmorskaVisina.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbNadmorskaVisina.SelectedText = "";
            this.tbNadmorskaVisina.SelectionLength = 0;
            this.tbNadmorskaVisina.SelectionStart = 0;
            this.tbNadmorskaVisina.ShortcutsEnabled = true;
            this.tbNadmorskaVisina.Size = new System.Drawing.Size(121, 23);
            this.tbNadmorskaVisina.TabIndex = 60;
            this.tbNadmorskaVisina.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbNadmorskaVisina.UseSelectable = true;
            this.tbNadmorskaVisina.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbNadmorskaVisina.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel8.Location = new System.Drawing.Point(20, 183);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(120, 19);
            this.metroLabel8.TabIndex = 59;
            this.metroLabel8.Text = "Nadmorska visina:";
            this.metroLabel8.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // dgvGeografski
            // 
            this.dgvGeografski.AllowUserToAddRows = false;
            this.dgvGeografski.AllowUserToResizeRows = false;
            this.dgvGeografski.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvGeografski.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvGeografski.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvGeografski.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvGeografski.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvGeografski.ColumnHeadersHeight = 28;
            this.dgvGeografski.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGeografski.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvGeografski.EnableHeadersVisualStyles = false;
            this.dgvGeografski.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvGeografski.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvGeografski.Location = new System.Drawing.Point(20, 250);
            this.dgvGeografski.Name = "dgvGeografski";
            this.dgvGeografski.ReadOnly = true;
            this.dgvGeografski.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvGeografski.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvGeografski.RowHeadersVisible = false;
            this.dgvGeografski.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvGeografski.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGeografski.Size = new System.Drawing.Size(289, 232);
            this.dgvGeografski.TabIndex = 58;
            this.dgvGeografski.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.dgvGeografski.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvGeografskiObjekti_UserDeletedRow);
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Id";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Visible = false;
            this.dataGridViewTextBoxColumn4.Width = 200;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Naziv geografskog objekta";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 260;
            // 
            // btnIzaberiGeografski
            // 
            this.btnIzaberiGeografski.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btnIzaberiGeografski.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            this.btnIzaberiGeografski.Location = new System.Drawing.Point(128, 488);
            this.btnIzaberiGeografski.Name = "btnIzaberiGeografski";
            this.btnIzaberiGeografski.Size = new System.Drawing.Size(177, 23);
            this.btnIzaberiGeografski.TabIndex = 57;
            this.btnIzaberiGeografski.Text = "Izaberi geografski objekat";
            this.btnIzaberiGeografski.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnIzaberiGeografski.UseSelectable = true;
            this.btnIzaberiGeografski.Click += new System.EventHandler(this.btnIzaberiGeografski_Click);
            // 
            // btnUnesiVrh
            // 
            this.btnUnesiVrh.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btnUnesiVrh.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            this.btnUnesiVrh.Location = new System.Drawing.Point(203, 828);
            this.btnUnesiVrh.Name = "btnUnesiVrh";
            this.btnUnesiVrh.Size = new System.Drawing.Size(102, 23);
            this.btnUnesiVrh.TabIndex = 47;
            this.btnUnesiVrh.Text = "Unesi vrh";
            this.btnUnesiVrh.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnUnesiVrh.UseSelectable = true;
            this.btnUnesiVrh.Click += new System.EventHandler(this.btnUnesiVrh_Click);
            // 
            // tbVisinaVrha
            // 
            // 
            // 
            // 
            this.tbVisinaVrha.CustomButton.Image = null;
            this.tbVisinaVrha.CustomButton.Location = new System.Drawing.Point(80, 1);
            this.tbVisinaVrha.CustomButton.Name = "";
            this.tbVisinaVrha.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbVisinaVrha.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbVisinaVrha.CustomButton.TabIndex = 1;
            this.tbVisinaVrha.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbVisinaVrha.CustomButton.UseSelectable = true;
            this.tbVisinaVrha.CustomButton.Visible = false;
            this.tbVisinaVrha.Lines = new string[0];
            this.tbVisinaVrha.Location = new System.Drawing.Point(203, 792);
            this.tbVisinaVrha.MaxLength = 32767;
            this.tbVisinaVrha.Name = "tbVisinaVrha";
            this.tbVisinaVrha.PasswordChar = '\0';
            this.tbVisinaVrha.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbVisinaVrha.SelectedText = "";
            this.tbVisinaVrha.SelectionLength = 0;
            this.tbVisinaVrha.SelectionStart = 0;
            this.tbVisinaVrha.ShortcutsEnabled = true;
            this.tbVisinaVrha.Size = new System.Drawing.Size(102, 23);
            this.tbVisinaVrha.TabIndex = 56;
            this.tbVisinaVrha.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbVisinaVrha.UseSelectable = true;
            this.tbVisinaVrha.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbVisinaVrha.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbVisinaVrha.Enter += new System.EventHandler(this.RemoveText1);
            this.tbVisinaVrha.Leave += new System.EventHandler(this.AddText1);
            // 
            // tbNazivVrha
            // 
            // 
            // 
            // 
            this.tbNazivVrha.CustomButton.Image = null;
            this.tbNazivVrha.CustomButton.Location = new System.Drawing.Point(159, 1);
            this.tbNazivVrha.CustomButton.Name = "";
            this.tbNazivVrha.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbNazivVrha.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbNazivVrha.CustomButton.TabIndex = 1;
            this.tbNazivVrha.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbNazivVrha.CustomButton.UseSelectable = true;
            this.tbNazivVrha.CustomButton.Visible = false;
            this.tbNazivVrha.Lines = new string[0];
            this.tbNazivVrha.Location = new System.Drawing.Point(16, 792);
            this.tbNazivVrha.MaxLength = 32767;
            this.tbNazivVrha.Name = "tbNazivVrha";
            this.tbNazivVrha.PasswordChar = '\0';
            this.tbNazivVrha.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbNazivVrha.SelectedText = "";
            this.tbNazivVrha.SelectionLength = 0;
            this.tbNazivVrha.SelectionStart = 0;
            this.tbNazivVrha.ShortcutsEnabled = true;
            this.tbNazivVrha.Size = new System.Drawing.Size(181, 23);
            this.tbNazivVrha.TabIndex = 55;
            this.tbNazivVrha.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbNazivVrha.UseSelectable = true;
            this.tbNazivVrha.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbNazivVrha.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbNazivVrha.Enter += new System.EventHandler(this.RemoveText);
            this.tbNazivVrha.Leave += new System.EventHandler(this.AddText);
            // 
            // dgvVrhovi
            // 
            this.dgvVrhovi.AllowUserToAddRows = false;
            this.dgvVrhovi.AllowUserToResizeRows = false;
            this.dgvVrhovi.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvVrhovi.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvVrhovi.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvVrhovi.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVrhovi.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvVrhovi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVrhovi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.Column4});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvVrhovi.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgvVrhovi.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvVrhovi.EnableHeadersVisualStyles = false;
            this.dgvVrhovi.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvVrhovi.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.dgvVrhovi.Location = new System.Drawing.Point(16, 568);
            this.dgvVrhovi.Name = "dgvVrhovi";
            this.dgvVrhovi.ReadOnly = true;
            this.dgvVrhovi.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVrhovi.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvVrhovi.RowHeadersVisible = false;
            this.dgvVrhovi.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvVrhovi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVrhovi.Size = new System.Drawing.Size(308, 218);
            this.dgvVrhovi.TabIndex = 54;
            this.dgvVrhovi.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Broj";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 70;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Naziv vrha";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 120;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Nadmorska visina";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel6.Location = new System.Drawing.Point(16, 539);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(130, 19);
            this.metroLabel6.TabIndex = 53;
            this.metroLabel6.Text = "Vrhovi na uzvišenju:";
            this.metroLabel6.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel5.ForeColor = System.Drawing.Color.Coral;
            this.metroLabel5.Location = new System.Drawing.Point(20, 228);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(201, 19);
            this.metroLabel5.TabIndex = 52;
            this.metroLabel5.Text = "Geografski objekti na uzvišenju:";
            this.metroLabel5.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel3.Location = new System.Drawing.Point(20, 92);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(148, 19);
            this.metroLabel3.TabIndex = 51;
            this.metroLabel3.Text = "Vrsta vodene površine:";
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // cbVrstaVodenePovrsine
            // 
            this.cbVrstaVodenePovrsine.FormattingEnabled = true;
            this.cbVrstaVodenePovrsine.ItemHeight = 23;
            this.cbVrstaVodenePovrsine.Items.AddRange(new object[] {
            "Jezero",
            "Bara",
            "More"});
            this.cbVrstaVodenePovrsine.Location = new System.Drawing.Point(188, 82);
            this.cbVrstaVodenePovrsine.Name = "cbVrstaVodenePovrsine";
            this.cbVrstaVodenePovrsine.Size = new System.Drawing.Size(121, 29);
            this.cbVrstaVodenePovrsine.TabIndex = 42;
            this.cbVrstaVodenePovrsine.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cbVrstaVodenePovrsine.UseSelectable = true;
            // 
            // cbVodenaPovrsina
            // 
            this.cbVodenaPovrsina.AutoSize = true;
            this.cbVodenaPovrsina.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.cbVodenaPovrsina.Location = new System.Drawing.Point(16, 51);
            this.cbVodenaPovrsina.Name = "cbVodenaPovrsina";
            this.cbVodenaPovrsina.Size = new System.Drawing.Size(127, 19);
            this.cbVodenaPovrsina.TabIndex = 48;
            this.cbVodenaPovrsina.Text = "Vodena površina";
            this.cbVodenaPovrsina.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cbVodenaPovrsina.UseSelectable = true;
            this.cbVodenaPovrsina.CheckedChanged += new System.EventHandler(this.cbReka_CheckedChanged);
            // 
            // cbUzvisenje
            // 
            this.cbUzvisenje.AutoSize = true;
            this.cbUzvisenje.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.cbUzvisenje.Location = new System.Drawing.Point(16, 144);
            this.cbUzvisenje.Name = "cbUzvisenje";
            this.cbUzvisenje.Size = new System.Drawing.Size(82, 19);
            this.cbUzvisenje.TabIndex = 49;
            this.cbUzvisenje.Text = "Uzvišenje";
            this.cbUzvisenje.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cbUzvisenje.UseSelectable = true;
            this.cbUzvisenje.CheckedChanged += new System.EventHandler(this.cbPut_CheckedChanged);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel2.Location = new System.Drawing.Point(16, 12);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(157, 19);
            this.metroLabel2.TabIndex = 47;
            this.metroLabel2.Text = "Tip površinskog objekta:";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(292, 27);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(75, 23);
            this.metroButton1.TabIndex = 4;
            this.metroButton1.Text = "devtools";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.Location = new System.Drawing.Point(735, 31);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(261, 19);
            this.metroLabel1.TabIndex = 5;
            this.metroLabel1.Text = "Dvoklik na kartu za učitavanje koordinata";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // CreatePovrsinskiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1278, 942);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "CreatePovrsinskiForm";
            this.Text = "Novi površinski objekat";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.CreateLinijskiForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLinijski)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKoordinate)).EndInit();
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGeografski)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVrhovi)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroPanel panel1;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton metroButton4;
        private MetroFramework.Controls.MetroTextBox tbNaziv;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroGrid dgvKoordinate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroButton metroButton6;
        private MetroFramework.Controls.MetroComboBox cbVrstaVodenePovrsine;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroCheckBox cbVodenaPovrsina;
        private MetroFramework.Controls.MetroCheckBox cbUzvisenje;
        private MetroFramework.Controls.MetroGrid dgvVrhovi;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroButton btnUnesiVrh;
        private MetroFramework.Controls.MetroTextBox tbVisinaVrha;
        private MetroFramework.Controls.MetroTextBox tbNazivVrha;
        private MetroFramework.Controls.MetroButton btnIzaberiGeografski;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private MetroFramework.Controls.MetroGrid dgvLinijski;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private MetroFramework.Controls.MetroGrid dgvGeografski;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroTextBox tbNadmorskaVisina;
        private MetroFramework.Controls.MetroLabel metroLabel8;
    }
}