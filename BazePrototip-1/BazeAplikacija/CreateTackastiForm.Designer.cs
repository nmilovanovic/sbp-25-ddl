﻿namespace BazeAplikacija
{
    partial class CreateTackastiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.tbNaziv = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroButton4 = new MetroFramework.Controls.MetroButton();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.lvZnamenitosti = new MetroFramework.Controls.MetroGrid();
            this.Naziv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Opis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbOpisZnamenitosti = new MetroFramework.Controls.MetroTextBox();
            this.tbImeZnamenitosti = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.cbTipTurizma = new MetroFramework.Controls.MetroComboBox();
            this.dtpTuristicko = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.cbTuristicko = new MetroFramework.Controls.MetroCheckBox();
            this.dtpNaseljeno = new MetroFramework.Controls.MetroDateTime();
            this.tbOpstina = new MetroFramework.Controls.MetroTextBox();
            this.tbBrojStanovnika = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.cbNaseljeno = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.tbAlt = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.tbProbaLen = new MetroFramework.Controls.MetroTextBox();
            this.tbProbaLat = new MetroFramework.Controls.MetroTextBox();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel1.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lvZnamenitosti)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.metroPanel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(20, 60);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1026, 854);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.HorizontalScrollbarBarColor = true;
            this.panel1.HorizontalScrollbarHighlightOnWheel = false;
            this.panel1.HorizontalScrollbarSize = 10;
            this.panel1.Location = new System.Drawing.Point(353, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(670, 848);
            this.panel1.TabIndex = 0;
            this.panel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.panel1.VerticalScrollbarBarColor = true;
            this.panel1.VerticalScrollbarHighlightOnWheel = false;
            this.panel1.VerticalScrollbarSize = 10;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.tbNaziv);
            this.metroPanel1.Controls.Add(this.metroLabel12);
            this.metroPanel1.Controls.Add(this.metroButton4);
            this.metroPanel1.Controls.Add(this.metroButton3);
            this.metroPanel1.Controls.Add(this.metroButton2);
            this.metroPanel1.Controls.Add(this.lvZnamenitosti);
            this.metroPanel1.Controls.Add(this.tbOpisZnamenitosti);
            this.metroPanel1.Controls.Add(this.tbImeZnamenitosti);
            this.metroPanel1.Controls.Add(this.metroLabel11);
            this.metroPanel1.Controls.Add(this.cbTipTurizma);
            this.metroPanel1.Controls.Add(this.dtpTuristicko);
            this.metroPanel1.Controls.Add(this.metroLabel10);
            this.metroPanel1.Controls.Add(this.metroLabel9);
            this.metroPanel1.Controls.Add(this.cbTuristicko);
            this.metroPanel1.Controls.Add(this.dtpNaseljeno);
            this.metroPanel1.Controls.Add(this.tbOpstina);
            this.metroPanel1.Controls.Add(this.tbBrojStanovnika);
            this.metroPanel1.Controls.Add(this.metroLabel8);
            this.metroPanel1.Controls.Add(this.metroLabel7);
            this.metroPanel1.Controls.Add(this.metroLabel6);
            this.metroPanel1.Controls.Add(this.cbNaseljeno);
            this.metroPanel1.Controls.Add(this.metroLabel5);
            this.metroPanel1.Controls.Add(this.tbAlt);
            this.metroPanel1.Controls.Add(this.metroLabel4);
            this.metroPanel1.Controls.Add(this.metroLabel3);
            this.metroPanel1.Controls.Add(this.metroLabel2);
            this.metroPanel1.Controls.Add(this.tbProbaLen);
            this.metroPanel1.Controls.Add(this.tbProbaLat);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(3, 3);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(344, 848);
            this.metroPanel1.TabIndex = 1;
            this.metroPanel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // tbNaziv
            // 
            // 
            // 
            // 
            this.tbNaziv.CustomButton.Image = null;
            this.tbNaziv.CustomButton.Location = new System.Drawing.Point(105, 1);
            this.tbNaziv.CustomButton.Name = "";
            this.tbNaziv.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbNaziv.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbNaziv.CustomButton.TabIndex = 1;
            this.tbNaziv.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbNaziv.CustomButton.UseSelectable = true;
            this.tbNaziv.CustomButton.Visible = false;
            this.tbNaziv.Lines = new string[0];
            this.tbNaziv.Location = new System.Drawing.Point(165, 0);
            this.tbNaziv.MaxLength = 32767;
            this.tbNaziv.Name = "tbNaziv";
            this.tbNaziv.PasswordChar = '\0';
            this.tbNaziv.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbNaziv.SelectedText = "";
            this.tbNaziv.SelectionLength = 0;
            this.tbNaziv.SelectionStart = 0;
            this.tbNaziv.ShortcutsEnabled = true;
            this.tbNaziv.Size = new System.Drawing.Size(127, 23);
            this.tbNaziv.TabIndex = 31;
            this.tbNaziv.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbNaziv.UseSelectable = true;
            this.tbNaziv.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbNaziv.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel12.Location = new System.Drawing.Point(15, 0);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(45, 19);
            this.metroLabel12.TabIndex = 30;
            this.metroLabel12.Text = "Naziv:";
            this.metroLabel12.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroButton4
            // 
            this.metroButton4.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.metroButton4.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            this.metroButton4.Location = new System.Drawing.Point(165, 822);
            this.metroButton4.Name = "metroButton4";
            this.metroButton4.Size = new System.Drawing.Size(127, 23);
            this.metroButton4.TabIndex = 29;
            this.metroButton4.Text = "Prosledi";
            this.metroButton4.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroButton4.UseSelectable = true;
            this.metroButton4.Click += new System.EventHandler(this.metroButton4_Click);
            // 
            // metroButton3
            // 
            this.metroButton3.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.metroButton3.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            this.metroButton3.Location = new System.Drawing.Point(145, 150);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(147, 23);
            this.metroButton3.TabIndex = 28;
            this.metroButton3.Text = "Pripadnost uzvišenjima";
            this.metroButton3.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroButton3.UseSelectable = true;
            this.metroButton3.Click += new System.EventHandler(this.metroButton3_Click);
            // 
            // metroButton2
            // 
            this.metroButton2.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.metroButton2.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            this.metroButton2.Location = new System.Drawing.Point(171, 444);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(121, 23);
            this.metroButton2.TabIndex = 27;
            this.metroButton2.Text = "Unesi znamenitost";
            this.metroButton2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.btnUnesiZnamenitost_Click);
            // 
            // lvZnamenitosti
            // 
            this.lvZnamenitosti.AllowUserToAddRows = false;
            this.lvZnamenitosti.AllowUserToResizeRows = false;
            this.lvZnamenitosti.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.lvZnamenitosti.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvZnamenitosti.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.lvZnamenitosti.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.lvZnamenitosti.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.lvZnamenitosti.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lvZnamenitosti.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Naziv,
            this.Opis});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.lvZnamenitosti.DefaultCellStyle = dataGridViewCellStyle5;
            this.lvZnamenitosti.EnableHeadersVisualStyles = false;
            this.lvZnamenitosti.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lvZnamenitosti.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.lvZnamenitosti.Location = new System.Drawing.Point(15, 476);
            this.lvZnamenitosti.Name = "lvZnamenitosti";
            this.lvZnamenitosti.ReadOnly = true;
            this.lvZnamenitosti.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.lvZnamenitosti.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.lvZnamenitosti.RowHeadersVisible = false;
            this.lvZnamenitosti.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.lvZnamenitosti.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.lvZnamenitosti.Size = new System.Drawing.Size(277, 172);
            this.lvZnamenitosti.TabIndex = 26;
            this.lvZnamenitosti.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // Naziv
            // 
            this.Naziv.HeaderText = "Naziv";
            this.Naziv.Name = "Naziv";
            this.Naziv.ReadOnly = true;
            this.Naziv.Width = 120;
            // 
            // Opis
            // 
            this.Opis.HeaderText = "Opis znamenitosti";
            this.Opis.Name = "Opis";
            this.Opis.ReadOnly = true;
            this.Opis.Width = 157;
            // 
            // tbOpisZnamenitosti
            // 
            // 
            // 
            // 
            this.tbOpisZnamenitosti.CustomButton.Image = null;
            this.tbOpisZnamenitosti.CustomButton.Location = new System.Drawing.Point(223, 1);
            this.tbOpisZnamenitosti.CustomButton.Name = "";
            this.tbOpisZnamenitosti.CustomButton.Size = new System.Drawing.Size(53, 53);
            this.tbOpisZnamenitosti.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbOpisZnamenitosti.CustomButton.TabIndex = 1;
            this.tbOpisZnamenitosti.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbOpisZnamenitosti.CustomButton.UseSelectable = true;
            this.tbOpisZnamenitosti.CustomButton.Visible = false;
            this.tbOpisZnamenitosti.Enabled = false;
            this.tbOpisZnamenitosti.Lines = new string[] {
        "Opis znamenitosti"};
            this.tbOpisZnamenitosti.Location = new System.Drawing.Point(15, 383);
            this.tbOpisZnamenitosti.MaxLength = 32767;
            this.tbOpisZnamenitosti.Multiline = true;
            this.tbOpisZnamenitosti.Name = "tbOpisZnamenitosti";
            this.tbOpisZnamenitosti.PasswordChar = '\0';
            this.tbOpisZnamenitosti.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbOpisZnamenitosti.SelectedText = "";
            this.tbOpisZnamenitosti.SelectionLength = 0;
            this.tbOpisZnamenitosti.SelectionStart = 0;
            this.tbOpisZnamenitosti.ShortcutsEnabled = true;
            this.tbOpisZnamenitosti.Size = new System.Drawing.Size(277, 55);
            this.tbOpisZnamenitosti.TabIndex = 24;
            this.tbOpisZnamenitosti.Text = "Opis znamenitosti";
            this.tbOpisZnamenitosti.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbOpisZnamenitosti.UseSelectable = true;
            this.tbOpisZnamenitosti.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbOpisZnamenitosti.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbOpisZnamenitosti.Enter += new System.EventHandler(this.RemoveText1);
            this.tbOpisZnamenitosti.Leave += new System.EventHandler(this.AddText1);
            // 
            // tbImeZnamenitosti
            // 
            // 
            // 
            // 
            this.tbImeZnamenitosti.CustomButton.Image = null;
            this.tbImeZnamenitosti.CustomButton.Location = new System.Drawing.Point(255, 1);
            this.tbImeZnamenitosti.CustomButton.Name = "";
            this.tbImeZnamenitosti.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbImeZnamenitosti.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbImeZnamenitosti.CustomButton.TabIndex = 1;
            this.tbImeZnamenitosti.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbImeZnamenitosti.CustomButton.UseSelectable = true;
            this.tbImeZnamenitosti.CustomButton.Visible = false;
            this.tbImeZnamenitosti.Enabled = false;
            this.tbImeZnamenitosti.Lines = new string[] {
        "Naziv znamenitosti"};
            this.tbImeZnamenitosti.Location = new System.Drawing.Point(15, 354);
            this.tbImeZnamenitosti.MaxLength = 32767;
            this.tbImeZnamenitosti.Name = "tbImeZnamenitosti";
            this.tbImeZnamenitosti.PasswordChar = '\0';
            this.tbImeZnamenitosti.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbImeZnamenitosti.SelectedText = "";
            this.tbImeZnamenitosti.SelectionLength = 0;
            this.tbImeZnamenitosti.SelectionStart = 0;
            this.tbImeZnamenitosti.ShortcutsEnabled = true;
            this.tbImeZnamenitosti.Size = new System.Drawing.Size(277, 23);
            this.tbImeZnamenitosti.TabIndex = 23;
            this.tbImeZnamenitosti.Text = "Naziv znamenitosti";
            this.tbImeZnamenitosti.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbImeZnamenitosti.UseSelectable = true;
            this.tbImeZnamenitosti.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbImeZnamenitosti.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbImeZnamenitosti.Enter += new System.EventHandler(this.RemoveText);
            this.tbImeZnamenitosti.Leave += new System.EventHandler(this.AddText);
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel11.Location = new System.Drawing.Point(15, 332);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(92, 19);
            this.metroLabel11.TabIndex = 22;
            this.metroLabel11.Text = "Znamenitosti:";
            this.metroLabel11.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // cbTipTurizma
            // 
            this.cbTipTurizma.Enabled = false;
            this.cbTipTurizma.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbTipTurizma.FormattingEnabled = true;
            this.cbTipTurizma.ItemHeight = 19;
            this.cbTipTurizma.Items.AddRange(new object[] {
            "Letnji",
            "Zimski",
            "Banjski"});
            this.cbTipTurizma.Location = new System.Drawing.Point(165, 698);
            this.cbTipTurizma.Name = "cbTipTurizma";
            this.cbTipTurizma.Size = new System.Drawing.Size(127, 25);
            this.cbTipTurizma.TabIndex = 20;
            this.cbTipTurizma.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cbTipTurizma.UseSelectable = true;
            // 
            // dtpTuristicko
            // 
            this.dtpTuristicko.CustomFormat = "dd.MM.yyyy.";
            this.dtpTuristicko.Enabled = false;
            this.dtpTuristicko.Location = new System.Drawing.Point(165, 742);
            this.dtpTuristicko.MaxDate = new System.DateTime(2100, 5, 26, 0, 0, 0, 0);
            this.dtpTuristicko.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtpTuristicko.Name = "dtpTuristicko";
            this.dtpTuristicko.Size = new System.Drawing.Size(127, 29);
            this.dtpTuristicko.TabIndex = 19;
            this.dtpTuristicko.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.dtpTuristicko.Value = new System.DateTime(2018, 5, 26, 0, 0, 0, 0);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel10.Location = new System.Drawing.Point(15, 752);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(106, 19);
            this.metroLabel10.TabIndex = 18;
            this.metroLabel10.Text = "Datum početka:";
            this.metroLabel10.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel9.Location = new System.Drawing.Point(15, 704);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(80, 19);
            this.metroLabel9.TabIndex = 17;
            this.metroLabel9.Text = "Tip turizma:";
            this.metroLabel9.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // cbTuristicko
            // 
            this.cbTuristicko.AutoSize = true;
            this.cbTuristicko.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.cbTuristicko.Location = new System.Drawing.Point(15, 667);
            this.cbTuristicko.Name = "cbTuristicko";
            this.cbTuristicko.Size = new System.Drawing.Size(125, 19);
            this.cbTuristicko.TabIndex = 16;
            this.cbTuristicko.Text = "Turističko mesto";
            this.cbTuristicko.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cbTuristicko.UseSelectable = true;
            this.cbTuristicko.CheckedChanged += new System.EventHandler(this.cbTuristicko_CheckedChanged);
            // 
            // dtpNaseljeno
            // 
            this.dtpNaseljeno.CustomFormat = "dd.MM.yyyy.";
            this.dtpNaseljeno.Enabled = false;
            this.dtpNaseljeno.Location = new System.Drawing.Point(165, 280);
            this.dtpNaseljeno.MaxDate = new System.DateTime(2100, 5, 26, 0, 0, 0, 0);
            this.dtpNaseljeno.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtpNaseljeno.Name = "dtpNaseljeno";
            this.dtpNaseljeno.Size = new System.Drawing.Size(127, 29);
            this.dtpNaseljeno.TabIndex = 15;
            this.dtpNaseljeno.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.dtpNaseljeno.Value = new System.DateTime(2018, 5, 26, 0, 0, 0, 0);
            // 
            // tbOpstina
            // 
            // 
            // 
            // 
            this.tbOpstina.CustomButton.Image = null;
            this.tbOpstina.CustomButton.Location = new System.Drawing.Point(105, 1);
            this.tbOpstina.CustomButton.Name = "";
            this.tbOpstina.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbOpstina.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbOpstina.CustomButton.TabIndex = 1;
            this.tbOpstina.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbOpstina.CustomButton.UseSelectable = true;
            this.tbOpstina.CustomButton.Visible = false;
            this.tbOpstina.Enabled = false;
            this.tbOpstina.Lines = new string[0];
            this.tbOpstina.Location = new System.Drawing.Point(165, 250);
            this.tbOpstina.MaxLength = 32767;
            this.tbOpstina.Name = "tbOpstina";
            this.tbOpstina.PasswordChar = '\0';
            this.tbOpstina.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbOpstina.SelectedText = "";
            this.tbOpstina.SelectionLength = 0;
            this.tbOpstina.SelectionStart = 0;
            this.tbOpstina.ShortcutsEnabled = true;
            this.tbOpstina.Size = new System.Drawing.Size(127, 23);
            this.tbOpstina.TabIndex = 14;
            this.tbOpstina.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbOpstina.UseSelectable = true;
            this.tbOpstina.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbOpstina.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tbBrojStanovnika
            // 
            // 
            // 
            // 
            this.tbBrojStanovnika.CustomButton.Image = null;
            this.tbBrojStanovnika.CustomButton.Location = new System.Drawing.Point(105, 1);
            this.tbBrojStanovnika.CustomButton.Name = "";
            this.tbBrojStanovnika.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbBrojStanovnika.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbBrojStanovnika.CustomButton.TabIndex = 1;
            this.tbBrojStanovnika.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbBrojStanovnika.CustomButton.UseSelectable = true;
            this.tbBrojStanovnika.CustomButton.Visible = false;
            this.tbBrojStanovnika.Enabled = false;
            this.tbBrojStanovnika.Lines = new string[0];
            this.tbBrojStanovnika.Location = new System.Drawing.Point(165, 218);
            this.tbBrojStanovnika.MaxLength = 32767;
            this.tbBrojStanovnika.Name = "tbBrojStanovnika";
            this.tbBrojStanovnika.PasswordChar = '\0';
            this.tbBrojStanovnika.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbBrojStanovnika.SelectedText = "";
            this.tbBrojStanovnika.SelectionLength = 0;
            this.tbBrojStanovnika.SelectionStart = 0;
            this.tbBrojStanovnika.ShortcutsEnabled = true;
            this.tbBrojStanovnika.Size = new System.Drawing.Size(127, 23);
            this.tbBrojStanovnika.TabIndex = 13;
            this.tbBrojStanovnika.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbBrojStanovnika.UseSelectable = true;
            this.tbBrojStanovnika.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbBrojStanovnika.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel8.Location = new System.Drawing.Point(15, 290);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(115, 19);
            this.metroLabel8.TabIndex = 12;
            this.metroLabel8.Text = "Datum osnivanja:";
            this.metroLabel8.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel7.Location = new System.Drawing.Point(15, 254);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(60, 19);
            this.metroLabel7.TabIndex = 11;
            this.metroLabel7.Text = "Opština:";
            this.metroLabel7.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel6.Location = new System.Drawing.Point(15, 222);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(106, 19);
            this.metroLabel6.TabIndex = 10;
            this.metroLabel6.Text = "Broj stanovnika:";
            this.metroLabel6.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // cbNaseljeno
            // 
            this.cbNaseljeno.AutoSize = true;
            this.cbNaseljeno.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.cbNaseljeno.Location = new System.Drawing.Point(15, 186);
            this.cbNaseljeno.Name = "cbNaseljeno";
            this.cbNaseljeno.Size = new System.Drawing.Size(126, 19);
            this.cbNaseljeno.TabIndex = 9;
            this.cbNaseljeno.Text = "Naseljeno mesto";
            this.cbNaseljeno.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.cbNaseljeno.UseSelectable = true;
            this.cbNaseljeno.CheckedChanged += new System.EventHandler(this.cbNaseljeno_CheckedChanged);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel5.Location = new System.Drawing.Point(298, 118);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(21, 19);
            this.metroLabel5.TabIndex = 8;
            this.metroLabel5.Text = "m";
            this.metroLabel5.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // tbAlt
            // 
            // 
            // 
            // 
            this.tbAlt.CustomButton.Image = null;
            this.tbAlt.CustomButton.Location = new System.Drawing.Point(105, 1);
            this.tbAlt.CustomButton.Name = "";
            this.tbAlt.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbAlt.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbAlt.CustomButton.TabIndex = 1;
            this.tbAlt.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbAlt.CustomButton.UseSelectable = true;
            this.tbAlt.CustomButton.Visible = false;
            this.tbAlt.Lines = new string[0];
            this.tbAlt.Location = new System.Drawing.Point(165, 116);
            this.tbAlt.MaxLength = 32767;
            this.tbAlt.Name = "tbAlt";
            this.tbAlt.PasswordChar = '\0';
            this.tbAlt.ReadOnly = true;
            this.tbAlt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbAlt.SelectedText = "";
            this.tbAlt.SelectionLength = 0;
            this.tbAlt.SelectionStart = 0;
            this.tbAlt.ShortcutsEnabled = true;
            this.tbAlt.Size = new System.Drawing.Size(127, 23);
            this.tbAlt.TabIndex = 7;
            this.tbAlt.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbAlt.UseSelectable = true;
            this.tbAlt.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbAlt.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel4.Location = new System.Drawing.Point(15, 120);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(120, 19);
            this.metroLabel4.TabIndex = 6;
            this.metroLabel4.Text = "Nadmorska visina:";
            this.metroLabel4.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel3.Location = new System.Drawing.Point(15, 82);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(125, 19);
            this.metroLabel3.TabIndex = 5;
            this.metroLabel3.Text = "Geografska dužina:";
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel2.Location = new System.Drawing.Point(15, 43);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(117, 19);
            this.metroLabel2.TabIndex = 4;
            this.metroLabel2.Text = "Geografska širina:";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // tbProbaLen
            // 
            // 
            // 
            // 
            this.tbProbaLen.CustomButton.Image = null;
            this.tbProbaLen.CustomButton.Location = new System.Drawing.Point(105, 1);
            this.tbProbaLen.CustomButton.Name = "";
            this.tbProbaLen.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbProbaLen.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbProbaLen.CustomButton.TabIndex = 1;
            this.tbProbaLen.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbProbaLen.CustomButton.UseSelectable = true;
            this.tbProbaLen.CustomButton.Visible = false;
            this.tbProbaLen.Lines = new string[0];
            this.tbProbaLen.Location = new System.Drawing.Point(165, 78);
            this.tbProbaLen.MaxLength = 32767;
            this.tbProbaLen.Name = "tbProbaLen";
            this.tbProbaLen.PasswordChar = '\0';
            this.tbProbaLen.ReadOnly = true;
            this.tbProbaLen.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbProbaLen.SelectedText = "";
            this.tbProbaLen.SelectionLength = 0;
            this.tbProbaLen.SelectionStart = 0;
            this.tbProbaLen.ShortcutsEnabled = true;
            this.tbProbaLen.Size = new System.Drawing.Size(127, 23);
            this.tbProbaLen.TabIndex = 3;
            this.tbProbaLen.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbProbaLen.UseSelectable = true;
            this.tbProbaLen.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbProbaLen.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tbProbaLat
            // 
            // 
            // 
            // 
            this.tbProbaLat.CustomButton.Image = null;
            this.tbProbaLat.CustomButton.Location = new System.Drawing.Point(105, 1);
            this.tbProbaLat.CustomButton.Name = "";
            this.tbProbaLat.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbProbaLat.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbProbaLat.CustomButton.TabIndex = 1;
            this.tbProbaLat.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbProbaLat.CustomButton.UseSelectable = true;
            this.tbProbaLat.CustomButton.Visible = false;
            this.tbProbaLat.Lines = new string[0];
            this.tbProbaLat.Location = new System.Drawing.Point(165, 39);
            this.tbProbaLat.MaxLength = 32767;
            this.tbProbaLat.Name = "tbProbaLat";
            this.tbProbaLat.PasswordChar = '\0';
            this.tbProbaLat.ReadOnly = true;
            this.tbProbaLat.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbProbaLat.SelectedText = "";
            this.tbProbaLat.SelectionLength = 0;
            this.tbProbaLat.SelectionStart = 0;
            this.tbProbaLat.ShortcutsEnabled = true;
            this.tbProbaLat.Size = new System.Drawing.Size(127, 23);
            this.tbProbaLat.TabIndex = 2;
            this.tbProbaLat.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tbProbaLat.UseSelectable = true;
            this.tbProbaLat.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbProbaLat.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(292, 31);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(75, 23);
            this.metroButton1.TabIndex = 4;
            this.metroButton1.Text = "devtools";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.Location = new System.Drawing.Point(735, 31);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(261, 19);
            this.metroLabel1.TabIndex = 5;
            this.metroLabel1.Text = "Dvoklik na kartu za učitavanje koordinata";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // CreateTackastiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1066, 934);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "CreateTackastiForm";
            this.Text = "Novi tačkasti objekat";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tableLayoutPanel1.ResumeLayout(false);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lvZnamenitosti)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroPanel panel1;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroTextBox tbProbaLen;
        private MetroFramework.Controls.MetroTextBox tbProbaLat;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox tbAlt;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroCheckBox cbNaseljeno;
        private MetroFramework.Controls.MetroDateTime dtpNaseljeno;
        private MetroFramework.Controls.MetroTextBox tbOpstina;
        private MetroFramework.Controls.MetroTextBox tbBrojStanovnika;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroCheckBox cbTuristicko;
        private MetroFramework.Controls.MetroComboBox cbTipTurizma;
        private MetroFramework.Controls.MetroDateTime dtpTuristicko;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroTextBox tbOpisZnamenitosti;
        private MetroFramework.Controls.MetroTextBox tbImeZnamenitosti;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroGrid lvZnamenitosti;
        private System.Windows.Forms.DataGridViewTextBoxColumn Naziv;
        private System.Windows.Forms.DataGridViewTextBoxColumn Opis;
        private MetroFramework.Controls.MetroButton metroButton3;
        private MetroFramework.Controls.MetroButton metroButton4;
        private MetroFramework.Controls.MetroTextBox tbNaziv;
        private MetroFramework.Controls.MetroLabel metroLabel12;
    }
}