﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using System.IO;
using Sistemi_Baza_Podataka_Deo_Drugi;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;
using NHibernate;
using MetroFramework;

namespace BazeAplikacija
{
    public partial class CreateLinijskiForm : MetroForm
    {
        IList<Uzvisenje> izabranaUzvisenja = null;
        IList<TackastiObjekat> listaIzabranihTackastih = new List<TackastiObjekat>();
        TackastiObjekat izabraniTackasti = null;

        private LinijskiObjekat workingObject;

        public CreateLinijskiForm()
        {
            workingObject = null;

            InitializeComponent();
            InitializeBrowser();
            //LatInChanged += LatInChangedHandler;
            //LngInChanged += LngInChangedHandler;
            //AltInChanged += AltInChangedHandler;
            //cbTipTurizma.SelectedIndex = 0;
           // dtpTuristicko.Format = DateTimePickerFormat.Custom;
           // dtpTuristicko.CustomFormat = "dd.MM.yyyy.";
        }

        public CreateLinijskiForm(LinijskiObjekat lob)
        {
            InitializeComponent();
            InitializeBrowser();

            workingObject = lob;
            this.Text = "Ažuriraj linijski objekat";
            metroButton4.Text = "Ažuriraj";
            tbNaziv.Text = lob.Naziv;
            tbDuzina.Text = lob.Duzina.ToString();

            inj.SetZoom(-1);

            izabranaUzvisenja = new List<Uzvisenje>();
            foreach (Uzvisenje uzv in MySession.GetAllUzvisenje())
            {
                bool unutra = false;
                foreach (GeografskiObjekat go in uzv.Objekti)
                {
                    if (go.Id == lob.Id)
                    {
                        unutra = true;
                        break;
                    }
                }
                if (unutra)
                {
                    izabranaUzvisenja.Add(uzv);
                }
            }
            listaIzabranihTackastih = new List<TackastiObjekat>();
            foreach(NalaziSeNa nsn in lob.NalaziSeNa.OrderBy(x => x.RedniBroj))
            {
                listaIzabranihTackastih.Add(nsn.Tackasti);
                dgvTackasti.Rows.Add(nsn.RedniBroj, nsn.Tackasti.Naziv, nsn.RastojanjeNaredni);
            }
            foreach(Koordinata koor in lob.Koordinate.OrderBy(x => x.Indeks))
            {
                dgvKoordinate.Rows.Add(koor.Indeks, koor.GeografskaSirina, koor.GeografskaDuzina);
            }
            Crtaj();

            if (lob.TipLinijskogObjekta == "reka")
            {
                cbReka.Checked = true;
            }
            else if (lob.TipLinijskogObjekta == "put")
            {
                cbPut.Checked = true;
                String klasa = (lob as Put).KlasaPuta;
                if (Char.IsDigit(klasa[0]))
                {
                    klasa += " klasa";
                }
                cbKlasaPuta.SelectedItem = cbKlasaPuta.Items.OfType<String>().First(x => x.ToLower() == klasa);
            }
            else if (lob.TipLinijskogObjekta == "granicna linija")
            {
                cbGranicnaLinija.Checked = true;
                tbDrzava.Text = (lob as GranicnaLinija).ImeDrzave;
            }
            cbReka.Enabled = false;
            cbPut.Enabled = false;
            cbGranicnaLinija.Enabled = false;

            MySession.Close();

            //LatInChanged += LatInChangedHandler;
            //LngInChanged += LngInChangedHandler;
            //AltInChanged += AltInChangedHandler;
            //cbTipTurizma.SelectedIndex = 0;
            // dtpTuristicko.Format = DateTimePickerFormat.Custom;
            // dtpTuristicko.CustomFormat = "dd.MM.yyyy.";
        }

        public ChromiumWebBrowser browser;
        private static readonly string myPath = Application.StartupPath;
        private static readonly string pagesPath = Path.Combine(myPath, "pages");
        private Inject inj;

        public void InitializeBrowser()
        {
            browser = new ChromiumWebBrowser(GetPagePath("osnovni1.html"));
            inj = new Inject(this);
            browser.RegisterJsObject("inj", inj); 
            panel1.Controls.Add(browser);
            browser.Dock = DockStyle.Fill;
        }

        private string GetPagePath(string pageName)
        {
            return Path.Combine(pagesPath, pageName);
        }

        //ovde se Inject koristi samo za vracanje kliknutih koordinata
        public class Inject
        {

            private CreateLinijskiForm myForm;
            private string tipObjekta = "unoslinijskog";
            //startne koordinate
            private double geografskaSirina = 43.331281;
            private double geografskaDuzina = 21.892535;

            private double latIn;
            private double lngIn;
            private double lengthIn;
            private double zoom = 10;

            public Inject(CreateLinijskiForm f)
            {
                myForm = f;
            }

            public string TipObjekta { get => tipObjekta; set => tipObjekta = value; }

            public string GetTipObjekta()
            {
                return TipObjekta;
            }

            public double GetGeografskaSirina()
            {
                return geografskaSirina;
            }

            public double GetGeografskaDuzina()
            {
                return geografskaDuzina;
            }

            public void SetZoom(double x)
            {
                zoom = x;
            }

            public double GetZoom()
            {
                return zoom;
            }

            public void SetGeografskaSirina(double x)
            {
                geografskaSirina = x;
            }

            public void SetGeografskaDuzina(double x)
            {
                geografskaDuzina = x;
            }

            public void SetLatLngIn(double x, double y,double z)
            {
                latIn = x;
                lngIn = y;
                lengthIn = z;
                LatLngChanged d = myForm.LatLngInChangedHandler;
                myForm.Invoke(d, new object[] { x,y,z });
            }
        }

        private delegate void LatLngChanged(double newLat,double newLng,double newLength);

        private void LatLngInChangedHandler(double newLat, double newLng, double newLength)
        {
            if(newLength == -1)dgvKoordinate.Rows.Add(dgvKoordinate.Rows.Count+1, newLat, newLng);
            else tbDuzina.Text = newLength.ToString();
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            browser.ShowDevTools();
        }      
       
        private void metroButton3_Click(object sender, EventArgs e)
        {
            var iu = new IzborUzvisenja();
            if (iu.ShowDialog() == DialogResult.OK)
            {
                izabranaUzvisenja = iu.ListaIzabranihUzvisenja;
            }
        }
        
        private bool Validacija()
        {
            if (tbNaziv.Text == "" || tbNaziv.Text.EndsWith(" međa"))
            {
                return false;
            }

            if (cbReka.Checked == false && cbPut.Checked == false && cbGranicnaLinija.Checked == false)
            {
                MetroMessageBox.Show(this, "Morate izabrati jedan od tipova linijskog objekta.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
                return false;
            }

            return true;
        }

        private void metroButton4_Click(object sender, EventArgs e)
        {
            if (!Validacija())
            {
                MessageBox.Show("Ne moze brate");
                return;
            }

            if (workingObject != null)
            {
                MySession.Open();

                LinijskiObjekat linijskiObjekat = MySession.GetLinijskiObjekat(workingObject.Id);
                if (cbReka.Checked == true)
                {
                    linijskiObjekat.TipLinijskogObjekta = "reka";
                }
                else if (cbPut.Checked == true)
                {
                    linijskiObjekat.TipLinijskogObjekta = "put";
                    if (cbKlasaPuta.Text == "1. klasa")
                    {
                        ((Put)linijskiObjekat).KlasaPuta = "1.";
                    }
                    else if (cbKlasaPuta.Text == "2. klasa")
                    {
                        ((Put)linijskiObjekat).KlasaPuta = "2.";
                    }
                    else if (cbKlasaPuta.Text == "3. klasa")
                    {
                        ((Put)linijskiObjekat).KlasaPuta = "3.";
                    }
                    else if (cbKlasaPuta.Text == "Auto-put")
                    {
                        ((Put)linijskiObjekat).KlasaPuta = "auto-put";
                    }

                }
                else if (cbGranicnaLinija.Checked == true)
                {
                    linijskiObjekat.TipLinijskogObjekta = "granicna linija";
                    ((GranicnaLinija)linijskiObjekat).ImeDrzave = tbDrzava.Text;
                }

                linijskiObjekat.Naziv = tbNaziv.Text;

                double result;
                bool jelMozet;
                jelMozet = double.TryParse(tbDuzina.Text, out result);
                if (jelMozet)
                {
                    linijskiObjekat.Duzina = result;
                }
                else
                {
                    linijskiObjekat.Duzina = 0;
                }
                if (linijskiObjekat.Duzina < 0)
                {
                    linijskiObjekat.Duzina = 0;
                }

                MySession.Update(linijskiObjekat);
                MySession.Flush();

                foreach (Koordinata koor in linijskiObjekat.Koordinate)
                {
                    MySession.Delete(koor);
                }
                linijskiObjekat.Koordinate = new List<Koordinata>();
                MySession.Update(linijskiObjekat);
                MySession.Flush();
                int ii = 1;
                foreach (DataGridViewRow dgvr in dgvKoordinate.Rows)
                {
                    Koordinata k = new Koordinata();
                    k.MyLinijskiObjekat = linijskiObjekat;
                    k.GeografskaSirina = Double.Parse(dgvr.Cells[1].Value.ToString());
                    k.GeografskaDuzina = Double.Parse(dgvr.Cells[2].Value.ToString());
                    k.Indeks = ii++;
                    linijskiObjekat.Koordinate.Add(k);
                }
                foreach (NalaziSeNa nsn in linijskiObjekat.NalaziSeNa)
                {
                    TackastiObjekat tob = nsn.Tackasti;
                    MySession.Delete(nsn);
                    MySession.Update(tob);
                    MySession.Flush();
                }
                linijskiObjekat.NalaziSeNa = new List<NalaziSeNa>();
                MySession.Update(linijskiObjekat);
                MySession.Flush();
                ii = 1;
                foreach (TackastiObjekat to in listaIzabranihTackastih)
                {
                    NalaziSeNa n = new NalaziSeNa();
                    n.Linijski = linijskiObjekat;
                    n.RedniBroj = ii;
                    n.RastojanjeNaredni = Int32.Parse(dgvTackasti.Rows[ii - 1].Cells[2].Value.ToString());

                    TackastiObjekat managedTackasti = (TackastiObjekat)MySession.Merge(to);

                    n.Tackasti = managedTackasti;
                    linijskiObjekat.NalaziSeNa.Add(n);
                    managedTackasti.NalaziSeNa.Add(n);
                    MySession.Save(n);
                    MySession.Save(managedTackasti);

                    ii++;
                }
                MySession.Update(linijskiObjekat);

                foreach (Uzvisenje uz in MySession.GetAllUzvisenje())
                {
                    GeografskiObjekat workingResult = null;
                    foreach (GeografskiObjekat gob in uz.Objekti)
                    {
                        if (gob.Id == workingObject.Id)
                        {
                            workingResult = gob;
                            break;
                        }
                    }
                    bool unutra = false;
                    foreach (Uzvisenje u in izabranaUzvisenja)
                    {
                        if (u.Id == uz.Id)
                        {
                            unutra = true;
                            break;
                        }
                    }
                    if (unutra && workingResult == null)
                    {
                        uz.Objekti.Add(linijskiObjekat);
                        MySession.Update(uz);
                        MySession.Flush();
                    }
                    if (!unutra && workingResult != null)
                    {
                        uz.Objekti.Remove(workingResult);
                        MySession.Update(uz);
                        MySession.Flush();
                    }
                }
                MySession.Close();
                return;
            }

            LinijskiObjekat r = new LinijskiObjekat();
            if(cbReka.Checked == true)
            {
                r = new Reka();
                r.TipLinijskogObjekta = "reka";   
            }
            else if(cbPut.Checked == true)
            {
                r = new Put();
                r.TipLinijskogObjekta = "put";
                if(cbKlasaPuta.Text == "1. klasa")
                {
                    ((Put)r).KlasaPuta = "1.";
                }
                else if (cbKlasaPuta.Text == "2. klasa")
                {
                    ((Put)r).KlasaPuta = "2.";
                }
                else if (cbKlasaPuta.Text == "3. klasa")
                {
                    ((Put)r).KlasaPuta = "3.";
                }
                else if (cbKlasaPuta.Text == "Auto-put")
                {
                    ((Put)r).KlasaPuta = "auto-put";
                }
                
            }
            else if(cbGranicnaLinija.Checked == true)
            {
                r = new GranicnaLinija();
                r.TipLinijskogObjekta = "granicna linija";
                ((GranicnaLinija)r).ImeDrzave = tbDrzava.Text;
            }

            r.Naziv = tbNaziv.Text;
            double resul;
            bool jelMoze;
            jelMoze = double.TryParse(tbDuzina.Text, out resul);
            if (jelMoze)
            {
                r.Duzina = resul;
            }
            else
            {
                r.Duzina = 0;
            }
            if (r.Duzina < 0)
            {
                r.Duzina = 0;
            }

            int i = 1;
            foreach (DataGridViewRow dgvr in dgvKoordinate.Rows)
            {
                Koordinata k = new Koordinata();
                k.MyLinijskiObjekat = r;
                k.GeografskaSirina = Double.Parse(dgvr.Cells[1].Value.ToString());
                k.GeografskaDuzina = Double.Parse(dgvr.Cells[2].Value.ToString());
                k.Indeks = i++;
                r.Koordinate.Add(k);
            }

            ISession s = DataLayer.GetSession();
            s.Save(r);
            if (izabranaUzvisenja != null && izabranaUzvisenja.Count != 0)
            {
                foreach (Uzvisenje u in izabranaUzvisenja)
                {
                    var managedUzvisenje = s.Merge(u);
                    managedUzvisenje.Objekti.Add(r);
                    s.Update(managedUzvisenje);
                }
            }

            //dodavanje tackastih

            i = 1;
            foreach (TackastiObjekat to in listaIzabranihTackastih)
            {
                NalaziSeNa n = new NalaziSeNa();
                n.Linijski = r;
                n.RedniBroj = i;
                
                n.RastojanjeNaredni = Int32.Parse(dgvTackasti.Rows[i - 1].Cells[2].Value.ToString());
                //managedtackasti.NalaziSeNa.Add(...)
                TackastiObjekat managedTackasti = s.Merge(to);
                managedTackasti.NalaziSeNa.Add(n);
                n.Tackasti = managedTackasti;
                //problematicno
                to.Id = -1;
                s.Save(n);
                s.SaveOrUpdate(managedTackasti);
                i++;
            }

            s.Flush();
            s.Close();

        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            if(dgvKoordinate.Rows.Count != 0)dgvKoordinate.Rows.RemoveAt(dgvKoordinate.Rows.Count - 1);
        }

        private void dgvKoordinate_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if(dgvKoordinate.Rows.Count >1 )Crtaj();
        }

        private void dgvKoordinate_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            Crtaj();
        }

        private void IzveziLinijskeKoordinate()
        {
            /*
           var flightPlanCoordinates = [
          { lat: 37.772, lng: -122.214 },
          { lat: 21.291, lng: -157.821 },
          { lat: -18.142, lng: 178.431 },
          { lat: -27.467, lng: 153.027 }
        ];*/

            StringBuilder sb = new StringBuilder();
            sb.Append("var flightPlanCoordinates = [");

            foreach (DataGridViewRow r in dgvKoordinate.Rows)
            {
                sb.Append("{lat: " + r.Cells[1].Value.ToString() + "," + " lng: " + r.Cells[2].Value.ToString() + " },");
            }
            String s = sb.ToString();
            char[] charsToTrim = { ',' };
            s = s.TrimEnd(charsToTrim);
            s += "];";
            File.WriteAllText(GetPagePath("linijskikoordinate.js"), s);

        }

        private void Crtaj()
        {
            IzveziLinijskeKoordinate();
            browser.Load(GetPagePath("osnovni1.html"));
        }

        private void CreateLinijskiForm_Load(object sender, EventArgs e)
        {
            IzveziLinijskeKoordinate();
            if (cbReka.Enabled)
            {
                cbReka.Checked = true;
                cbKlasaPuta.SelectedIndex = 0;
                tbDrzava.Text = "Ime države";
            }
        }

        private void cbReka_CheckedChanged(object sender, EventArgs e)
        {
            if (cbReka.Checked == true)
            {
                cbPut.Checked = false;
                cbGranicnaLinija.Checked = false;
                cbKlasaPuta.Enabled = false;
                tbDrzava.Enabled = false;
            }
        }

        private void cbPut_CheckedChanged(object sender, EventArgs e)
        {
            if (cbPut.Checked == true)
            {
                cbReka.Checked = false;
                cbGranicnaLinija.Checked = false;
                cbKlasaPuta.Enabled = true;
                tbDrzava.Enabled = false;
            }
        }

        private void metroCheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (cbGranicnaLinija.Checked == true)
            {
                cbPut.Checked = false;
                cbReka.Checked = false;
                cbKlasaPuta.Enabled = false;
                tbDrzava.Enabled = true;
            }
        }

        public void RemoveText(object sender, EventArgs e)
        {
            if (tbDrzava.Text == "Ime države") tbDrzava.Text = "";
        }

        public void AddText(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbDrzava.Text))
                tbDrzava.Text = "Ime države";
        }

        private void metroButton6_Click(object sender, EventArgs e)
        {
            var iu = new IzborTackastih();
            if (iu.ShowDialog() == DialogResult.OK)
            {
                izabraniTackasti = iu.IzabraniTackasti;
                foreach (TackastiObjekat t in listaIzabranihTackastih)
                {
                    if (izabraniTackasti.Id == t.Id)
                    {
                        MetroMessageBox.Show(this, "Ovaj tačkasti objekat je već dodat.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
                        return;
                    }
                }
                listaIzabranihTackastih.Add(izabraniTackasti);
                //add row to dgv
                dgvTackasti.Rows.Add(dgvTackasti.Rows.Count+1, izabraniTackasti.Naziv, 0);
            }
        }

        private void metroButton5_Click(object sender, EventArgs e)
        {
            if (dgvTackasti.Rows.Count != 0)
            {
                listaIzabranihTackastih.RemoveAt(listaIzabranihTackastih.Count - 1);
                dgvTackasti.Rows.RemoveAt(dgvTackasti.Rows.Count - 1);
            }
        }
    }
}
