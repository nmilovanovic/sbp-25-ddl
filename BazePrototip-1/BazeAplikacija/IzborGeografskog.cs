﻿using MetroFramework;
using MetroFramework.Forms;
using Sistemi_Baza_Podataka_Deo_Drugi.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BazeAplikacija
{
    public partial class IzborGeografskog : MetroForm
    {
        public IzborGeografskog()
        {
            InitializeComponent();
        }

        public GeografskiObjekat IzabraniGeografskiObjekat { get; set; }
        public List<GeografskiObjekat> GeografskiObjektiZaPrikaz { get; set; }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count != 1)
            {
                MetroMessageBox.Show(this, "Treba izabrati jedan geografski objekat.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Information, 200);
                return;
            }
            IzabraniGeografskiObjekat = (from l in GeografskiObjektiZaPrikaz where l.Id == Int32.Parse(dgv.SelectedRows[0].Cells[0].Value.ToString()) select l).First();

            DialogResult = DialogResult.OK;
            Close();
        }

        private void IzborGeografskog_Load(object sender, EventArgs e)
        {
            foreach (GeografskiObjekat l in GeografskiObjektiZaPrikaz)
            {
                dgv.Rows.Add(l.Id, l.Naziv);
            }
        }
    }
}
