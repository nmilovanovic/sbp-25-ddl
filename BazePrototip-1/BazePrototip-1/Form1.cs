﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;

namespace BazePrototip_1
{
    public partial class Form1 : Form
    {
        public ChromiumWebBrowser browser;
        private static readonly string myPath = Application.StartupPath;
        private static readonly string pagesPath = Path.Combine(myPath, "pages");

        public Form1()
        {
            InitializeComponent();
            InitializeBrowser();
        }

        public class GeoObj
        {
            public string Name { get; set; }
            public string Tip { get; set; }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            List<GeoObj> l = new List<GeoObj>
            {
                new GeoObj()
                {
                    Name = "Nis",
                    Tip = "Tackasti objekat"
                },
                new GeoObj()
                {
                    Name = "Pesterska visoravan",
                    Tip = "Povrsinski objekat"
                },
                new GeoObj()
                {
                    Name = "Djetinja",
                    Tip = "Linijski objekat"
                },
                new GeoObj()
                {
                    Name = "Golijska moravica",
                    Tip = "Linijski objekat"
                },
                new GeoObj()
                {
                    Name = "Zlatibor",
                    Tip = "Povrsinski objekat"
                },new GeoObj()
                {
                    Name = "Nis",
                    Tip = "Tackasti objekat"
                },
                new GeoObj()
                {
                    Name = "Pesterska visoravan",
                    Tip = "Povrsinski objekat"
                },
                new GeoObj()
                {
                    Name = "Djetinja",
                    Tip = "Linijski objekat"
                },
                new GeoObj()
                {
                    Name = "Golijska moravica",
                    Tip = "Linijski objekat"
                },
                new GeoObj()
                {
                    Name = "Zlatibor",
                    Tip = "Povrsinski objekat"
                },
            };

            dgv.DataSource = l;
            dgv.DataSource = null;
            dgv.DataSource = l;

        }

        public void InitializeBrowser()
        {
            CefSettings settings = new CefSettings();
            Cef.Initialize(settings);
            browser = new ChromiumWebBrowser(GetPagePath("page2.html"));
            //browser = new ChromiumWebBrowser("https://cesiumjs.org/Cesium/Apps/HelloWorld.html");

            richTextBox1.ForeColor = Color.Red;
            richTextBox1.Text += GetPagePath("page1");


            panel1.Controls.Add(browser);
            browser.Dock = DockStyle.Fill;
            
        }

        private string GetPagePath(string pageName)
        {
            return Path.Combine(pagesPath, pageName);
        }

    }
}
